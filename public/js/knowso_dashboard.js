window.onload = function(){
    var tracking_protection = true;
    var fb_status;
    var fb_login_status = document.getElementById("fb_login_status") ;
    window.fbAsyncInit = function() {
        //alert("fbasyncinit")
        //tracking_protection = false;


        FB.init({
            appId            : '1289232081174305',
            autoLogAppEvents : true,
            xfbml            : true,
            version          : 'v3.0'
        });

        //FB.getLoginStatus(function(response) {
        FB.getLoginStatus(function(response) {

            tracking_protection = false;


            //alert("inside FB.getLoginStatus");
            if (response.status === 'connected') {
                // the user is logged in and has authenticated your
                // app, and response.authResponse supplies
                // the user's ID, a valid access token, a signed
                // request, and the time the access token
                // and signed request each expire
                var uid = response.authResponse.userID;
                //var accessToken = response.authResponse.accessToken;
                //alert("Connected: User ID: "+ uid + "\nAccess Token: " + JSON.stringify(response.authResponse));
                FB.api('/'+ uid, function(response) {
                    //alert("Name: "+ response.name + "\nFirst name: "+ response.first_name + "ID: "+response.id);
                    var img_link = "https://graph.facebook.com/"+response.id+"/picture";
                    fb_login_status.innerHTML =
                        "<p><i class='fa facebook-msg-icon fa-facebook-square'></i> <i class='fa fa-star-o'></i> Logged in to Facebook as user: '" + response.name +
                        "' <img src='" + img_link + "' style='max-height:40px'> - visit <a href=\"http://www.facebook.com\" target='_blank'>Facebook</a>";
                    // JSON.stringify(response.authResponse, null, 4)

                });

                //console.log("*****\nFacebook status connected:\n " + JSON.stringify(response.authResponse, null, 4) + "\n*****");


            } else if (response.status === 'not_authorized') {
                //var uid = response.authResponse.userID;
                //var accessToken = response.authResponse.accessToken;
                //var response = FB.getAuthResponse();

                //console.log("*****\nFacebook status - not_authorized:\n " + JSON.stringify(response.authResponse, null, 4) + "\n*****");
                fb_login_status.innerHTML =
                    "<p><i class='fa facebook-msg-icon fa-facebook-square'></i> <i class='fa fa-star-o'></i> Logged in to Facebook but not to this app " +
                    "- Knowso app may need re-authorisation - visit <a href=\"http://www.facebook.com\" target='_blank'>Facebook</a> or reconnect Facebook app below";
                //});
                //alert("User not authorised!");
                // the user must go through the login flow
                // to authorize your app or renew authorization
            }
            else {
                //console.log("*****\nFacebook status - not connected:\n " + JSON.stringify(response.authResponse, null, 4) + "\n*****");
                fb_login_status.innerHTML = "<p><i class='fa facebook-msg-icon fa-facebook-square'></i> You are not currently logged in to Facebook or we cannot determine login status!</p>";
                //alert("User not logged in to Facebook!");
                // the user isn't logged in to Facebook.
            }


        });



        // if(!fb_connect){

        //}

    };


    (function(d, s, id){

        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            //fb_login_status.innerHTML = "* Cannot determine your Facebook login status: 1 return*";
            //alert("returning");
            return;
        }

        //fb_login_status.innerHTML = "* Cannot determine your Facebook login status: 2*";
        //fb_login_status.innerHTML = "* Attempting to determine facebook login status *";


        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        var res = fjs.parentNode.insertBefore(js, fjs);

        //alert(JSON.stringify(js));
        setTimeout(function() {
            check_tracking_protection(fb_login_status, tracking_protection);
        }, 3000)


    }(document, 'script', 'facebook-jssdk'));

    //alert("Status\n" + fb_status);

    $("#scroll-result").click(function() {
        $('html, body').animate({
            scrollTop: $("#result").offset().top
        }, 2000);
    });


};



function check_tracking_protection(fb_login_status, tracking_on){
    if(tracking_on === true){
        fb_login_status.innerHTML = "* Cannot determine your Facebook login status: Disable tracking protection if necessary *\n";
    }
}