

var colour_1 = "#0080ff";
var colour_1_dark = "#003366";
var colour_2 = "#ff471a";
var colour_2_dark = "#661400";
var colour_3 = "#0059b3";
var colour_3_dark = "#00264d";
var colour_4 = "#009900";
var colour_4_dark = "#004d00";

var analysis_score_selected = 0;

if(document.getElementById("analysis_form") != undefined){
    document.getElementById("analysis_form").addEventListener("submit", function(event){
        event.preventDefault();
        //$("div").text($("form").serialize());
        //console.log("Serialize attempt:\n\n" + $(this).serialize());
        var analysis_data = $(this).serialize();
        //alert("Token: "+ analysis_data._token);
        $.ajax({
            type: 'post',
            url: "/save_user_analysis",

            data: analysis_data,
            dataType:'json',
            success: function (data) {
                //alert('ok:\n' + data);

                if(data.error){
                    //alert("Error: \n" + data.error);
                    $('.modal-title').html("Error");
                    $('#saveResultsModal-body').html(data.error);
                    $('#saveResultsModal').modal('toggle');
                }
                else{
                    //alert(JSON.stringify(data));
                    data_saved(data); // process saving of data
                }





            },
            error: function (data,status, response) {

                //console.log("Error - data:\n " + JSON.stringify(data));
                console.log("Error - status:\n " + JSON.stringify(status));
                console.log("Error - response:\n " + JSON.stringify(response));

            }
        });
        //alert("default prevented!");
    });
}
// else{
//     console.log("++ analysis_form undefined! ++");
// }


function data_saved(data){

    if(data !== undefined && data.save_result === true){
        //alert("You have saved your selections");
        $('.modal-title').html("Processing results");
        $('#saveResultsModal-body').html("You have saved your selections<br><a href='/user_analysis/results'>View your results");
        //$('#saveResultsModal').modal('toggle');
    }
    else if(data.no_change !== undefined){
        //alert(data.no_change);
        $('.modal-title').html("No changes");
        $('#saveResultsModal-body').html(data.no_change + "<br><a href='/user_analysis/results'>View previous results");
        //$('#saveResultsModal').modal('toggle');
    }
    else{
        $('.modal-title').html("Possible error!");
        $('#saveResultsModal-body').html("Could not save your selections");

        //alert("Could not save your selections");
    }

    $('#saveResultsModal').modal('toggle');

}

/* De-select selected hidden checkboxes if necessary [for browser refresh, etc] */

if(document.getElementsByClassName("checkbox-hidden") != undefined){
    var checkbox_hidden = document.getElementsByClassName("checkbox-hidden");

    for (var c in checkbox_hidden){
        //console.log("found checkbox id: "+ checkbox_hidden[c].id + "\n");
        $("#"+ checkbox_hidden[c].id).prop('checked', false);
    }
}
// else{
//     console.log("++ checkbox-hidden undefined! ++");
// }


/* hide all section bodys apart from 'section-body-1' on browser refresh / page load */
if(document.getElementsByClassName("section-body") != undefined) {
    var section_body = document.getElementsByClassName("section-body");
    for (var s in section_body) {
        //console.log("found checkbox id: "+ section_body[s].id + "\n");
        if (section_body[s].id === "section-body-1") {
            //$("#"+ section_body[s].id).css('display', 'none');
            $("#" + section_body[s].id).slideDown(1500);
        }

    }
}

if(document.getElementsByClassName("section") != undefined) {
    var sections = document.getElementsByClassName("section");

    for (var s = 0; s < sections.length; s++) {
        //var section_id = sections[s].id;
        sections[s].addEventListener("click", function (event) {


            //alert(this.id);
            var body = $(this).find('div:first');
            body.css("display", "block");
            //body.slideDown();
        });
    }
}

// Add event listeners for each analysis selection button (So invisible checkboxes can be turned on and off, etc)
if(document.getElementsByClassName("btn-analysis-input") != undefined) {

    var btn_analysis_input = document.getElementsByClassName("btn-analysis-input");
//for(var a in btn_analysis_input){
    for (var a = 0; a < btn_analysis_input.length; a++) {
        //var control = a.getElementById()
        // alert(btn_analysis_input[a].id);

        var id = btn_analysis_input[a].id;
        //alert(id);
        btn_analysis_input[a].addEventListener("click", process_analysis);
    }
}

if(document.getElementsByClassName("btn-prev") != undefined) {

    var btn_prev = document.getElementsByClassName("btn-prev");

//for(var b in btn_next){
    var prev_count = btn_prev.length;

    for (var b = 0; b < prev_count; b++) {

        btn_prev[b].addEventListener("click", function () {
          //  console.log("Prev Button clicked id: " + this.id);

            var this_num = (+this.id.substr(5));
            var last_num = (+this.id.substr(5) - 1);
            var this_section = "section-body-" + this_num;
            var last_section = "section-body-" + last_num;
            //console.log("Next section should be id: " + next_section);
            //$("#" + next_section).css("display","block");
            $("#" + this_section).css("display", "none");
            $("#next-" + last_num).css("display", "block"); // display the last 'next-x' button
         //   console.log("** Attempting to scroll back to #section-start-" + last_num + " ***");
            $('html, body').animate({
                //scrollTop: $("#analysis_form").offset().top
                scrollTop: $("#section-start-" + last_num).offset().top
            }, 1000);
            $("#" + last_section).slideDown(1500);
            //$(this).css("display", "none");

            //console.log("b variable count: " + count + " | num count: " + num);


            $("#submit-form-button").css("display", "none"); // Hide submit button When 'Prev' button is clicked, just in case it is showing...


        });
    }
}

if(document.getElementsByClassName("btn-next") != undefined) {

    var btn_next = document.getElementsByClassName("btn-next");
    var count = btn_next.length;
    for (var b = 0; b < count; b++) {

        btn_next[b].addEventListener("click", function () {
           // console.log("Button clicked id: " + this.id);

            var this_num = (+this.id.substr(5));
            var num = (+this.id.substr(5) + 1);
            var this_section = "section-body-" + this_num;
            var next_section = "section-body-" + num;
            //console.log("Next section should be id: " + next_section);
            //$("#" + next_section).css("display","block");
            $("#" + this_section).css("display", "none");

            $('html, body').animate({
                //scrollTop: $("#analysis_form").offset().top
                scrollTop: $("#section-start-" + num).offset().top
            }, 1000);
            $("#" + next_section).slideDown(1500);
            $(this).css("display", "none");
            //console.log("b variable count: " + count + " | num count: " + num);

            if (num > count) {
                $("#submit-form-button").css("display", "block"); // show submit button if we have reached the final section...
            }

        });
    }
}

function process_analysis(){

    var checkbox = this.id.slice(0, -4);
    if(!$('#'+ checkbox).prop('checked')){
        //alert("Was NOT already checked");
        $(this).css({"background": "#b3ffb3", "color": "#006600", "border": "2px solid #006600"});
        $(this).html("Yes") ;
        //find_check.prop('checked', true);
        $('#'+ checkbox).prop('checked', true);
        //$(this).html("Not!") ;
       // console.log("Selecting checkbox on "+ this.id.slice(0, -4));
    }
    else{
        //alert("Trying to un-check!");
        $(this).css({"background": "#fff", "color" : "#495057", "border": "2px solid #e6e6e6"});

        $(this).html("No");
        $('#'+ checkbox).prop('checked', false);
        //$(this).html("Yes!");
      //  console.log("DE-Selecting checkbox on "+ this.id.slice(0, -4));
    }

}
// user-analysis-graph
if(document.getElementById('user-analysis-graph') != undefined){
    $('#user-analysis-graph').fadeIn('1000');
}

if(document.getElementById('results-barchart') != undefined){

    // Load google charts
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart_ajax);
}

var results_more = document.getElementsByClassName('results-more');
if(results_more.length > 0){


    //alert('results_more not undefined!')
    //var melancholic_more = document.getElementById('melancholic_more');
    var modal_more = document.getElementById('modal-more');
    modal_more.addEventListener("click", function () {

        for(var r = 0; r < results_more.length; r++){
            var id = results_more[r].id;
         //   console.log("DEBUG: Line 250: Hide id, " + id);
            $('#'+id).css("display", "none");
        }
        var href =$(this).attr('href');
        //alert("Melancholic more is here! "+ href)

        //$(href).css("display", "block");
        $(href).fadeIn(1200);
      //  console.log("** Displaying More results for: " + href + " ***");

        $('.analysis-score').html("Score: " + analysis_score_selected.toFixed(2) + "%");

        var colour_results= {

            sanguine_more: {background: colour_1, background_dark:colour_1_dark, color: '#fff'},
            melancholic_more: {background: colour_2, background_dark:colour_2_dark, color: '#fff'},

            phlegmatic_more: {background: colour_3, background_dark:colour_3_dark, color: '#fff'},
            choleric_more: {background: colour_4, background_dark:colour_4_dark, color: '#fff'},
        };

        switch(href) {
            case '#melancholic_more':
                colour_results_more(colour_results.melancholic_more);
                break;
            case '#phlegmatic_more':
                colour_results_more(colour_results.phlegmatic_more);
                break;
            case '#choleric_more':
                colour_results_more(colour_results.choleric_more);
                break;
            // case n:
            //     code block
            //     break;
            default:
                colour_results_more(colour_results.sanguine_more);
        }

        //console.log ("Colour results debug: + " + JSON.stringify(colour_results.eval(id)));
        //colour_results_more(colour_results.melancholic_more);
        $('#temperament_modal').modal('hide');
        // $('.modal-analysis-score').slideToggle();
    });
}
// else{
//     alert("DEBUG on knowso_analysis.js:260 | results_more not defined");
// }

function colour_results_more(colour_array){
   // console.log('** Changing card colour:'  + colour_array.background_dark + ' **')
    $('.card-analysis > .card-body').css({"background": colour_array.background, border: "2px solid " + colour_array.background_dark});
    $('.card-analysis').css({"background": colour_array.background, color:colour_array.color, border: "3px solid " + colour_array.background_dark} );
}

// Draw the chart and set the chart values
function drawChart_ajax() {

    $.ajax({

        url: "/get_personality_results",
        //
        // data: analysis_data,
        dataType:'json',
        success: function (id_data) {

            drawChart_code(id_data);

        },
        error: function (data,status, response) {

            //console.log("Error - data:\n " + JSON.stringify(data));
            console.log("Get results Error - status:\n " + JSON.stringify(status));
            console.log("Get Results Error - response:\n " + JSON.stringify(response));

            return JSON.stringify(response);

        }
    });


}

function drawChart_code(id_data){


    var data = new google.visualization.DataTable(id_data);
    //data.columns = [];

    data.addColumn('string', 'Temperament');
    data.addColumn('number','Score (%)');
    // data.addColumn('string', 'percent');

    data.addColumn({type: 'string', role: 'style'});

    // data.addColumn('string','Color');
    var total_score = (id_data.sanguine + id_data.melancholic + id_data.phlegmatic + id_data.choleric);

    //var score_info = row_title + '_score_info';
    //alert("Tendency 1:\n" + id_data.sanguine_score_info);
    $('#sanguine_score_info').html(id_data.sanguine_score_info);
    $('#melancholic_score_info').html(id_data.melancholic_score_info);
    $('#phlegmatic_score_info').html(id_data.phlegmatic_score_info);
    $('#choleric_score_info').html(id_data.choleric_score_info);
    //alert("Tendency 2:\n"+ id_data[score_info]);
    //calculate percetages for each score

    // ++ format decimal places to 2 and turn back into a number.
    var sanguine_percent = Number((id_data.sanguine / total_score * 100).toFixed(2));
    var melancholic_percent = Number((id_data.melancholic / total_score * 100).toFixed(2));
    var phlegmatic_percent = Number((id_data.phlegmatic / total_score * 100).toFixed(2));
    var choleric_percent = Number((id_data.choleric / total_score * 100).toFixed(2));
   // console.log('total scores: ' + total_score);

    data.addRows([
        ['Sanguine', sanguine_percent, 'color: ' + colour_1],
        ['Melancholic',  melancholic_percent, 'color: ' + colour_2],
        ['Phlegmatic',  phlegmatic_percent, 'color:' + colour_3],
        ['Choleric', choleric_percent, 'color: ' + colour_4]
    ]);


    // Optional; add a title and set the width and height of the chart
    //var options = {'title':'My Average Day', 'width':400, 'height':300};
    var options = {
        // title: 'Company Performance',
        'width':'auto',
        'height':400,
        vAxis: {gridlines:{color: '#ffc', count: 0}},

        //chartArea:{fill:'#ccc'},
       hAxis: { textStyle:{color:'#e6f2ff'}},
       //hAxis: {gridlinesColor:'#66b3ff'},
        // 'colors': ['#66b3ff', '#e6693e', '#0080ff', '#f3b49f'],
        chartArea: {top: 0,backgroundColor:'#66b3ff',width: '100%', height:'70%'},
        backgroundColor: {fill:'#66b3ff'},
        bar: {groupWidth: "96%"},
        // Score:{format:'#%'}
    };

    // Display the chart inside the <div> element with id="piechart"
    var chart = new google.visualization.ColumnChart(document.getElementById('results-barchart'));
    chart.draw(data, options);
    var prev_selection;

    google.visualization.events.addListener(chart, 'select', function (id_data) {
        var selection = chart.getSelection();

        //console.log("++ Listening for selection: "+ JSON.stringify(selection) + " +++");

        // If same bar of chart is clicked on twice in a row by the user, selection[0].row will empty,...
        // ...so 'get_row' var will not fill and fatal JS Error occurs. Stop error using code below!..
        if(selection[0] !== undefined){
            var get_row = selection[0].row;
            prev_selection = get_row;
        }
        else{
            get_row = prev_selection;
        }

        var row_title = data.getValue(get_row, 0);
        analysis_score_selected = data.getValue(get_row, 1);
        //alert('stringify Row selection ' + JSON.stringify(data.getValue(get_row, 1)) );
        $('.modal-title').html(row_title);

        var body_text = get_temperament_text(row_title);
        $('.modal-para').html(body_text.text);

        if(body_text.target !== undefined){
          //  console.log("Changing target: " + body_text.target );
            $('#modal-more').attr("href", "#" + body_text.target);
        }
        $('.temp-modal-content').css({background:body_text.background, color:body_text.color})
        // modal popup of brief temperament description, adding score and sliding down...

        $('.modal-analysis-score').text("Score: " + analysis_score_selected.toFixed(2) + "%").slideDown();
        $('#temperament_modal').modal('toggle');
        //$('.modal-analysis-score').slideDown();
        //alert('A table row was selected\n' + data.getValue(get_row, 0));
    });

    // $("#temperament_modal").on("hidden.bs.modal", function () {
    //     $('.modal-analysis-score').slideUp();
    // });


    $(window).resize(function(){
        // drawChart1();
        // drawChart2();
        chart.draw(data, options);
        //google.visualization.events.addListener(chart, 'click', selectHandler);
    });
}

// function get_score_info(score, section){
//
//     switch (true){
//         case score < 10:
//             return"Your current scores indicate that you do not have traits that are particularly similar to the " + section + " temperament";
//            // break;
//         case score < 25:
//             return"Your current scores would indicate that you have a few tendencies towards the " + section + " temperament";
//         case score < 35:
//             return"Your current scores indicate that the " + section + " temperament would apply to you in a significant range of areas";
//         case score < 45:
//             return"Your current scores indicate that you have a strong and significant leaning to the " + section + " temperament";
//         default:
//             // anything higher than 45 out of 100
//             return"Your current scores indicate that your personality leans very strongly towards the " + section + " temperament";
//     }
// }

// fill Modal box with appropriate content when Temperament results Graph column is clicked
function get_temperament_text(select_data){
    // 'text' current from : http://fourtemperaments.com/4-primary-temperaments/
    data = [];
    data.sanguine ={text:"Sanguines are naturally people-orientated. They have an active, positive movement in a favorable environment. They influence their environment by encouraging others to work together.",
        background: colour_1, color:"#fff", target:"sanguine_more"};
    data.choleric ={text:" Cholerics are naturally result-orientated. They have active, positive, and forward movement, in an antagonistic environment. They influence their environment by overcoming opposition to get results.",
        background:colour_4, color:"#fff", target:"choleric_more"};//"I am choleric";
    data.phlegmatic ={text:"Phlegmatics live a quiet, routine life free of the normal anxieties of the other temperaments. They avoid getting too involved with people, and life in general, preferring a private, low-key life-style, centered around home and family",
        background:colour_3, color:"#fff", target:"phlegmatic_more"};//"I am phlegmatic";
    data.melancholic ={text:"The Melancholy naturally wants to do things right, and is quality-orientated. Melancholies are not trying to be right, they are driven to figure out what is right. They have a cautious, tentative response designed to reduce tension in an unfavorable environment.",
        background:colour_2, color:"#fff", target:"melancholic_more"};//"I am melancholic";

    if(select_data != "" || select_data !== undefined){
        var select_lower = select_data.toLowerCase();
        return data[select_lower]; // return JS object detailing appropriate response according to which temperament bar has been clicked!
    }

    return false;
}


function get_personality_data(){
    // $.ajax({
    //
    //     url: "/get_personality_results",
    //     //
    //     // data: analysis_data,
    //     dataType:'json',
    //     success: function (data) {
    //         //alert('ok:\n' + data);
    //
    //         // if(data.error){
    //         //     //alert("Error: \n" + data.error);
    //         //     $('.modal-title').html("Error");
    //         //     $('#saveResultsModal-body').html(data.error);
    //         //     $('#saveResultsModal').modal('toggle');
    //         // }
    //         // else{
    //         //     //alert(JSON.stringify(data));
    //         //     data_saved(data); // process saving of data
    //         // }
    //         //
    //         //
    //         //
    //         //alert(JSON.stringify(data));
    //         return data;
    //
    //
    //     },
    //     error: function (data,status, response) {
    //
    //         //console.log("Error - data:\n " + JSON.stringify(data));
    //         console.log("Get results Error - status:\n " + JSON.stringify(status));
    //         console.log("Get Results Error - response:\n " + JSON.stringify(response));
    //
    //         return JSON.stringify(response);
    //
    //     }
    // });
}
