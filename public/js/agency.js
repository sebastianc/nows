(function($) {
  "use strict"; // Start of use strict

  // Smooth scrolling using jQuery easing
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: (target.offset().top - 54)
        }, 1000, "easeInOutExpo");
        return false;
      }
    }
  });

  // Closes responsive menu when a scroll trigger link is clicked
  $('.js-scroll-trigger').click(function() {
    $('.navbar-collapse').collapse('hide');
  });

  // Activate scrollspy to add active class to navbar items on scroll
  $('body').scrollspy({
    target: '#mainNav',
    offset: 56
  });

  // Collapse Navbar
  var navbarCollapse = function() {
    if ($("#mainNav").offset().top > 100) {
      $("#mainNav").addClass("navbar-shrink");
    } else {
      $("#mainNav").removeClass("navbar-shrink");
    }
  };
  // // Collapse now if page is not at top
  // navbarCollapse();
  // // Collapse the navbar when page is scrolled
  // $(window).scroll(navbarCollapse);

  // Hide navbar when modals trigger
  $('.portfolio-modal').on('show.bs.modal', function(e) {
    $(".navbar").addClass("d-none");
  })
  $('.portfolio-modal').on('hidden.bs.modal', function(e) {
    $(".navbar").removeClass("d-none");
  })

})(jQuery); // End of use strict

/* Extra code added by Colin */
$(function() {

    $('[data-toggle="tooltip"]').tooltip();

    /* Cookie warning - START */
    function createCookie(name, value, days, path) {
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            var expires = "; expires=" + date.toGMTString();
        }
        else var expires = "";
        document.cookie = name + "=" + value + expires + "; path=" + path;
    }

    function readCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
        }
        return null;
    }

    var cookieMessage = document.getElementById('cookie-message');
    if (cookieMessage == null) {
        return;
    }
    var cookie = readCookie('seen-cookie-message');
    if (cookie != null && cookie == 'yes') {
        cookieMessage.style.display = 'none';
    } else {
        //
        $("#cookie-message").slideDown('slow');
        //cookieMessage.style.display = 'block';
    }

    // Set/update cookie
    var cookieExpiry = cookieMessage.getAttribute('data-cookie-expiry');
    if (cookieExpiry == null) {
        cookieExpiry = 30;
    }
    var cookiePath = cookieMessage.getAttribute('data-cookie-path');
    if (cookiePath == null) {
        cookiePath = "/";
    }

    $("#got-it").click(function () {

        $("#cookie-message").slideUp('slow');
        createCookie('seen-cookie-message', 'yes', cookieExpiry, cookiePath);

    });

    /* Cookie warning - END */

});

// Closes respective popup
$(".close-fb").click(function() {
    $('#fb-tab').toggle();
    $('.row2').toggle();
    $('#in-tab').hide();
    $('#tw-tab').hide();
    $('#id-tab').hide();
    $('#jobs-tab').hide();
    $('#mo-tab').hide();
    //$('#bogus').toggle();
});

$('.close-in').click(function() {
    $('#in-tab').toggle();
    $('.row2').toggle();
    $('#fb-tab').hide();
    $('#tw-tab').hide();
    $('#id-tab').hide();
    $('#jobs-tab').hide();
    $('#mo-tab').hide();
    //$('#bogus').toggle();
});

$('.close-tw').click(function() {
    $('#tw-tab').toggle();
    $('.row2').toggle();
    $('#fb-tab').hide();
    $('#in-tab').hide();
    $('#id-tab').hide();
    $('#jobs-tab').hide();
    $('#mo-tab').hide();
    //$('#bogus').toggle();
});

$('.close-id').click(function() {
    $('#id-tab').toggle();
    $('.row3').toggle();
    $('#fb-tab').hide();
    $('#in-tab').hide();
    $('#tw-tab').hide();
    $('#jobs-tab').hide();
    $('#mo-tab').hide();
    //$('#bogus').toggle();
});

$('.close-jobs').click(function() {
    $('#jobs-tab').toggle();
    $('.row3').toggle();
    $('#fb-tab').hide();
    $('#in-tab').hide();
    $('#tw-tab').hide();
    $('#id-tab').hide();
    $('#mo-tab').hide();
    //$('#bogus').toggle();
});

$('.close-mo').click(function() {
    $('#mo-tab').toggle();
    $('.row3').toggle();
    $('#fb-tab').hide();
    $('#in-tab').hide();
    $('#tw-tab').hide();
    $('#id-tab').hide();
    $('#jobs-tab').hide();
    //$('#bogus').toggle();
});

/*
 *   Stripe
 */
if (window.location.href.indexOf('/register') > 0) {
// Create a Stripe client.
    var stripe = Stripe('pk_test_x2JR2BoxGglJ0aUP0AnLDwnE00RaUGS2hZ');

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
    var style = {
        base: {
            color: '#32325d',
            fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
            fontSmoothing: 'antialiased',
            fontSize: '16px',
            '::placeholder': {
                color: '#aab7c4'
            }
        },
        invalid: {
            color: '#fa755a',
            iconColor: '#fa755a'
        }
    };


    $('#register-2').show();

// Create an instance of Elements.
    var elements = stripe.elements();

// Create an instance of the card Element.
    var card = elements.create('card', {style: style});

// Add an instance of the card Element into the `card-element` <div>.
    card.mount('#card-element');

// Handle real-time validation errors from the card Element.
    card.addEventListener('change', function (event) {
        var displayError = document.getElementById('card-errors');
        if (event.error) {
            displayError.textContent = event.error.message;
        } else {
            displayError.textContent = '';
        }
    });

    setTimeout(function () {
        $('#register-2').hide();
    }, 100);


// Handle form submission.
    var form = document.getElementById('payment-form');
    form.addEventListener('submit', function (event) {
        event.preventDefault();

        stripe.createToken(card).then(function (result) {
            if (result.error) {
                // Inform the user if there was an error.
                var errorElement = document.getElementById('card-errors');
                errorElement.textContent = result.error.message;
            } else {
                // Send the token to your server.
                stripeTokenHandler(result.token);
            }
        });
    });

// Submit the form with the token ID.
    function stripeTokenHandler(token) {
        // Insert the token ID into the form so it gets submitted to the server
        var form = document.getElementById('payment-form');
        var hiddenInput = document.createElement('input');
        hiddenInput.setAttribute('type', 'hidden');
        hiddenInput.setAttribute('name', 'stripeToken');
        hiddenInput.setAttribute('value', token.id);
        form.appendChild(hiddenInput);

        var email = document.createElement('input');
        email.setAttribute('type', 'hidden');
        email.setAttribute('name', 'stripeEmail');
        email.setAttribute('value', $('#email').val());
        form.appendChild(email);

        // Submit the form
        form.submit();
    }
}
