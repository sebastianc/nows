<?php

namespace App\Console;

use App\Console\Commands\CheckTrialsCommand;
use App\Console\Commands\RegistrationReport;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Facades\Log;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        CheckTrialsCommand::class,
        RegistrationReport::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //Log::info('schedule:run');

        $this->scheduleCheck($schedule);
        $this->scheduleReg($schedule);
    }

    private function scheduleCheck(Schedule $schedule)
    {
        $schedule->command('check:trials')
                ->daily()
                ->withoutOverlapping();
    }

    private function scheduleReg(Schedule $schedule)
    {
        $schedule->command('reg:report')
                //->weeklyOn(1, '7:30')
                ->dailyAt('7:00')
                ->withoutOverlapping();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
