<?php

namespace App\Console\Commands;

use App\Mail\ReportMail;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class RegistrationReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reg:report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check for affiliates stats and conversions after registration';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $date = date('Y-m-d H:i:s', time());
        Log::info('Registration report command on ' . $date);

        $dayToCheck = Carbon::now()->subDays(2);

        $users = DB::table('kn_users')
            ->select('aff_id',
                    DB::raw('sum(case when stripe_id IS NOT NULL then 1 else 0 end) AS `Signups`'),
                    DB::raw('sum(case when payment_date IS NOT NULL then 1 else 0 end) AS `TotalPayments`'),
                    DB::raw('sum(case when stripe_id = \'PaymentFailed\' then 1 else 0 end) AS `PaymentFailed`'),
                    DB::raw('sum(case when payment_date IS NULL AND cancelled = 1 then 1 else 0 end) AS `Cancelled`'),
                    DB::raw('sum(case when stripe_id IS NULL AND stripe_paid = 1 then 1 else 0 end) AS `FailedPayWall`'),
                    DB::raw('sum(case when verified = 1 then 1 else 0 end) AS `Verifiedemail`'),
                    DB::raw('sum(case when last_login IS NOT NULL then 1 else 0 end) AS `LoggedIn`')
            )
            ->whereNotNull('aff_id')
            ->whereDate('created_at', '>', $dayToCheck)
            ->groupBy('aff_id')
            ->get()->toArray();
        //dd($users);

        $spreadsheet = new Spreadsheet();

        //add some column titles in headers
        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Affiliate id')
            ->setCellValue('B1', 'Total Sign Ups')
            ->setCellValue('C1', 'Total Payments')
            ->setCellValue('D1', 'Payment Failed')
            ->setCellValue('E1', 'Cancelled')
            ->setCellValue('F1', 'Failed Pay Wall Step')
            ->setCellValue('G1', 'Verified email')
            ->setCellValue('H1', 'Logged In');

        foreach($users as $key => $user) {
            $spreadsheet->setActiveSheetIndex(0)
                        ->setCellValue('A'. ($key+2), $user->aff_id)
                        ->setCellValue('B'. ($key+2), $user->Signups)
                        ->setCellValue('C'. ($key+2), $user->TotalPayments)
                        ->setCellValue('D'. ($key+2), $user->PaymentFailed)
                        ->setCellValue('E'. ($key+2), $user->Cancelled)
                        ->setCellValue('F'. ($key+2), $user->FailedPayWall)
                        ->setCellValue('G'. ($key+2), $user->Verifiedemail)
                        ->setCellValue('H'. ($key+2), $user->LoggedIn);
        }

        $cell_st =[
            'font' =>['bold' => true,
                      'size' => 12
                    ],
        ];

        $spreadsheet->getActiveSheet()->getStyle('A1:H1')->applyFromArray($cell_st);

        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(16);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(16);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(16);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(16);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(16);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(21);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(16);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(16);

        $spreadsheet->getActiveSheet()->setTitle('Knowso Affiliate Report');

        $report = new Xlsx($spreadsheet);
        $report->save("output.xlsx");

        // Mail the report and attached Excel document to us

        $data= [
            'page_title' => 'Knowso Affiliate Registration Report on ' . $date,
            'title' => 'Knowso Affiliate Registration Report on ' . $date,
            'text1' => '',
            'text2' => '',
            'date'  => $date
        ];

        Mail::to(
                'sebastian.ciocan@mediablanket.co.uk',
                //'dev@mediablanket.co.uk',
                'andrew.cheltnam@mediablanket.co.uk',
                'anthony.bradley@mediablanket.co.uk'
            )
            ->send(new ReportMail($data, "output.xlsx"));

    }
}
