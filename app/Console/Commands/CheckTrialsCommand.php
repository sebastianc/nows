<?php

namespace App\Console\Commands;

use App\Mail\NoticeMail;
use App\User;
use App\Services\Payments\RegularPayment;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Notifications\Notifiable;

class CheckTrialsCommand extends Command
{
    use Notifiable;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:trials';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check for user trial periods ending';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        //Default is 5 trial days
        $trialDays = 5;
        $dayToCheck = Carbon::now()->subDays($trialDays);

        $users = User::whereNull('payment_date')
                ->whereDate('created_at', $dayToCheck->format('Y-m-d'))
                ->whereNull('cancelled_date')
                ->where('cancelled', 0)
                ->where('stripe_id', '<>', 'CKuser')
                ->where('stripe_id', '<>', 'PaymentFailed')
                ->whereNotNull('stripe_id')
                ->get();

        // Debug
        //$users = User::where('email','hovastank-t@yahoo.com')->get();
        //dd($users);

        Log::info('Check Trials command on ' . date('Y-m-d H:i:s', time()));

        foreach($users as $user) {

            $stripeData = (new RegularPayment())->post('', [
                'amount' => 39.99,
                'description' => 'Payment after trial for email '.$user->email,
                'member_email_address' => $user->email,
                'save_card' => 1
            ]);

            // Decoding due to highly custom Guzzle handler service
            $stripeData = json_decode(json_decode($stripeData->content()));

            /**
             * Stripe Status OK is wrong call, ERROR is failure
             */
            if (isset($stripeData->status) || !isset($stripeData->response_payload)) {
                //dd($stripeData);
                if($stripeData->status == "ERROR"){
                    if (isset($stripeData->data->message)) {
                        $error = $stripeData->data->message;
                    } elseif (isset($stripeData->data)) {
                        $error = $stripeData->data;
                    } else {
                        $error = "An unknown error has occurred";
                    }
                } elseif ($stripeData->status == "OK"){
                    $error = "A GET request was made instead of a POST because Stripe Service is called via https instead of http (or vice versa), or Url is wrong";
                }

                Log::info('PaymentFailed for user id '. $user->id . ' with error: ' . $error);

                $data= [
                    'page_title' => 'Knowso - Payment',
                    'title' => 'KnowSo Trial Ended, Activation Payment Failed for email: '.$user->email,
                    'text1' => 'Just to remind you, KnowSo gives you unique insights by letting you have:<br/>
                    <ul>
                        <li class="animated slideInLeft_center" style="animation-delay: 200ms"><i class="fa fa-check"></i>Unlimited access to your Social Report and score</li>
                        <li class="animated slideInLeft_center" style="animation-delay: 600ms"><i class="fa fa-check"></i>Unlimited advice on improving your social image</li>
                        <li class="animated slideInLeft_center" style="animation-delay: 1000ms"><i class="fa fa-check"></i>Free alerts</li>
                        <li class="animated slideInLeft_center" style="animation-delay: 1400ms"><i class="fa fa-check"></i>Professional support and advice</li>
                        <li class="animated slideInLeft_center" style="animation-delay: 1800ms"><i class="fa fa-check"></i>Stats on how Social Media might impact your ability to get a loan</li>
                    </ul>
                    <br/>Payment was attempted for '.$user->email.' ; Please contact us to activate your account with our Customer Services team<br/>',
                    'text2' => ''
                ];

                Mail::to($user->email)->send(new NoticeMail($user, $data));

                Log::info('sentPaymentFailedEmail for user '. $user->id);

                $user->stripe_id = 'PaymentFailed';
                $user->stripe_paid = 0;
                $user->payment_date = date('Y-m-d H:i:s', time());
                $user->save();

            } else {
                $user->stripe_id = $stripeData->response_payload->data->miscellaneous->customer;
                $user->stripe_paid = 1;
                $user->payment_date = date('Y-m-d H:i:s', time());
                $user->save();

                Log::info('PaymentSuccess for user id '. $user->id);
            }

        }

    }
}
