<?php

namespace App\Services\Payments;


use App\Services\HttpService;

class RegularPayment extends HttpService
{
    public function __construct()
    {
        $this->config = [
            'base_uri' => (env("APP_ENV") === "production") ? config('services.payment-service.production') : config('services.payment-service.development'),
            'headers' => ['Authorization' => 'Bearer '.env("PAYMENT_TOKEN"),
                'Accept' => 'application/json' ,
                'Content-Type' => 'application/json'
            ],
            'debugging_handler' => false
        ];
        parent::__construct();
    }

}
