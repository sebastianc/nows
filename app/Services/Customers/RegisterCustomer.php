<?php

namespace App\Services\Customers;


use App\Services\HttpService;

class RegisterCustomer extends HttpService
{
    public function __construct()
    {
        $this->config = [
            'base_uri' => (env("APP_ENV") === "production") ? config('services.register-stripe-service.production') : config('services.register-stripe-service.development'),
            'headers' => ['Authorization' => 'Bearer '.env("PAYMENT_TOKEN"),
                'Accept' => 'application/json' ,
                'Content-Type' => 'application/json'
            ],
            'debugging_handler' => false
        ];
        parent::__construct();
    }

}
