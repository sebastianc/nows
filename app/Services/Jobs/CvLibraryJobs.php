<?php

namespace App\Services\Payments;


use App\Services\MultiService;

class CvLibraryJobs extends MultiService
{
    public function __construct()
    {
        $this->config = [
            'jobs_list' => config('services.cv-library.get-jobs'),
            'view_job' => config('services.cv-library.view-job'),
            'headers' => [],
            'debugging_handler' => true
        ];
        parent::__construct();
    }

}