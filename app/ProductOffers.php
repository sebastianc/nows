<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductOffers extends Model
{
    protected $connection = 'mysql_dbsupaleads';

    protected $table = "tblProductOffers";


    public static function all_offers(){

        return self::all()
            ->where('active',1)
            ->where('deleted',0)
            ->where('list_top_overall', 1)
            ->sortBy('list_top_overall_index')
            ->toArray();

    }

    public static function eating_offers(){

        return self::all()
            ->where('active',1)->where('deleted',0)->where('list_eating',1)->sortBy('list_eating_index')->toArray();

    }

    public static function shopping_offers(){

        return self::all()
            ->where('active',1)->where('deleted',0)->where('list_shopping',1)->sortBy('list_shopping_index')->toArray();
    }

    public static function holiday_offers(){

        return self::all()
            ->where('active',1)->where('deleted',0)->where('list_holidays',1)->sortBy('list_holidays_index')->toArray();
    }

    public static function health_offers(){

        return self::all()
            ->where('active',1)
            ->where('deleted',0)->where('list_health_fitness',1)->sortBy('list_health_fitness_index')->toArray();
    }

//    public function index(){
//
//    }
}
