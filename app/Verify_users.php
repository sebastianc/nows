<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Verify_users extends Model
{
    protected $guarded = [];

    protected $table = "kn_verify_users";

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
