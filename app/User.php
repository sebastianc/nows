<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "kn_users";

//    protected $fillable = [
//        'name', 'email', 'password',
//    ];

//    protected $fillable = [
//        'name','first_name', 'last_name', 'email', 'password','date_of_birth','title','mobile_number','date_of_birth','house_number'
//        ,'house_name','flat_number','street_name','city','county','post_code','residential_status','education',
//        'employment_status' ,'marital_status', 'social_media_check_reason', 'car', 'last_login', 'stripe_id'
//    ];

    protected $guarded = ['id', 'remember_token'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    public function setEmailAttribute($email)
    {
        $this->attributes['email'] = strtolower($email);
    }

    public function verifyUser()
    {
        return $this->hasOne('App\Verify_user');
    }

    /* save social connection profile data retrieved from target Social Site API.
    [Social site id - user_access_token] */
    public function saveSocialConnection($id, $data = []){

        if(empty($data)|| !is_array($data) || empty($id)){
            return false;
        }
        return $this->where('id',$id)->update($data);
    }

    public function getSocialConnections($id){
//
//        if(empty($data)|| !is_array($data) || empty($id)){
//            return false;
//        }

        $user = $this->where('id',$id)->get()->toArray();
        return $user[0];

    }

    // remove connection based on supplied array (array names MUST mirror target DB table columns!)
    public function delSocialConnection($id, $data = []){

        if(!is_array($data) || empty($id)){
            //\Log::debug(__FUNCTION__. " | Returning false from first eval: id=".print_r($id, true)."\n | data \n".print_r($data, true)) ;
            return false;
        }

        foreach($data as $name => $val){
            if(!empty($val)){
               // \Log::debug(__FUNCTION__." | Name:$name is not empty ( $val ). Returning false") ;
                return false; // only run (blank columns) if supplied array has empty value
            }

            //\Log::debug(__FUNCTION__. " | Attempting to save:\n".print_r($data, true)) ;

            return $this->where('id',$id)->update($data);
        }

    }

    public function set_paypal_subs_to_inactive($email_address){
        if(empty($email_address)){
            return false;
        }
        return $this->where('email',$email_address)->update(['stripe_paid'=> 0]);
    }

    public function paypal_ipn_subs_update($subscr_id, $data){
        if(empty($data)){
            return false;
        }
        // Removed when switched to Stripe
        //return $this->where('paypal_subscr_id',$subscr_id)->update($data);
        return false;
    }


    // Removed when switched to Stripe : paypal_subscr_id
    public function set_paypal_subs_to_active($paypal_subscr_id, $data){
        if(empty($paypal_subscr_id) || empty($data)){
            return false;
        }

        /**
         * TO BE DONE!!!
         */
    }

    public function get_user_paypal_check($email_address){
        if(empty($email_address)){
            return false;
        }

        // Removed when switched to Stripe : paypal_subscr_id , paypal_subscr_suspended
        $select = ['stripe_paid', 'sub_date', 'payment_date', 'cancelled',
            'cancelled_date', 'expiry_date'];

        //$user_paypal = $this->where('email',$email_address)->select($select)->get()->toArray();

        //echo print_r($user_paypal, true);
        $user = $this->where('email',$email_address)->select($select)->get()->toArray();
        return $user[0];
    }
}
