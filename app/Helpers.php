<?php

use App\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * Check if the user is on CreditKnowledge and return result.
 *
 * @param string $email
 *
 * @return mixed
 */
function isCreditKnowledgeUser(string $email)
{

    if(isProductionEnvironment()){
        $connectionName = 'credit_knowledge';
    } else {
        $connectionName = 'credit_knowledge_dev';
    }

    //$user = DB::connection($connectionName)->select('select * from users where email = :email AND ( billing_status = "Trial" OR billing_status = "Subscribed" )', ['email' => $email]);
    // Same as above but looks cleaner
    $user = DB::connection($connectionName)
            ->table('users')
            ->select('*')
            ->where('email', '=', $email)
            ->where(function ($query) {
                $query->where('billing_status', '=', 'Trial')
                    ->orWhere('billing_status', '=', 'Subscribed');
            })
            ->get()
            ->toArray();

    if(!empty($user)){
        return $user;
    }
    return false;
}

/**
 * Check the application environment.
 *
 * @param string $environment
 *
 * @return boolean
 */
function isEnvironment(string $environment)
{
    return app()->environment($environment);
}

/**
 * Check if application is in production environment or not.
 * 
 * @return boolean
 */
function isProductionEnvironment()
{
    return isEnvironment('production');
}

/**
 * Check if application is in staging environment or not.
 * 
 * @return boolean
 */
function isStagingEnvironment()
{
    return isEnvironment('staging');
}

/**
 * Check if date is valid or not.
 * 
 * @param Carbon $date
 * 
 * @return boolean
 */
function isValidDate(Carbon $date)
{
    return !$date->lt(Carbon::minValue());
}

/**
 * Return formatted date|datetime.
 *
 * @param string $date
 * @param string $format
 *
 * @return string
 */
function formatDate(string $date, string $format = "d-m-Y")
{
    return Carbon::parse($date)->format($format);
}

/**
 * Return formatted datetime for database datetime column.
 *
 * @param string $format
 * @param string $date
 *
 * @return string
 */
function dbDateTimeFormatted(string $format, string $date)
{
    return Carbon::createFromFormat($format, $date)->format('Y-m-d H:i:s');
}

/**
 * Return formatted current date and time.
 *
 * @param string $format
 *
 * @return string
 */
function nowFormatted(string $format = 'Y-m-d H:i:s')
{
    return now()->format($format);
}

/**
 * Convert an array to string.
 *
 * @param array $array
 * @param string $glue
 *
 * @return string
 */
function arrayToString(array $array = [], string $glue = ", ")
{
    return collect($array)->implode($glue);
}

/**
 * Return CSV string of array values.
 *
 * @param Collection $collection
 * @param string $key
 *
 * @return string
 */
function getCSVString(Collection $collection, string $key)
{
    return arrayToString($collection->pluck($key)->all(), ',');
}

/**
 * Return storage app path.
 * 
 * @param string|null $path
 * 
 * @return string
 */
function storage_app_path(string $path = null)
{
    return storage_path('app/' . $path);
}

/**
 * Update multiple database records.
 * 
 * @param string $table
 * @param array $values
 * @param string $index
 * 
 * @return boolean
 */
function batchUpdate(string $table, array $values, string $index = 'id')
{
    $ids = $final = [];
    foreach ($values as $val) {
        $ids[] = $val[$index];
        foreach (array_keys($val) as $field) {
            if ($field !== $index) {
                $value = is_null($val[$field]) ? 'NULL' : '"' . $val[$field] . '"';
                $final[$field][] = 'WHEN `'. $index .'` = "' . $val[$index] . '" THEN ' . $value . ' ';
            }
        }
    }
    
    $cases = '';
    foreach ($final as $k => $v) {
        $cases .=  '`'. $k.'` = (CASE '. implode("\n", $v) . "\n" . 'ELSE `'.$k.'` END), ';
    }
    
    $query = "UPDATE `$table` SET " . substr($cases, 0, -2) . " WHERE `$index` IN(" . implode(',', $ids) . ");";
    return DB::statement($query);
}
