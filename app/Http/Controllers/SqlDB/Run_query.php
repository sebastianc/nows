<?php

namespace App\Http\Controllers\SqlDB;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class Run_query extends Controller
{

    private $errors = [];
    private $auth_error = true;

    public function index(){

        $this->check_auth();

        if(!empty($this->errors)){
            //echo __FUNCTION__ . print_r($this->errors, true);
            //echo "<br>".__FUNCTION__ . " - Auth error:  -" . print_r($this->auth_error, true);
            $errors = $this->return_errors();
            return view("sql/sql_query",['error'=> $errors, 'auth_error' => $this->auth_error]);
            //return $this->return_errors();
        }

        return view("sql/sql_query");
    }


    public function run_query(Request $data){

        //$errors = [];

        //$posted = $data->query_box;
        $sql_string = $data->query_box;

        if(empty($sql_string)){

            return back()->with('warning', "Query was empty!");

        }



        $res = $this->execute_query($sql_string);

        $errors = $this->return_errors();

        if(!empty($errors)){
            return back()->with(['error'=> $errors]);
        }

        $result = "<p>Attempted Query ($sql_string) returned following result: </p>";


        if(is_array($res)) {

            //echo "<p>Query:</p><pre>" . print_r($res, true)."</pre>"; exit;
            $result .="\n<table>\n";

            $title_head = "";
            $title_created = false;
            // BUILD TITLES
            foreach ($res as $object) {

                $count = count($object);
                $build_title = "";
                if(!$title_created){
                    $build_title .="<tr>\n";
                }

                foreach ($object as $name => $data) {
                    if($title_created){
                        continue 1;
                    }


//
                    if($name == $title_head){
                        \Log::debug("Title 1 ($title_head) appears to match name ($name)!");
                        $build_title ="<th colspan='$count'>$name</th>";
                        //$result .= $build_title;
                        break;
                    }
                    else{
                        $title_head = $name;
                        $build_title .="<th>$title_head</th>";
                        //$result .= "<p style='font-weight:600'>$title</p>";
                    }





                }

                if(!$title_created){
                    $build_title .="\n</tr>\n";
                }

                $title_created = true;

                $result .= $build_title;


                //$result .="<tr>";
                $build_rows = "";
                $title_repeat = false;
                //$count = count($object);
                foreach ($object as $name => $data) {
//
//                    if (empty($title)) {
//                        $title = $name;
//                        $result .="<th colspan='2'>$title</th>";
//                        //$result .= "<p style='font-weight:600'>$title</p>";
//                    }

//                    if($title == $name){
//                        \Log::debug("Title ($title) appears to match name ($name)!");
//                        $build_rows .= "<tr><td colspan='$count'> {$data}</td></tr>\n";
//                        $title_repeat = true;
//                    }
//                    else{
//                        //$result .= "$name : {$data}<br>";
//                        $build_rows .= "<td>{$data}</td>";
//                    }

                    $build_rows .= "<td>{$data}</td>";


                }

                if(!$title_repeat){
                    $result .= "<tr>\n{$build_rows}\n</tr>\n";
                }
                else{
                    $result .= $build_rows;
                }


                //$result .="</tr>";


            }
            $result .="</table>\n";
            //\Log::debug("Array returned:\n".print_r($res, true));
            return back()->with(['status'=> print_r($result, true)]); // if more substantial array / object data is processed.
        }

        \Log::debug("Res returned:\n".print_r($res, true));

        return back()->with(['status'=> print_r($res, true)]); // if only true or false returned.
        //}else{
            //$result .= print_r($res, true);
       // }






        //return back()->with(['error'=> "Test: Nothing happens yet!", 'status'=> print_r($posted, true)]);

    }

    private function execute_query($string){

        //$disallowed = "drop|truncate";
        $disallowed = "drop table|drop database|truncate";
        if(preg_match("/$disallowed/i", $string)){
            $this->errors[] = "Query ('$string'')contains disallowed commands:<br>Disallowed: [ $disallowed ]";

            return false;
        }

        try{
            //return DB::unprepared($string)->toArray();
            $res = DB::select(DB::raw($string));
            \Log::debug("Select RAW Query returned:\n".print_r($res, true));
            return $res;
        }
        catch(\Exception $e){
            //return "Error caught:<br>".$e->getMessage();
            $this->errors[] = "Error caught for query<br>['$string']:<br>".$e->getMessage();
            return false;
        }

    }

    private function check_auth(){

        $email = Auth::user()->email;
        if($email != "sebastian.ciocan@mediablanket.co.uk"){

            return $this->errors[] = "User is ". $email . " - Cannot continue!";
        }

        $this->auth_error = false;
    }

    private function return_errors(){
        return implode("<br>", $this->errors);

    }

    //return view("")
}
