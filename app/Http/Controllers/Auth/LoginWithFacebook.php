<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Socialite;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\User;

class LoginWithFacebook extends Controller
{
    public function RedirectToFB(Request $get){

        //echo "Came back from FB with input for:!<br><pre>".print_r($get->all(), true)."</pre>";exit;

        return Socialite::driver('facebook')->redirectUrl('http://'.$_SERVER['HTTP_HOST'].'/callback_from_fb')->redirect();

    }

    public function CallbackFromFB(Request $get){

        //echo "Came back from FB with input for:!<br><pre>".print_r($get->all(), true)."</pre>";exit;

        if($get->input('error')== "access_denied"){
            $denied_msg[] = "Error: ".ucwords(str_ireplace("_", " ",$get->input('error')));
            $denied_msg[] = "Reason: ".ucwords(str_ireplace("_", " ",$get->input('error_reason')));
            return redirect('login')->with(['warning' =>"Login Failed:<br>".implode(" | ", $denied_msg)]);
        }

        //echo "GOT THROUGH TO HERE AFTER LOGIN!<br><pre>".print_r($get->all(), true)."</pre>";exit;

        try {
            $user = Socialite::driver('facebook')->redirectUrl('http://'.$_SERVER['HTTP_HOST'].'/callback_from_fb')->user();
        } catch (Exception $e) {
            return redirect('login')->with(['warning' =>"Error logging in:<br>".$e->getMessage()]);
        }

        if(!empty($get->input('code'))){
            $hash_code = md5($get->input('code'));
        }
        else{
            redirect("login")->with(['warning' =>print_r($user, true)]);
        }

        //echo "Came back from FB with input for:!<br><pre>".print_r($get->all(), true)."</pre><pre>".print_r($user, true)."</pre>";exit;

        $authUser = $this->findOrCreateUser($user, $hash_code);

        if($authUser){
            \Log::Debug("Auth user is:".PHP_EOL . print_r($authUser, true));
            Auth::login($authUser, true);

            $user = User::find(Auth::id());
            if(!empty($user->last_login)){
                session(['last_login' => $user->last_login]);
            }

            $user->last_login = date("Y-m-d H:i:s");
            $user->save();
        }



        //return redirect()->route('dashboard');
        return redirect('dashboard')->with(['connected' =>true,'connected_msg' =>"Successful facebook login for '".$user->name."'"]);


        //echo "Came back from FB with input for:!<br><pre>".print_r($get->all(), true)."</pre><pre>".print_r($user, true)."</pre>";

    }

    private function findOrCreateUser($facebookUser, $hash_code)
    {
        $authUser = User::where('fb_login_id', $facebookUser->id)->first();

        if ($authUser){
            return $authUser;
        }

        $name_split = explode(" ", $facebookUser->name);
        //$first_name = "";
        //$last_name = "";

        $first_name = $name_split[0];
        if(count($name_split) > 1){
            $last_name = $name_split[1];
        }
        else{
            $last_name = $name_split[0];
        }

       $create_user = [
            'name' => $facebookUser->name,
            'first_name' => $first_name,
            'last_name' => $last_name,
            'email' => $facebookUser->email,
            'password' => $hash_code,
            'fb_access_token' => $facebookUser->token,
            'fb_login_id' => $facebookUser->id,
            'fb_user_id' => $facebookUser->id,
            'fb_user_connected' => "1",
            'verified' => "1",
//            'avatar' => $facebookUser->avatar
        ];

        $authUser = User::where('email', $facebookUser->email)->first();


        if ($authUser){

            unset($create_user['name']);
            unset($create_user['first_name']);
            unset($create_user['last_name']);
            unset($create_user['email']);
            unset($create_user['password']);
            //return $authUser;
            //return User::update($create_user);
            //echo "Can try and update!<br><pre>".print_r($authUser, true)."</pre>";exit;
            $res = DB::table('kn_users')
                ->where('id', $authUser['id'])
                ->update($create_user);

            return $authUser;
        }



        return User::create($create_user);
    }
}
