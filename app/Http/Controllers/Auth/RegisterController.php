<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\User_register;
use App\Verify_users;
use App\Mail\VerifyMail;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Auth\RegistersUsers;

use Illuminate\Http\Request;
//use Input;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers; // ++ By Colin: Will be overridden due to need to stop un-activated users from logging in:

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    private $mb_office_ip = []; // new April 2018. Array with IP's that decide what to display for MB staff on registration form

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        $this->mb_office_ip = [];
    }

    public function register_paypal(Request $request)
    {
        //return redirect("https://www.paypal.com/cgi-bin/webscr");
        //\Log::debug("Request: '".__FUNCTION__."'\n".print_r($request->all(), true));

        $res = $this->validator($request->all())->validate();

        $exclude_fields = ["_token", "return", "rm", "cancel_return","notify_url", "custom", "unique_id", "item_number", "cmd", 'password_confirmation',
            "hosted_button_id", "dob_day", "dob_month", "dob_year"];
        $reg_data = $request->except($exclude_fields);
        $reg_data['date_of_birth'] = $request->input('dob_day')."-".$request->input('dob_month')."-". $request->input('dob_year');
        $reg_data['name'] = trim($request->input('first_name')) . " ". trim($request->input('last_name'));
        $reg_data['stripe_id']= $request->input('custom');

        $reg_data['password'] = bcrypt($reg_data['password']); // don't forget to encrypt the password in the usual laravel way!...

        $user_pre_register = new User_register();

        foreach($reg_data as $field => $value){
            $user_pre_register->$field = $value;
        }
        \Log::debug("Save user pre register attempt in: '".__FUNCTION__."' returned:\n".print_r($user_pre_register->save(), true));
        //;
        return "Request: '".__FUNCTION__."'\n".print_r($res, true);
        //return "Request: '".__FUNCTION__."'\n".print_r($request->all(), true);


//        event(new Registered($user = $this->create($request->all())));
//
//        $this->guard()->login($user);
//
//        return $this->registered($request, $user)
//            ?: redirect($this->redirectPath());
    }

    public function showRegistrationForm_old($test_data ="")
    {
        $access_allowed = true;

        // only allow access to old registration from Mediablanket office (or selected other IP's for testing) - See constructor.
        if(in_array(request()->ip(), $this->mb_office_ip)){
            $access_allowed = true;
        }

        $date_values = $this->fill_date_values($test_data);

        $test_data = $this->fill_test_data($test_data); // will fill test_data with pre-pop info if $data === "test_data"

        return view('auth.register_knowso', ['test_data'=> $test_data, 'date_values' => $date_values, "access_allowed" => $access_allowed]);  // March 2018: Override usual login view.
    }

    public function showRegistrationForm_new($test_data_val ="")
    {

        $date_values = $this->fill_date_values($test_data_val);

        $test_data = $this->fill_test_data($test_data_val); // will fill test_data with pre-pop info if $data === "test_data"

        return view('auth.register_knowso_new', ['test_data'=> $test_data, 'date_values' => $date_values]);  // March 2018: Override usual login view.
    }

    /*
     * Formerly showRegistrationForm_paypal
     * If there are no parameters in /register request the route points here
     * Actual Registration route
    */
    public function showRegistrationForm($aid ="")
    {
        /*$mb_staff_warning = false;

        if(in_array(request()->ip(), $this->mb_office_ip)){
            $mb_staff_warning = "Your IP Address, '".request()->ip()."' appears to be a mediablanket office IP address: 
You can bypass the Paypal integrated registration process <a href=\"/register_mb\">here</a>.";
        }*/
        $date_values = $this->fill_date_values(null);
        /*  Endpoint secured by removing testing data
         *
        $test_data = $this->fill_test_data($test_data_val); // will fill test_data with pre-pop info if $data === "test_data"*/

        // Get unique_id for saving against user database record, checking for successful Paypal subs activation.
        $unique_id = empty(old('unique_id'))? uniqid("kn_") :old('unique_id');

        return view('auth.register_knowso_paypal', [
            'aid'=> $aid,
            'date_values' => $date_values,
            'unique_id' => $unique_id
        ]);  // March 2018: Override usual login view.

    }

    private function fill_date_values($test_data_val)
    {

        if($test_data_val == "test_data"){
            $day_test = "02";
            $month_test = "07";
            $year_test = "1998";
        }

        $date_values = [];
        $days = ""; $months = ""; $years = "";
        $months_text = ["", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        for($i = 1; $i < 32; $i++){
            $ipad = str_pad($i, 2, '0', STR_PAD_LEFT);
            $selected = (old('dob_day') == $ipad)?  " selected" : ((isset($day_test) && $day_test == $ipad && !old('dob_day'))? " selected": "");
            $days .= "<option value=\"$ipad\"{$selected}>$i</option>\n";
        }
        for($i = 1; $i < 13; $i++){
            $ipad = str_pad($i, 2, '0', STR_PAD_LEFT);
            $selected = (old('dob_month') == $ipad)?  " selected" : ((isset($month_test) && $month_test == $ipad && !old('dob_month'))? " selected": "");
            $months .= "<option value=\"$ipad\"{$selected}>{$months_text[$i]}</option>\n";
        }

        $upper = date("Y")-18;
        $lower = date("Y") - 90;
        for($i = $upper; $i > $lower; $i--){
            $selected = (old('dob_year') == $i)?  " selected" : ((isset($year_test) && $year_test == $i && !old('dob_year'))? " selected": "");
            $years .= "<option value=\"$i\"{$selected}>$i</option>\n";
        }

        $date_values['days'] = $days;
        $date_values['months'] = $months;
        $date_values['years'] = $years;

        return $date_values;
    }

    private function fill_test_data($test_data_val)
    {

        // usually, return empty values for the view under normal circumstances.
        if($test_data_val !== "test_data"){
            $data = new \stdClass();
            $data->title = "";
            $data->first_name = "";
            $data->last_name = "";
            $data->mobile_number = "";
            $data->house_number = "";
            $data->house_name = "";
            $data->flat_number = "";
            $data->street_name = "";
            $data->city = "";
            $data->county = "";
            $data->post_code = "";
            $data->residential_status = "";

            return $data;
        }
        // .. otherwise, fill test data
        $data = new \stdClass();
        $data->title = "mr";
        $data->first_name = "Joe";
        $data->last_name = "Bloggs";
        $data->mobile_number = "07593999999";
        $data->house_number = "3";
        $data->house_name = "PARSONAGE CHAMBERS";
        $data->flat_number = "";
        $data->street_name = "PARSONAGE";
        $data->city = "MANCHESTER";
        $data->county = "GREATER MANCHESTER";
        $data->post_code = "M3 2HW";
        $data->residential_status = "private_tenant";

        return $data;

    }

    protected function registered(Request $request, $user)
    {
        $this->guard()->logout();
        return redirect('/login')->with('status', 'We sent you an activation code. Check your email and click on the link to verify.');
    }


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $data['date_of_birth'] = $data['dob_day']."-".$data['dob_month']."-".$data['dob_year'];
        //\Log::debug("In Function:".__FUNCTION__ . "\nData: \n".print_r($data, true));

        return Validator::make($data, [
//            'name' => 'required|string|max:255',
            'title' => 'required|string|max:4',
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:kn_users',
            'password' => 'required|string|min:6|confirmed',
            'date_of_birth' => 'required | date_format:"d-m-Y"',
            'mobile_number' => 'required|string|max:11|min:11',
            'house_number' => 'required_without_all:house_name,flat_number|max:5',
            'house_name' => 'required_without_all:house_number,flat_number|max:100',
            'flat_number' => 'required_without_all:house_number,house_name|max:10',
            'street_name' => 'required|string|max:100',
            'city' => 'required|string|max:50',
            'county' => 'required|string|max:60',
            'post_code' => 'required|string|max:8',
            'residential_status' => 'required|string|max:50',
            'education' => 'required|string|max:50',
            'employment_status' => 'required|string|max:50',
            'marital_status' => 'required|string|max:50',
            'social_media_check_reason' => 'required|string|max:50',
//            'car' => 'required|string|max:50',

        ]);


    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $data['date_of_birth'] = $data['dob_day']."-".$data['dob_month']."-".$data['dob_year'];
        $user = User::create([
            'title' => $data['title'],
            'name' => trim($data['first_name']) . " ". trim($data['last_name']),
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'date_of_birth' => $data['date_of_birth'],
            'mobile_number' => $data['mobile_number'],
            'house_number' => $data['house_number'],
            'house_name' => $data['house_name'],
            'flat_number' => $data['flat_number'],
            'street_name' => $data['street_name'],
            'city' => $data['city'],
            'county' => $data['county'],
            'post_code' => $data['post_code'],
            'residential_status' => $data['residential_status'],
            'education' => $data['education'],
            'employment_status' => $data['employment_status'],
            'marital_status' => $data['marital_status'],
            'social_media_check_reason' => $data['social_media_check_reason'],
            'stripe_paid'=> 1,
//            'car' => $data['car']
        ]);

        \Log::debug("In Function:".__FUNCTION__ . "\nData: \n".print_r($data, true));

        $verifyUser = Verify_users::create([
            'user_id' => $user->id,
            'token' => str_random(40)
        ]);

        Mail::to($user->email)->send(new VerifyMail($user, $verifyUser));

        return $user;
    }

    public function verifyUser($token)
    {
        $verifyUser = Verify_users::where('token', $token)->first();
        if(isset($verifyUser) ){
            $user = $verifyUser->user;
            if(!is_object($user)){
                return redirect('/login')->with('warning', "Token is too old, please try to register again.");
            }
            if(!$user->verified) {
                $verifyUser->user->verified = 1;
                $verifyUser->user->save();
                $status = "Your e-mail is verified. You can now login.";
            }else{
                $status = "Your e-mail is already verified. You can now login.";
            }
        }else{
            return redirect('/login')->with('warning', "Sorry your email cannot be identified. Try to sign up again.");
        }

        return redirect('/login')->with('status', $status);
    }
}
