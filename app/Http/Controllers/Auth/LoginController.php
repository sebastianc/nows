<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Email\Email_alerts;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard_load';

    private $paypal_subs_active = false;
    private $paypal_subs_expired = false;

    private $paypal_details = [];

    private $email = '';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');

    }

    public function showLoginForm()
    {
        return view('auth.login_knowso'); // March 2018: Override usual login view.
    }

    use AuthenticatesUsers;

    /*
     * Intercept the process of authentication to check with CreditKnowledge if the user already exists
     */
    public function attemptLogin(Request $request)
    {
        $this->email = $request->input('email');

        $isCreditUser = isCreditKnowledgeUser($this->email);

        $checkExistingUser = User::where('email', $this->email)->first();

        if($isCreditUser !== false && empty($checkExistingUser)) {
            /*
             * Copy the user over from CreditKnowledge, parsing data changes between databases
             */

            $user = array_map(function($item) {
                return (array)$item;
            }, $isCreditUser);

            $user = $user[0];
            $user['name'] = $user['first_name'] . ' ' . $user['last_name'] ;

            $user['house_number'] = $user['building_number'];
            unset($user['building_number']);
            $user['house_name'] = $user['building_name'];
            unset($user['building_name']);
            $user['post_code'] = $user['postcode'];
            unset($user['postcode']);

            unset($user['id']);
            unset($user['cra_account_status']);
            unset($user['billing_status']);
            unset($user['billing_amount']);
            unset($user['billing_frequency']);
            unset($user['next_bill_date']);
            unset($user['next_report_date']);
            unset($user['validation_identifier']);
            unset($user['user_identifier']);
            unset($user['user_reference_number']);
            unset($user['alias']);
            unset($user['ip_address']);
            unset($user['terms_agreement']);
            unset($user['consent_internal']);
            unset($user['internal_call']);
            unset($user['internal_email']);
            unset($user['internal_sms']);
            unset($user['consent_third_party']);
            unset($user['consent_third_party']);
            unset($user['third_party_call']);
            unset($user['third_party_email']);
            unset($user['third_party_sms']);
            unset($user['email_verified_at']);
            unset($user['active']);
            unset($user['remember_token']);
            unset($user['updated_at']);

            $user['verified'] = 1;
            $user['stripe_paid'] = 1;
            $user['stripe_id'] = 'CKuser';
            $user['payment_date'] = date('Y-m-d H:i:s', time());

            $createdUser = User::create($user);

            /*
             * This raw query is preventing the auto-rehashing / resetting of the pass
             * */

            DB::table('kn_users')
                ->where('id', $createdUser->id)
                ->update(['password' => $user['password']]);

            // Log the user in
            //return redirect()->intended($this->redirectPath());
            $credentials = $this->credentials($request);
            $credentials['stripe_paid'] = 1; /** Only allow "stripe_paid" users to authenticate */
            return $this->guard()->attempt(
                $credentials,
                $request->filled('remember')
            );

        } elseif($isCreditUser !== false && $isCreditUser[0]->active == 0){
            // User has a declined payment or has unsubscribed from credit knowledge, show error

            // Change password for extra security in case execution stops
            $request->merge(['password' => 'reset']);
            return $this->sendUserExpiredResponse($request);
        }

        // Log the user in

        $credentials = $this->credentials($request);
        $credentials['stripe_paid'] = 1; /** Only allow "stripe_paid" users to authenticate */
        return $this->guard()->attempt(
            $credentials,
            $request->filled('remember')
        );
    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function sendUserExpiredResponse(Request $request)
    {
        throw ValidationException::withMessages([
            $this->username() => ['Log in failed! Your CreditKnowledge subscription status for '.$this->email.' needs renewal'],
        ]);
    }

    public function authenticated(Request $request, $user)
    {
        /*
         * Direct MB staff log in without payment, should block non MB Matching Facebook login
         */
        $email = $this->email;

        $mb_user = $this->check_mediablanket_email($email);

        $this->paypal_details = $this->check_subs_status($email);

        // Stop if no paypal details
        if (empty($this->paypal_details) && !$mb_user) {

            (new Email_alerts())->send_basic("Log in failed! Could not query the account status of user, $email Please try again"
                , "Knowso login: Paypal DB query failure");
            auth()->logout();
            return back()->with('warning', "Log in failed! Could not query the account status of user, $email Please try again");
        }

        if (!$this->paypal_details['stripe_paid'] && !$mb_user) {

            (new Email_alerts())->send_basic("<p>User login was attempted, but trial is over for " . $email .
                "</p><p>Expired on " . $this->paypal_details['expiry_date'] . "</p>"
                , "Knowso login: Subscription No longer active");
            auth()->logout();
            return back()->with('warning', 'Your trial is no longer active and has expired on ' . $this->paypal_details['expiry_date'] . '. 
        Contact our Customer Support team <a href="mailto:hello@mediablanket.co.uk">hello@mediablanket.co.uk</a> if you want to to retry payment');
        }

        if ($this->check_subs_expired($email)) {

            (new Email_alerts())->send_basic("<p>User login was attempted, the account was cancelled for " . $email .
                "</p><p>Cancelled on " . $this->paypal_details['cancelled_date'] . "</p>"
                , "Knowso login: Expired Subscription Login Attempt");
            auth()->logout();
            return back()->with('warning', 'Account cancelled ' . $this->paypal_details['expiry_date'] .
                '<br>Please email <a href="mailto:hello@mediablanket.co.uk">hello@mediablanket.co.uk</a> if you wish to activate your account in the future.');
        }

        // check if user has activated/verified account by clicking on the received email
        if (!$user->verified) {
            auth()->logout();
            return back()->with('warning', 'You need to confirm your account. We have sent you an activation code, please check your email.');
        }

        $get_user = User::find(Auth::id());
        if (!empty($get_user->last_login)) {
            session(['last_login' => $user->last_login]);
        }

        //$user->online = 1;

        // disable the timestamps before saving
        $get_user->last_login = date("Y-m-d H:i:s");
        $get_user->save();

        return redirect()->intended($this->redirectPath());
    }

    // ** Paypal Subscription checks | START **
    private function check_mediablanket_email($email)
    {
        /**
         * If user has a mediablanket email address, bypass these checks!
         */
        $exclude_email = ["awb1968@gmail.com"]; // exclude selected email addresses from checks.
        if(in_array($email, $exclude_email)){
            return true;
        }

        $res = stristr($email, "mediablanket.co.uk");
        if($res){
            //\Log::Debug("\nFind Mediablanket email result\n$res\n++++");
            return true;
        }

        return false;
    }

    private function check_subs_status($email)
    {
        /**
         * using either table columns
         * if expired, set 'cancelled' : 1 and 'cancelled_date' to date supplied by Paypal API query?
         */
        try{

            $check = (new User())->get_user_paypal_check($email);
            //\Log::Debug("Check subs status:\n ".print_r($check, true));
            return $check;
            //return $subs_status['stripe_paid'];
        }
        catch(\Exception $e){

            return false;
            $class = __CLASS__;
            $function = __FUNCTION__;
            \Log::Debug("In Class ($class->$function) could not retrieve subscription status [stripe_paid]");
        }

    }

    private function check_subs_active()
    {
        /**
         * Check that subscription is active if not (table column: stripe_paid). If not active, deny log in.
         */
    }

    private function check_subs_expired($email)
    {
        /**
         * Check if subscription cancelled (cancelled : 1).
         * If cancelled, check cancellation date (table column: cancelled_date). If expired, set table column:'stripe_paid'
         * to 0 and deny log in.
         */
        if($this->paypal_details['cancelled'] && $this->paypal_details['expiry_date'] < date("Y-m-d H:i:s")){
            //echo "\nPaypal subscription cancelled (".$this->paypal_details['cancelled'].") and paypal subs expiry date ( ".$this->paypal_details['expiry_date']." ) smaller than current time";
            \Log::debug("\nSubscription cancelled (".$this->paypal_details['cancelled'].") and 
            paypal subs expiry date ( ".$this->paypal_details['expiry_date']." ) smaller than current time for user, '".$email."''") ;
            return true; // user should be stopped from logging in due to subs expiry
        }

        return false; // otherwise, user should be ok to log in.
    }

    // ** Paypal Subscription checks | END **

}
