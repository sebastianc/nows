<?php

namespace App\Http\Controllers\Email;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Mail;

class Email_alerts extends Controller
{

    private $recipients = [];

    public function __construct()
    {
        $this->recipients = [
            "sebastian.ciocan@mediablanket.co.uk",
            "admin@knowso.co.uk",
        ];
    }

    public function send_basic($email_content = "Test message", $subject = "Knowso admin alert", $recipients_list = [])
    {

        if(empty($recipients_list)){
            $recipients_list = $this->recipients;
        }

        //echo "Attempting to send email\n";

        Mail::send('emails.knowso_email', array('email' => $email_content, 'recipients_list'=>$recipients_list), function($message) use ($recipients_list, $subject){

            $message->to($recipients_list)->subject($subject);

        });

    }
}
