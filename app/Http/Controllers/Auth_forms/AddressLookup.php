<?php

namespace App\Http\Controllers\Auth_forms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AddressLookup extends Controller
{
    public function search($postcode) {

        header('Content-type: application/json');

        $str_postcode = strtoupper($postcode);
        // $str_postcode = "AA1 1AA";

        $arr_response = array();

        $arr_post_variables = array();
        $arr_post_variables['postcode'] = $str_postcode;
        $arr_post_variables['key'] = 'd776c-003aa-1bb9d-00709';
        $arr_post_variables['response'] = 'paf_compact'; // [paf_compact|data_formatted]

        $str_data_to_post = http_build_query($arr_post_variables);

        error_log('Doing Live Address Lookup...');
        error_log('Request: ' . $str_data_to_post);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'pcls1.craftyclicks.co.uk/xml/rapidaddress?' . $str_data_to_post);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $xml = curl_exec($ch);
        curl_close($ch);

        $obj_xml = simplexml_load_string($xml);

        //error_log(print_r($obj_xml, true));

        $arr_response['status'] = 'SUCCESS';
        $arr_response['response'] = $obj_xml;

        $str_delevery_point_html = '';

        // print_r($obj_xml);
        // die();

        $street_name = "";
        $street_name_descriptor = "";

        if ( isset($obj_xml->address_data_paf_compact->thoroughfare->thoroughfare_name) ) {
            $street_name = $obj_xml->address_data_paf_compact->thoroughfare->thoroughfare_name;
        }

        if ( isset($obj_xml->address_data_paf_compact->thoroughfare->thoroughfare_descriptor) ) {
            $street_name_descriptor = $obj_xml->address_data_paf_compact->thoroughfare->thoroughfare_descriptor;
        }

        if (strlen($street_name_descriptor) > 0) {
            $street_name = $street_name . " " . $street_name_descriptor;
        }

        $town = $obj_xml->address_data_paf_compact->town;
        $county = $obj_xml->address_data_paf_compact->postal_county;
        $post_code = $obj_xml->address_data_paf_compact->postcode;



        /**
         * check for error
         */
        if (!isset($obj_xml->error_code)) {


            foreach ($obj_xml->address_data_paf_compact->thoroughfare->delivery_point as $obj_delivery_point) {

                $company_name = $obj_delivery_point->organisation_name;
                $building_number = $obj_delivery_point->building_number;
                $sub_building_name = $obj_delivery_point->sub_building_name;
                $building_name = $obj_delivery_point->building_name;

                $address_string = $company_name . " " . $sub_building_name . " " . $building_name . " " . $building_number . " " . $street_name . " " . $town;

                $str_delevery_point_html .= "
              <option value='$address_string' data-street_name='$street_name' data-town='$town' data-county='$county' data-postcode='$post_code' data-company_name='$company_name' data-building_number='$building_number' data-sub_building_name='$sub_building_name' data-building_name='$building_name'>$address_string</option>
            ";

            }

        } else {

            /**
             * if we die here the ajax will fail and ask them to enter manually
             */
            die();

        }

        $arr_response['delivery_point_select'] = "
          <select id='choose-address' style='max-width:100%'>
            <option value=''>Select Address</option>
            $str_delevery_point_html
            <option value=''>Address Not In List</option>
          </select>
        ";


        echo json_encode($arr_response);
        exit;

    }
}
