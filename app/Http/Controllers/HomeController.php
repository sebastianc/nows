<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Social_API\Instagram_API;
use App\Http\Controllers\Social_API\Twitter_API;
use App\Http\Controllers\Social_API\Facebook_API;
use App\Http\Controllers\Social_API\Linkedin_API;
//use Illuminate\Http\Request;
use App\Http\Controllers\Social_API\Twitter_API_Settings;
use App\Http\Controllers\Social_API\Twitterapiexchange;
use App\Services\Payments\CvLibraryJobs;
use Illuminate\Support\Facades\Auth;
//use Illuminate\Support\Facades\DB;

use Illuminate\Support\ServiceProvider;

use App\User;

require "Facebook/autoload.php";

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    private $err;

    // social media class profile builders | May 2018
    private $ins; // instagram
    private $fb; // facebook // ++ New (potentially) May 2018

    public function __construct()
    {
        $this->middleware('auth'); // while this is here, nothing can run if user session is not valid.
    }

    private function build_instagram_view($access_token)
    {
        return $this->ins->get_instagram_profile($access_token);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //unset($err);
        //view('dashboard', ['data'=>$data, 'fb_profile'=>$fb_profile, 'tw_profile'=> $tw_profile, 'li_profile'=> $li_profile, 'test_api'=>$test_api]);
        //return redirect('/dashboard_load');
        //redirect('/dashboard_load');
        //exit;

        $previous_url = strtolower(str_replace(url('/'), '', url()->previous()));

        //$display_header = "Welcome, " . ucfirst(Auth::user()->first_name);
        //$display_text = "Loading your profile. Please wait ..";
        if($previous_url == "/dashboard"){
            return redirect("/dashboard_load");
        }

        $test_api = false;
        if(filter_input(INPUT_SERVER, 'HTTP_HOST')=="knowso.local"){
            $test_api = true;
        }


        $fb = new Facebook_API();
        $tw = new Twitter_API();
        $li = new Linkedin_API();
        $this->ins = new Instagram_API();

        //$int_brands = $fb->get_international_brands();
        //echo "<p>International Brands:</p><pre>".print_r($int_brands, true)."</pre>";

        //echo "<p>Check facebook user:</p><pre>".print_r($fb->check_user("facebook"), true)."</pre>"; exit;


        $tw_set = new Twitter_API_Settings();

        //echo "Settings:<pre>".print_r($tw_set->get_settings(), true)."</pre>";exit;
        $settings = $tw_set->get_settings();


        $data = [];

        if(!empty(session('last_login'))){
            $data['last_login'] = date("d.m.Y", strtotime(session('last_login')));
            //echo "<p>Last log in:</p><pre>".print_r($data['last_login'], true)."</pre>"; exit;
        }
        else{
            $data['last_login'] = "";
        }


        $data['fb_connected'] = false; // facebook
        $data['li_connected'] = false; // linkedin
        $data['tw_connected'] = false; // twitter
        $data['ins_connected'] = false; // Instagram | Added: May2018

        $err = [];

        $user_id = Auth::user()->id; // get id of logged in knowso user
        $user = new User;
        $user_data = $user->getSocialConnections( $user_id ); // check connections against logged in user.
        //echo "<pre>". print_r($user_data, true)."</pre>"; exit;
        //$data['user_data'] = $user_data;

        //$fb_profile = "";
        $ins_profile = []; // initialise Instagram profile data array.

        //Commented as it caused 500 with latest Guzzle
        //$ins_profile = $this->build_instagram_view($user_data['ins_access_token']);

        //echo "<p>DEBUG | Instagram profile attempt:</p><pre>". print_r($ins_profile, true)."</pre>"; exit;

        $fb_profile = array();
        if(!empty($user_data['fb_access_token']) ){
            //echo "<pre>". print_r($user_data['fb_access_token'], true)."</pre>"; exit;
            //$fb_profile = $fb->get_facebook_connection($user_data['fb_access_token']);
            $fb_profile = $fb->get_facebook_profile($user_data['fb_access_token']);
            //echo "CLASS:".__CLASS__."FACEBOOK PROFILE<pre>". print_r($fb_profile, true)."</pre>"; exit;

            if(isset($fb_profile[0]) && isset($fb_profile[1]) && $fb_profile[0]=== false){

                //$err[] = "Facebook error:<br>".$fb_profile[1];
                //echo "User data: <pre>". print_r($user_data, true)."</pre>"; exit;
                $err[] = "<p style='margin:0'><span style='color:#4267b2; font-size:1.3rem'><i class='fa fa-facebook-square'></i>&#160;</span>Your Facebook profile access token has expired or access was revoked: 
                Please attempt to re-authorise when ready!</p>";

                //echo  $fb_profile[1]; exit;
            }elseif(!empty($fb_profile)){

                //$fb->get_trends();

                $fb_profile->most_vocab = "";

                $fb_profile->trends = "";
                $fb_profile->knowso_recommendations = [];
                $fb_profile->knowso_recommendations_words = [];
                $fb_profile->other_recommendations = [];
                $fb_profile->your_searches = [];


                if(isset($fb_profile->user['posts']['data'])) {
                    $facebook_posts_list = $fb->get_posts_info($fb_profile->user['posts']['data'], 'message');
                    //echo "FB posts list:<pre>". print_r($facebook_posts_list, true)."</pre>"; exit;
                } else {
                    $facebook_posts_list = [];
                }

                $fb_trends = $fb->get_trends($facebook_posts_list);

                if(!empty($fb_trends)){
                    $trends_list = array_slice(array_keys($fb_trends), 0, 5);
                    $fb_profile->trends = ucwords(implode(" | ", $trends_list));
                }

                $fb_rude_words = $fb->get_rude_words($facebook_posts_list);
                //echo "FB RuDE WORDS?:<pre>". print_r($fb_rude_words, true)."</pre>"; exit;

                if(!empty($fb_rude_words) && count($fb_rude_words) > 0){
                    $rude_list = array_slice(array_keys($fb_rude_words), 0, 5);
                    //echo "FB RuDE LIST?:<pre>". print_r($rude_list, true)."</pre>"; exit;
                    $fb_profile->knowso_recommendations[] = "Avoid using foul language";
                    $fb_profile->knowso_recommendations_words[] = "Foul language found: ".PHP_EOL .ucwords(implode(" | ", $rude_list));

                }

                $fb_drinking_words = $fb->get_drinking_words($facebook_posts_list);

                if(!empty($fb_drinking_words) && count($fb_drinking_words) > 0){

                    $drink_list = array_slice(array_keys($fb_drinking_words), 0, 5);
                    $fb_profile->knowso_recommendations[] = "Avoid posts about drinking excessively";
                    $fb_profile->knowso_recommendations_words[] = "Drink related words found: ".PHP_EOL .ucwords(implode(" | ", $drink_list));
                }

                if(!empty($facebook_posts_list)){
                    $fb_posts_words= $fb->get_words_count($facebook_posts_list, true);

                    $words_list = array_slice(array_keys($fb_posts_words), 0, 5);
                    //echo "<p>'FB Words list':</p><pre>".print_r($fb_posts_words, true)."</pre>";exit;
                    //echo "<p>'FB Words list':</p><pre>".print_r($words_list, true)."</pre>";exit;
                    $fb_profile->most_vocab = implode(" | ", $words_list);
                }

                if(empty($fb_profile->trends)){
                    $fb_profile->trends = " - None Found -";
                }

                // New - May 2018
                if(empty($fb_profile->your_searches)){
                    $fb_profile->your_searches = "<i class='fa fa-times fa_none' style=''></i> No searches currently available";
                }
                else{
                    $fb_profile->your_searches = implode("&#160;" , $fb_profile->your_searches);
                }

                if(empty($fb_profile->knowso_recommendations)){
                    $fb_profile->knowso_recommendations = "Your profile appears to be free of problem content.";
                    $fb_profile->knowso_recommendations_words = "No dodgy language or problem words";
                }
                else{
                    $fb_profile->knowso_recommendations = implode("<br>" , $fb_profile->knowso_recommendations);
                    $fb_profile->knowso_recommendations_words = implode(PHP_EOL , $fb_profile->knowso_recommendations_words);
                }

                // ++ TEST Other Recommendations - May 2018 ++ | START //
                //$fb_profile->other_recommendations[] = "<span class=\"other_rec\">
                //<a href=\"https://www.supacompare.co.uk/compare/compare-unsecured-loans\" target=\"_blank\">Loans</a></span>";
                /*
                $fb_profile->other_recommendations['short_term_loans'] = "<span class=\"other_rec\">
                <a href=\"https://www.supacompare.co.uk/compare/compare-short-term-loans\" target=\"_blank\">Short Term Loans</a></span>";
                //$fb_profile->other_recommendations[] = "<span class=\"other_rec\"><a href=\"/offers\">Offers</a></span>";
                //$fb_profile->other_recommendations[] = "<span class=\"other_rec\"><a href=\"https://www.supacompare.co.uk/compare/home-insurance\" target=\"_blank\">Home Insurance</a></span>";
                $fb_profile->other_recommendations['car_insurance'] = "<span class=\"other_rec\"><a href=\"https://www.supacompare.co.uk/compare/car-insurance-comparison\" target=\"_blank\">Car Insurance</a></span>";
                $fb_profile->other_recommendations['travel_insurance'] = "<span class=\"other_rec\"><a href=\"https://www.supacompare.co.uk/compare/travel-insurance\" target=\"_blank\">Travel Insurance</a></span>";
                */
                // END //

                $fb_profile->other_recommendations = $fb->get_other_recommendations($facebook_posts_list);

                // Added May 2018
                if(empty($fb_profile->other_recommendations)){
                    $fb_profile->other_recommendations = "<i class='fa fa-times fa_none' style=''></i> No recommendations found";
                }
                else{
                    $fb_profile->other_recommendations = implode("&#160;" , $fb_profile->other_recommendations);
                }

                $posts = 0;
                $friends = 0;
                if(isset($fb_profile->user['posts']['data'])){
                    $posts = count($fb_profile['posts']['data']);
                    $friends = $fb_profile['friends']['summary']['total_count'];
                    //\Log::debug("Function: '".__FUNCTION__."'' | ** POSTS COUNT:".$posts); exit;
                }

                $fb_profile->social_score = $fb->social_score_calculate(['posts'=> $posts, "friends"=> $friends]);

                //\Log::debug("Function: '".__FUNCTION__."'' | ** social score: ".$fb_profile->social_score); exit;

            }
        }

        $tw_profile = array();
        if(!empty($user_data['tw_access_token']) && !empty($user_data['tw_tokenSecret']) ){
            //echo "<pre>". print_r($user_data['fb_access_token'], true)."</pre>"; exit;
            $tw_profile = $tw->get_twitter_profile($user_data['tw_access_token'], $user_data['tw_tokenSecret']);

            if(isset($tw_profile[0]) && $tw_profile[0] == false ){

               // $err[] = "Twitter error:<br>".$tw_profile[1];
                $err[] = "<p style='margin:0'><span style='color:#1DA1F2;; font-size:1.3rem'><i class='fa fa-twitter-square'></i>&#160;</span>Your Twitter profile access token has expired or access was revoked: 
                Please attempt to re-authorise when ready!</p>";
                //return view('dashboard', ['data'=>$data, 'err'=> implode("<br>", $err)]);
            } else {

                $tw_posts_get = new Twitterapiexchange($settings);

                $url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';


                $requestMethod = 'GET';

                $tw_screen_name = $tw_profile->nickname;


                $tw_posts = json_decode($tw_posts_get->setGetfield("?screen_name={$tw_screen_name}&count=200")
                    ->buildOauth($url, $requestMethod)
                    ->performRequest(), true);

                $tw_profile->tweets = $tw_posts;

                if(!empty($tw_profile->tweets)){

                    $tw_profile->trends = " - None Found! -";
                    $tw_profile->knowso_recommendations = [];
                    $tw_profile->your_searches = [];

                    $tw_profile->knowso_recommendations_words = [];
                    $tw_profile->other_recommendations = [];

                    $tweets_list = $tw->get_tweet_info($tw_profile->tweets, 'text');
                    //echo "<p>'Twitter object':</p><pre>".print_r($tw_profile->tweets, true)."</pre>";exit;

                    $tw_profile->words = $tw->get_words_count($tweets_list);

                    $tw_profile->hashtag_words = $tw->get_words_count_specific($tw_profile->words, "#"); // get words with hash tag - Array already sorted.

                    $tw_profile->hashtag_words_list ="";

                    $hashtag_list = array_slice(array_keys($tw_profile->hashtag_words), 0, 5);
                    //echo "<p>'Array slice hashtag list':</p><pre>".print_r($hashtag_list, true)."</pre>";exit;
                    if(!empty($hashtag_list)){
                        $tw_profile->hashtag_words_list = implode(" ",$hashtag_list);
                    }
                    else{
                        $tw_profile->hashtag_words_list = "<i class='fa fa-times fa_none' style=''></i> No hashtags found yet";
                    }

                    $tw_rude_words = $tw->get_rude_words($tweets_list);

                    if(!empty($tw_rude_words) && count($tw_rude_words) > 0){

                        $rude_list = array_slice(array_keys($tw_rude_words), 0, 5);

                        $tw_profile->knowso_recommendations[] = "<i class='fa fa-warning fa_warn' style=''></i> Avoid using foul language";
                        $tw_profile->knowso_recommendations_words[] = "Foul language found: ".PHP_EOL .ucwords(implode(" | ", $rude_list));

                    }

                    $tw_drinking_words = $tw->get_drinking_words($tweets_list);

                    if(!empty($tw_drinking_words) && count($tw_drinking_words) > 0){

                        $drink_list = array_slice(array_keys($tw_drinking_words), 0, 5);
                        $tw_profile->knowso_recommendations[] = "<i class='fa fa-warning fa_warn' style=''></i> Avoid posting tweets about drinking excessively";
                        $tw_profile->knowso_recommendations_words[] = "Drink related words found: ".PHP_EOL .ucwords(implode(" | ", $drink_list));
                    }

                    $tw_trends = $tw->get_trends($tweets_list);


                    if(!empty($tw_trends)){
                        $trends_list = array_slice(array_keys($tw_trends), 0, 5);
                        $tw_profile->trends = ucwords(implode(" | ", $trends_list));
                    }
                    else{
                        $tw_profile->trends = "<i class='fa fa-times fa_none' style=''></i> No trends available";
                    }

                    if(empty($tw_profile->knowso_recommendations)){
                        $tw_profile->knowso_recommendations = "Your tweets appear to be free of problem content.";
                    }
                    else{
                        $tw_profile->knowso_recommendations = implode("<br>", $tw_profile->knowso_recommendations);
                    }

                    if(empty($tw_profile->knowso_recommendations_words)){
                        $tw_profile->knowso_recommendations_words = "No dodgy language or problem words";
                    }
                    else{
                        $tw_profile->knowso_recommendations_words = implode(PHP_EOL, $tw_profile->knowso_recommendations_words);
                    }

                    // Added May 2018
                    if(empty($tw_profile->your_searches)){
                        $tw_profile->your_searches = "<i class='fa fa-times fa_none' style=''></i> No search data";
                    }
                    else{
                        $tw_profile->your_searches = implode("&#160;" , $tw_profile->your_searches);
                    }

                    // Added May 2018
                    $tw_profile->other_recommendations = $tw->get_other_recommendations($tweets_list);

                    if(empty($tw_profile->other_recommendations)){
                        $tw_profile->other_recommendations = "<i class='fa fa-times fa_none' style=''></i> No recommendations found";
                    }
                    else{
                        $tw_profile->other_recommendations = implode("&#160;" , $tw_profile->other_recommendations);
                    }
                    $tweets = 0;
                    $following = 0;
                    $followers = 0;
                    $favourites = 0;
                    if(isset($tw_profile->user['statuses_count'])){
                        $tweets = $tw_profile->user['statuses_count'];
                        //echo "<p>'tweet count':</p><pre>$tweets</pre>"; exit;
                        $following = $tw_profile->user['friends_count'];
                        $followers = $tw_profile->user['followers_count'];
                        $favourites = $tw_profile->user['favourites_count'];

                    }

                    $tw_profile->social_score = $tw->social_score_calculate([
                        'tweets'=> $tweets,
                        "following"=> $following,
                        "followers"=> $followers,
                        "favourites"=> $favourites,
                    ]);

                    //echo "<p>'tweets list':</p><pre>".print_r($tweets_list, true)."</pre>";

                }

            }
        }

        $li_profile = array();
        if(!empty($user_data['li_access_token']) ){

            $li_profile = $li->get_linkedin_profile($user_data['li_access_token']);

            if(isset($li_profile[0]) && isset($li_profile[1]) && $li_profile[0]=== false){

                //$err[] = "LinkedIn error:<br>".$li_profile[1];
                $err[] = "<p style='margin:0'><span style='color:#0073b1; font-size:1.3rem'><i class='fa fa-linkedin-square'></i>&#160;</span>Your LinkedIn profile access token has expired or access was revoked: 
                Please attempt to re-authorise when ready!</p>";

                //echo  $li_profile[1]; exit;
            }else{
                //echo "<pre>". print_r($user_data['li_access_token'], true)."</pre>"; exit;

                // your_searches; jobs_recommended; people_may_know; knowso_recommendations; knowso_recommendations_words; other_recommendations;

                // Removing empty test data
                /*$li_profile->your_searches = []; $li_profile->jobs_recommended = []; $li_profile->people_may_know = [];
                $li_profile->knowso_recommendations = []; $li_profile->knowso_recommendations_words = []; $li_profile->other_recommendations = [];*/

                //dd($li_profile);

                // New - May 2018
                if(empty($li_profile->your_searches)){
                    $li_profile->your_searches = "<i class='fa fa-clock-o fa_none' style=''></i> Coming soon";
                }
                else{
                    $li_profile->your_searches = implode("&#160;" , $li_profile->your_searches);
                }

                if(empty($li_profile->user['location']['name'])){
                    $li_profile->location = "<i class='fa fa-clock-o fa_none' style=''></i> Coming Soon";
                }
                else{
                    $li_profile->location = $li_profile->user['location']['name'];
                }

                $li_profile->jobs_total = $li_profile->user['positions']['_total'];

                $li_profile->summary = (strlen($li_profile->user['summary']) > 700) ? 'Good' : 'Needs more text';

                $li_profile->score = $li_profile->user['numConnections'] * 100 / 500;

                $percent = $li_profile->score;

                $li_profile->score = (string) $li_profile->score . '%';

                if(empty($li_profile->people_may_know)){
                    $li_profile->people_may_know = "<i class='fa fa-clock-o fa_none' style=''></i> Coming Soon";
                }
                else{
                    $li_profile->people_may_know = implode("&#160;" , $li_profile->people_may_know);
                }

                if(empty($li_profile->knowso_recommendations)){
                    if($percent == 100){
                        $li_profile->knowso_recommendations = "Your profile appears to be free of problem content.";
                    } else {
                        $li_profile->knowso_recommendations = "You need to make more LinkedIn connections.";
                    }
                    $li_profile->knowso_recommendations_words = "No dodgy language or problem words";
                }
                else{
                    $li_profile->knowso_recommendations = implode("<br>" , $li_profile->knowso_recommendations);
                    $li_profile->knowso_recommendations_words = implode(PHP_EOL , $li_profile->knowso_recommendations_words);
                }

                // ++ Added May 2018
                if(empty($li_profile->other_recommendations)){
                    $li_profile->other_recommendations = "<i class='fa fa-times fa_none' style=''></i> No recommendations found";
                }
                else{
                    $li_profile->other_recommendations = implode("&#160;" , $li_profile->other_recommendations);
                }
            }

            //echo "CLASS:".__CLASS__."LINKEDIN PROFILE<pre>". print_r($li_profile, true)."</pre>"; exit;
        }

        if(isset($fb_profile['id'])){

            $data['fb_connected']= true;
        }

        if(isset($tw_profile->user['id_str'])){

            $data['tw_connected']= true;
        }

        if(isset($li_profile->user['id'])){

            $data['li_connected']= true;
        }

        if(isset($ins_profile->user['id'])){

            $data['ins_connected']= true;
        }

        $save_result = User::select('personality_analysis_data_1')->where('email',Auth::user()->email)->first();

        $jobsData = (new CvLibraryJobs())->get('', [
            'key' => '5A4C5Lf6ga-m7rGc',
            'geo' => Auth::user()->city,
            'distance' => 30,
            'tempperm' => 'Any',
            'posted' => 7
        ]);

        if(isset($jobsData->jobs) && count($jobsData->jobs) >= 8) {
            $sanguineJobs = [];
            $melancholicJobs = [];
            $phlegmaticJobs = [];
            $cholericJobs = [];
            $allJobs = [];

            $melancholicIndicators = [
                "methodic", "planning", "Executive", "discipline", "hard work", "oversee", "Fundraiser", "conscientious", "organize",
                "communication", "Manager", "careful", "plan", "organizational", "coordinate", "Lawyer", "goal", "improve", "monitor"
            ];
            $phlegmaticIndicators = [
                "earning", "high-paying", "growth", "interesting", "fun", "Caretaker", "Driving Instructor", "Game Designer", "Toy Designer",
                "Stunt", "CAR Mechanic", "food critic", "Sommelier", "advertising", "marketing", "social media", "biologist", "technician",
                "Mechanic", "fabricator", "Traffic Controller", "makeup", "event planner", "announcer", "producer", "veterinary technician",
                "detective", "brewmaster", "wine specialist"
            ];
            $cholericIndicators = [
                "Dietician", "Medical Records Technician", "Alternative Medicine", "Holistic Medicine", "Naturopath", "Photographer",
                "Massage Therapist", "Ergonomic Consultant", "Dog Sitter", "Dog Walker", "Zoologist", "Dog Trainer", "Groomer",
                "Biologist", "Ecologist", "Botanist", "Designer", "Social Media Manager", "Programmer", "Software Developer",
                "Artist", "Actor", "Musician", "Music Tutor", "Interior designer", "Fashion Designer", "Narrator", "Voiceover artist",
                "Writer", "Editor", "Proofreader", "Blogger", "Accountant", "Auditor", "Financial Analyst", "Controller", "Healthcare",
                "Purchaser", "Market Researcher", "Carpenter", "Electrician", "Plumber", "Gardener", "Landscaping", "Construction",
                "Life Coach", "Chef", "Therapist", "Psychologist", "School Counselor", "charity", "non-profit", "Researcher", "Farmer",
                "Travel agent", "Librarian", "Housekeeper", "Janitor", "Mailman", "Truck Driver", "Network Market", "Antiques Appraiser"
            ];

            $jobs = json_decode(json_encode($jobsData->jobs), true);

            foreach ($jobs as $job) {
                $check = 0;
                if(count($melancholicJobs) < 3) {
                    foreach ($melancholicIndicators as $mela) {
                        if (((stripos($job['hl_title'], $mela) !== false) || (stripos($job['description'], $mela) !== false)) && $check == 0) {
                            $job['description'] = substr($job['description'],0,290).'...';
                            $melancholicJobs[] = $job;
                            $check = 1;
                        }
                    }
                }
                if (!$check && (count($phlegmaticJobs) < 3)) {
                    foreach ($phlegmaticIndicators as $phl) {
                        if (((stripos($job['hl_title'], $phl) !== false) || (stripos($job['description'], $phl) !== false)) && $check == 0) {
                            $job['description'] = substr($job['description'],0,290).'...';
                            $phlegmaticJobs[] = $job;
                            $check = 1;
                        }
                    }
                }
                if (!$check && (count($cholericJobs) < 3)) {
                    foreach ($cholericIndicators as $cho) {
                        if (((stripos($job['hl_title'], $cho) !== false) || (stripos($job['description'], $cho) !== false)) && $check == 0) {
                            $job['description'] = substr($job['description'],0,290).'...';
                            $cholericJobs[] = $job;
                            $check = 1;
                        }
                    }
                }
                if (!$check && (count($sanguineJobs) < 3)) {
                    $job['description'] = substr($job['description'],0,290).'...';
                    $sanguineJobs[] = $job;
                }
            }

            if (empty($melancholicJobs)) {
                $jobs[2]['description'] = substr($jobs[2]['description'],0,290).'...';
                $jobs[3]['description'] = substr($jobs[3]['description'],0,290).'...';
                $melancholicJobs[] = $jobs[2] + $jobs[3];
            }

            if (empty($phlegmaticJobs)) {
                $jobs[4]['description'] = substr($jobs[4]['description'],0,290).'...';
                $jobs[5]['description'] = substr($jobs[5]['description'],0,290).'...';
                $phlegmaticJobs[] = $jobs[4] + $jobs[5];
            }

            if (empty($cholericJobs)) {
                $jobs[6]['description'] = substr($jobs[6]['description'],0,290).'...';
                $jobs[7]['description'] = substr($jobs[7]['description'],0,290).'...';
                $cholericJobs[] = $jobs[6] + $jobs[7];
            }

        } else {
            $sanguineJobs = [];
            $melancholicJobs = [];
            $phlegmaticJobs = [];
            $cholericJobs = [];
            $allJobs = [];
        }

        $allJobs['sanguine'] = $sanguineJobs;
        $allJobs['melancholic'] = $melancholicJobs;
        $allJobs['phlegmatic'] = $phlegmaticJobs;
        $allJobs['choleric'] = $cholericJobs;

        if(count($err) > 0){
            //echo "<pre>". print_r($err, true)."</pre>"; exit;
            return view('dashboard', ['data' => $data, 'err' => implode("\r", $err), 'allJobs' => $allJobs,
                'fb_profile' => $fb_profile, 'tw_profile' => $tw_profile, 'save_result' => $save_result,
                'li_profile' => $li_profile, 'ins_profile' => $ins_profile, 'test_api' => $test_api]);

        }

        return view('dashboard', ['data' => $data, 'fb_profile' => $fb_profile, 'tw_profile' => $tw_profile, 'save_result' => $save_result,
            'li_profile' => $li_profile, 'ins_profile' => $ins_profile, 'test_api' => $test_api, 'allJobs' => $allJobs]);

    }

}

// ** Ultimately delete use clauses..

//use App\Http\Controllers\Facebook;
//use App\Http\Controllers\Facebook_del\Exceptions\FacebookSDKException;
///use App\Http\Controllers\Facebook\Exceptions\FacebookResponseException;

//use App\Http\Controllers\Facebook\FacebookResponse;
