<?php
/**
 * Created by PhpStorm.
 * User: colino
 * Date: 09/03/18
 * Time: 11:16
 */

namespace App\Http\Controllers\Social_API;

use Illuminate\Support\Facades\Auth;
use App\User;


class Twitter_API extends Social_Connect_API {

//    private function connect_twitter(){
//
//    }

    public function disconnect_twitter(){

        $user_id = Auth::user()->id;
        $user = new User;

        $tw_details = [];
        $tw_details['tw_access_token'] = "";
        $tw_details['tw_tokenSecret'] = "";
        $tw_details['tw_user_id'] = "";
        $tw_details['tw_user_connected'] = 0;

        $res = $user->delSocialConnection($user_id, $tw_details);

        if($res){
            return back()->with(["disconnected" =>true, "disconnected_msg" => "Twitter connection removed"]);
        }

        return back()->with( ["disconnected" =>false,"disconnected_msg" =>"Twitter connection removal failed!"]);
    }

    public function redirect_to_tw(){

        //$this->get_twitter_profile();
        //https://stackoverflow.com/questions/30035023/how-do-i-reduce-the-socialite-scope-in-laravel-5#32804170


        return $this->redirectToProvider("twitter");
    }


    public function get_twitter_profile($token, $secret){


//        $this->fields = [
//            'id','name'
//        ];

        try{
            $tw_user = $this->getUserFromToken("twitter", $token, $secret);
            //echo "<p>Twitter user:<pre>".print_r($tw_user, true)."</pre>"; exit;
            return $tw_user;
        }
        catch(\Exception $e){
            return array(false, $e->getMessage()); // return array with 'false' in position [0] and error in [1] for evaluation...
        }

        //$user = $this->getUserFromToken($token, $secret);

        //echo __FUNCTION__." : Twitter returned: <pre>".print_r($user, true). "</pre>"; exit;
    }

    public function store_twitter_profile(){

        if(empty(session('user'))){
            return false;
        }

        $tw_user = session('user');

        //echo "<p>".__FUNCTION__. " : request: <pre>".print_r($tw_user, true)."</pre>"; exit;

        $tw_details = [];
        $tw_details['tw_access_token'] = $tw_user->token;
        $tw_details['tw_tokenSecret'] = $tw_user->tokenSecret;
        $tw_details['tw_user_id'] = $tw_user->user['id_str'];
        $tw_details['tw_user_connected'] = 1;

        $user_id = Auth::user()->id;
        //exit;
        $store = new User();
        $res = $store->saveSocialConnection($user_id, $tw_details);

        if($res){

            return Redirect("dashboard")->with(["connected" =>true, "connected_msg" => "Twitter Connection established"]);
        }

    }

    // ++ Tweet Information ++

    public function get_tweet_info($tweet_array, $text){

        return $this->get_raw_post_info($tweet_array, $text);

    }

    public function social_score_calculate($calc=[]){
        //\Log::debug("CLASS:".__CLASS__. " | Function: '".__FUNCTION__."' | ** calc array:\n". print_r($calc, true));
        return parent::social_score_calculate($calc);
    }
}