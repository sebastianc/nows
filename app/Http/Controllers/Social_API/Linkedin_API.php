<?php

namespace App\Http\Controllers\Social_API;

use Illuminate\Support\Facades\Auth;
use App\User;

class Linkedin_API extends Social_Connect_API
{

    public $fields;

    public function disconnect_linkedin()
    {
        $user_id = Auth::user()->id;
        $user = new User;

        $li_details = [];
        $li_details['li_access_token'] = "";
        //$tw_details['li_tokenSecret'] = "";
        $li_details['li_user_id'] = "";
        $li_details['li_user_connected'] = 0;

        $res = $user->delSocialConnection($user_id, $li_details);

        if($res){
            return back()->with(["disconnected" =>true, "disconnected_msg" => "LinkedIn connection removed"]);
        }

        return back()->with( ["disconnected" =>false,"disconnected_msg" =>"LinkedIn connection removal failed!"]);
    }

    public function get_linkedin_profile($token)
    {
        //$token = "856525660071878662-XXgmqwIgziPkG4744fM8W5SnyBrXUtR";
        //$secret = "GOoxP7ibQMC40tzUubyhuWGq6usOv9LXNp9aOhz9Nf1fJ";

        try{
            // send required fields for retrieval.
            $this->fields = [
                'id', 'first-name', 'last-name', 'formatted-name',
                'email-address', 'num-connections','num-connections-capped', 'positions','summary','headline', 'location', 'industry',
                'current-share','specialties','public-profile-url', 'picture-url', 'picture-urls::(original)',
                'api-standard-profile-request',
            ];
            $li_profile = $this->getUserFromToken("linkedin", $token);

            //\Log::debug("DEBUG LINKEDIN:" .$li_profile->getEmail());
            //\Log::debug("DEBUG LINKEDIN:" .$li_profile->getConnections());
            return $li_profile;
            //return $li_profile->getTokenFields("num-connections");
        }
        catch(\Exception $e){
            return array(false, $e->getMessage()); // return array with 'false' in position [0] and error in [1] for evaluation...
        }

        //$user = $this->getUserFromToken($token, $secret);

        //echo __FUNCTION__." : Twitter returned: <pre>".print_r($user, true). "</pre>"; exit;
    }

    public function redirect_to_li()
    {
        //https://stackoverflow.com/questions/30035023/how-do-i-reduce-the-socialite-scope-in-laravel-5#32804170

        //$scopes = ["r_fullprofile",  "r_emailaddress"];
        $scopes = [ "r_emailaddress","r_basicprofile"];

        return $this->redirectToProvider("linkedin", $scopes);
    }

    public function store_linkedin_profile()
    {
        if(empty(session('user'))){
            return false;
        }

        $li_user = session('user');

        //echo "<p>".__FUNCTION__. " : request: <pre>".print_r($li_user, true)."</pre>"; exit;

        $li_details = [];
        $li_details['li_access_token'] = $li_user->token;
        //$tw_details['tw_tokenSecret'] = $tw_user->tokenSecret;
        $li_details['li_user_id'] = $li_user->user['id'];
        $li_details['li_user_connected'] = 1;

        $user_id = Auth::user()->id;
        //exit;
        $store = new User();
        $res = $store->saveSocialConnection($user_id, $li_details);

        if($res){
            return Redirect("dashboard")->with(["connected" =>true, "connected_msg" => "Linkedin Connection established"]);
        }

    }

}