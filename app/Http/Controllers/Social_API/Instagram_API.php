<?php

namespace App\Http\Controllers\Social_API;
use Laravel\Socialite\Facades\Socialite;
//use SocialiteProviders\Instagram\Provider;

use Illuminate\Support\Facades\Auth;
use App\User;

use Illuminate\Http\Request;

class Instagram_API extends Social_Connect_API
{

    public function disconnect_instagram(){

        $user_id = Auth::user()->id;
        $user = new User;

        $ins_details = [];
        $ins_details['ins_access_token'] = "";
        //$tw_details['ins_tokenSecret'] = "";
        $ins_details['ins_user_id'] = "";
        $ins_details['ins_user_connected'] = 0;

        $res = $user->delSocialConnection($user_id, $ins_details);

        if($res){
            return back()->with(["disconnected" => true, "disconnected_msg" => "Instagram connection removed"]);
        }

        return back()->with( ["disconnected" => false,"disconnected_msg" =>"Instagram connection removal failed!"]);
    }

    public function get_instagram_profile($token){

        try{

            // send required fields for retrieval.
//            $this->fields = [
//                'id', 'first-name', 'last-name', 'formatted-name',
//                'email-address', 'num-connections','num-connections-capped', 'positions','summary','headline', 'location', 'industry',
//                'current-share','specialties','public-profile-url', 'picture-url', 'picture-urls::(original)',
//                'api-standard-profile-request',
//            ];
            $ins_profile = $this->getUserFromToken("instagram", $token);

            //\Log::debug("DEBUG LINKEDIN:" .$ins_profile->getEmail());
            //\Log::debug("DEBUG LINKEDIN:" .$ins_profile->getConnections());
            return $ins_profile;
            //return $ins_profile->getTokenFields("num-connections");
        }
        catch(\Exception $e){
            return array(false, $e->getMessage()); // return array with 'false' in position [0] and error in [1] for evaluation...
        }

    }

    public function redirect_to_ins(){

        //https://stackoverflow.com/questions/30035023/how-do-i-reduce-the-socialite-scope-in-laravel-5#32804170

        //$scopes = ["r_fullprofile",  "r_emailaddress","r_basicprofile"];
        $scopes = [ "r_emailaddress","r_basicprofile"];


        //return Socialite::driver('linkedin')->fields($fields);
        return Socialite::with('instagram')->redirect();

        //return $this->redirectToProvider("instagram", $scopes);
    }

    public function store_instagram_profile(){

        if(empty(session('user'))){
            return false;
        }

        $ins_user = session('user');

        //echo "<p>".__FUNCTION__. " : request: <pre>".print_r($li_user, true)."</pre>"; exit;

        $ins_details = [];
        $ins_details['ins_access_token'] = $ins_user->token; // ins_access_token (1000), ins_user_id (255), ins_user_connected tinyint(1)
        //$tw_details['tw_tokenSecret'] = $tw_user->tokenSecret;
        $ins_details['ins_user_id'] = $ins_user->user['id'];
        $ins_details['ins_user_connected'] = 1;

        $user_id = Auth::user()->id;
        //exit;
        $store = new User();
        $res = $store->saveSocialConnection($user_id, $ins_details);

        if($res){

            return Redirect("dashboard")->with(["connected" =>true, "connected_msg" => "Instagram Connection established"]);
        }

    }
}
