<?php
/**
 * Created by PhpStorm.
 * User: colino
 * Date: 16/03/18
 * Time: 11:16
 */

namespace App\Http\Controllers\Social_API;

use Illuminate\Support\Facades\Auth;
use App\User;

class Twitter_API_Settings{

    private $settings;

    public function __construct(){

        $this->set_settings();


    }

    private function set_settings(){

        /* settings for twitter account admin@knowso.co.uk (DevKnowso) */

        $tw_client_id = config('services.twitter.client_id');
        $tw_client_secret = config('services.twitter.client_secret');
        $tw_oauth_access_token = config('services.twitter.oauth_access_token');
        $tw_oauth_access_token_secret = config('services.twitter.oauth_access_token_secret');

        //echo "Services:<pre>".print_r($srv_tw, true)."</pre>"; exit;
        $this->settings = array(

            'consumer_key' => $tw_client_id,
            'consumer_secret' => $tw_client_secret,
            'oauth_access_token' => $tw_oauth_access_token,
            'oauth_access_token_secret' => $tw_oauth_access_token_secret,
        );

    }

    public function get_settings(){

        return $this->settings;
    }
}