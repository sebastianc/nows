<?php
/**
 * Created by PhpStorm.
 * User: colino
 * Date: 01/03/18
 * Time: 21:07
 *
 * Custom facebook class...
 */

namespace App\Http\Controllers\Social_API;

use Illuminate\Support\Facades\Auth;
use App\User;

//require "../Facebook/autoload.php"; // load Facebook Auth class;
require app_path()."/Http/Controllers/Facebook/autoload.php"; // load Facebook Auth class;

class Facebook_API extends Social_Connect_API {

//    public function __construct()
//    {
//        parent::__construct();
//
//        //\Log::debug( "Hello from: ".__CLASS__. PHP_EOL);
//    }

//    private function connect_facebook(){
//        $app_access_token = "1289232081174305";
//        $app_secret = "d57dc5dc1fe4925b0e371bb8d34714b4";
//
//        $this->fb_appsecret_proof = hash_hmac('sha256', $app_access_token, $app_secret);
//
//        return new \Facebook\Facebook([
//            'app_id' => $app_access_token,
//            'app_secret' => $app_secret,
//            'default_graph_version' => 'v2.12',
//            //'default_access_token' => '{access-token}', // optional
//        ]);
//
//        //return false;
//    }

    public function disconnect_facebook(){

        $user_id = Auth::user()->id;
        $user = new User;

        $data['fb_user_id'] = "";
        $data['fb_access_token'] = "";
        $data['fb_user_connected'] = "0";
        $res = $user->delSocialConnection($user_id, $data);

        //echo "Result of Disconnect: $res";

        if($res){
            return back()->with(["disconnected" =>true, "disconnected_msg" => "Facebook connection removed"]);
        }

        return back()->with( ["disconnected" =>false,"disconnected_msg" =>"Facebook connection removal failed!"]);

    }

    public function get_facebook_profile($token){

        //$token = "856525660071878662-XXgmqwIgziPkG4744fM8W5SnyBrXUtR";
        //$secret = "GOoxP7ibQMC40tzUubyhuWGq6usOv9LXNp9aOhz9Nf1fJ";

        try{

            // send required fields for retrieval.
            $this->fields = [
                'id','name','groups','gender','link','picture.type(large)','comments.limit(100)','posts.limit(250)','education','email','friends','age_range',
                'likes', 'verified'
            ];

            return $this->getUserFromToken("facebook", $token);

            //return $fb_profile;
            //return $li_profile->getTokenFields("num-connections");
        }
        catch(\Exception $e){
            return array(false, $e->getMessage()); // return array with 'false' in position [0] and error in [1] for evaluation...
        }

    }


    public function redirect_to_fb(){
        //https://stackoverflow.com/questions/30035023/how-do-i-reduce-the-socialite-scope-in-laravel-5#32804170
        $scopes = [
            'user_posts',
            'user_friends',
//            'https://www.googleapis.com/auth/plus.profile.emails.read'
        ];

        return $this->redirectToProvider("facebook", $scopes);
    }

    public function store_facebook_profile(){

        if(empty(session('user'))){
            return false;
        }

        //$fb = $this->connect_facebook();

        $fw_user = session('user');


        $fb_details = [];
        $fb_details['fb_access_token'] = $fw_user->token;
        $fb_details['fb_user_id'] = $fw_user->user['id'];
        $fb_details['fb_user_connected'] = 1;

        //echo "fb_details:<p>". print_r($fb_details, true)."</p>";

        //$store = new User();
        $user_id = Auth::user()->id;
        //exit;
        $store = new User();
        $res = $store->saveSocialConnection($user_id, $fb_details);
        if($res){

            return Redirect("dashboard")->with(["connected" =>true, "connected_msg" => "Facebook Connection established"]);
        }

        return Redirect("dashboard")->with(["connected" =>false, "connected_msg" => "Facebook Connection Failed!"]);
        //echo "<p>Result of store attempt: $store</p>";

    }

    // ++ FB User Posts Information ++

    public function get_posts_info($posts_array, $text){

        return $this->get_raw_post_info($posts_array, $text);

    }

    public function social_score_calculate($calc=[]){
       return parent::social_score_calculate($calc);
    }


}