<?php

namespace App\Http\Controllers\Social_API;

use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Auth;
//use Illuminate\Support\Facades\DB;
//use Illuminate\Support\Facades\Session;

//use App\User;
//use App\Http\Controllers\Facebook;

// Parent class for Social media connections
class Social_Connect_API{

    protected $fields; //  change fields that are requested from API on relevant child class.
    private $get_words_links;
    private $other_recommendations_links = [];

    public function __construct()
    {

        //\Log::debug("Hello from Parent:".__CLASS__ . PHP_EOL);
    }

    // New - 2018-05-18 - test
//    public function check_user($provider){
//        return Socialite::driver($provider)->user();
//    }

    protected function get_raw_post_info($post_array, $key_name = false)
    {
        if(!is_array($post_array) || empty($post_array)){
            \Log::debug("FUNCTION '".__FUNCTION__.":post_array failed:\n".print_r($post_array, true));
            return false;
        }

        $raw_array = [];

        if($key_name){
            foreach($post_array as $data){
                isset($data[$key_name])? $raw_array[] = $data[$key_name]:"";
            }

            return $raw_array;

        }

        foreach($post_array as $data){
            $raw_array[] = $data;
        }

        return $raw_array;

    }

    protected function getUserFromToken($provider, $token, $secret = ""){

        if(!empty($this->fields)){
            //\Log::debug("this->fields for provider, '$provider'' not empty!");

            if(empty($secret)){
                return Socialite::driver($provider)->fields($this->fields)->userFromToken($token);
            }

            return Socialite::driver($provider)->fields($this->fields)->userFromToken($token);
        }

        if(empty($secret)){
            return Socialite::driver($provider)->userFromToken($token);
        }

        return Socialite::driver($provider)->userFromTokenAndSecret($token, $secret);

    }

    protected function redirectToProvider($provider = "facebook", $scopes = [], $fields = [])
    {
        if(!empty($scopes) && !empty($fields)){
            \Log::debug("Scopes and fields for linked in not empty!");
            return Socialite::driver($provider)->scopes($scopes)->fields($fields)->redirect();
        }

        if(!empty($scopes)){

            \Log::debug("Scopes for linked in not empty!");
            return Socialite::driver($provider)->scopes($scopes)->redirect();
        }

        return Socialite::driver($provider)->redirect();

        //\Log::debug(__FUNCTION__. " | Redirect status:\n". print_r($redirect, true));
    }

    public function handleProviderCallback($provider = "facebook", Request $request)
    {

        // Linkedin - error handling
        if($provider == "linkedin"){
            $error = $this->handleProviderCallback_li($provider, $request);
            if($error){

                return redirect("dashboard")->with(["connected" =>false, "connected_msg" =>
                    "Error connecting to ". ucfirst($provider).":<br>".implode(" - ",$error) . "<br>Please try again when ready."]);

            }
        }

        // facebook - error handling
        if($request->input('error') == "access_denied"){

            $msg[] = "Error: ".$request->input('error') ;
            $msg[] = "Error Description: ".$request->input('error_description');
            $msg[] = "Reason: ".$request->input('error_reason');
            //echo "<p>".$request->input('error')."</p>"; exit;
            return redirect("dashboard")->with(["connected" =>false, "connected_msg" =>
                "Error connecting to ". ucfirst($provider).":<br>".implode(" - ",$msg) . "<br>Please try again when ready."]);
        }

        // twitter - error handling
        if($request->input('denied') ){

            $msg[] = "Error: ".$request->input('denied') ;

            return redirect("dashboard")->with(["connected" =>false, "connected_msg" =>
                "Error connecting to ". ucfirst($provider).":<br>".implode(" - ",$msg) . "<br>Please try again when ready."]);
        }

        // ** if we reach here, we can process the successful Social site connection / redirect

        /* If this fails with: Socialite \ Two \ InvalidStateException when connecting to facebook Api
        *  use Socialite::driver($provider)->stateless()->user()
         * Be sure to add stateless() to your redirect line as well or you may have trouble logging in after redirect.
         * Socialite::driver('google')->stateless()->redirect()
        */
        $user = Socialite::driver($provider)->user();

        //echo "<p>".__FUNCTION__. " : request: <pre>".print_r($user, true)."</pre>"; exit;


        return redirect ("store/$provider")->with(["user" => $user]);
        //echo "Access token: ".print_r($user, true);

        // $user->token;
    }

    // linkedin specific...
    private function handleProviderCallback_li($provider, $request){
        // error=user_cancelled
        if($request->input('error') ){

            $msg[] = "Error: ".$request->input('error') ;
            $msg[] = "Description: ".$request->input('error_description') ;
            return $msg;

        }

        return false;
    }

    // ++ Social Connection posts collation ++

    protected function read_international_brands(){

        return file(app_path()."/text_files/international_brands.txt");
    }

    protected function read_drinking_words(){

        return file(app_path()."/text_files/drinking_words");
    }

    protected function read_rude_words(){

        return file(app_path()."/text_files/rude_words");
    }

    // ++ Added May 2018 - Generic read...
    protected function read_word_file($file_name)
    {
        $file = app_path()."/text_files/$file_name";
        if(!file_exists($file)) {
            \Log::debug("Cannot find word file '$file_name' in function, ".__FUNCTION__);return false;
        }

        //return file($file);
        return file($file, FILE_IGNORE_NEW_LINES); // return array of words and do not add new lines to allow word search easily.
    }

    protected function search_word_file($words_array, $posts_list){
        if(!array($words_array))
        {\Log::debug("Function: '".__FUNCTION__ ."' words_array not an array");return [false];} // return empty/false array for evaluation
        if(!array($posts_list))
        {\Log::debug("Function: '".__FUNCTION__ ."' posts_list not an array");return [false];} // return empty/false array for evaluation

        $found_words = [];
        foreach($words_array as $word){
            foreach($posts_list as $post){
                $word_trim = trim($word);

                if(preg_match("/\b$word_trim\b/i", $post)){
                    $found_words[] = $word_trim;
                    //echo "<p>FOUND MATCH for '$word_trim'!</p>";
                }

            }

        }

        return $found_words;
    }

    protected function read_other_links($filename = 'recommend_valid_links' ){
        $links_array = [];
        $file_path = app_path()."/text_files/$filename";
        foreach(file($file_path) as $links_line){
            list($name, $value) = explode("==", $links_line);
            $links_array[$name] = $value;
        }

        //\Log::debug("** read_other_links array:\n". print_r($links_array, true)."\n***");

        return $links_array;
    }

    public function get_other_recommendations($posts_list = []){

        //$other_recommendations_links = [];

        $other_recommendations = $this->read_other_links();

        // get links for dashboard if possible based on content of text files with a similar path to '/text_files/recommend_*'
        $this->get_other_recommendations_links($other_recommendations, $posts_list, 'recommend_car_ins', 'car_insurance');
        $this->get_other_recommendations_links($other_recommendations, $posts_list, 'recommend_travel_ins', 'travel_insurance');
        $this->get_other_recommendations_links($other_recommendations, $posts_list, 'recommend_home_ins', 'home_insurance');
        $this->get_other_recommendations_links($other_recommendations, $posts_list, 'recommend_offers', 'offers');
        $this->get_other_recommendations_links($other_recommendations, $posts_list, 'recommend_short_loans', 'short_term_loans');
        $this->get_other_recommendations_links($other_recommendations, $posts_list, 'recommend_unsec_loans', 'unsecured_loans');
//
//        if(!empty($this->get_words_links['rec_offers'])&& is_array($this->get_words_links['rec_offers'])){
//            // select first array element (Links in array should be exactly same apart from the key name)
//            $other_recommendations_links[] = reset($this->get_words_links['rec_offers']);
//        }

        //return $get_words_count;
        return $this->other_recommendations_links; // return array of links or empty array, if applicable.
        //return $rec_car_ins;
    }

    private function get_other_recommendations_links($other_recommendations, $posts_list, $file_name, $link_name){

        $found_words = [];
        $words_list = $this->read_word_file($file_name);
        //$rec_travel_ins  = array_map('trim', $rec_travel_ins );  // remove line endings etc.

        $words_list_found_words = $this->search_word_file($words_list, $posts_list);
        //\Log::debug("Recommend Offers FOUND words:\n". print_r($words_list_found_words, true));
        if(!empty($words_list_found_words) && is_array($words_list_found_words)){
            $found_words = array_merge($found_words, $words_list_found_words);
        }

        //\Log::debug("Function: ".__FUNCTION__ ." | found_words array:\n". print_r($found_words, true));

        $get_words_count = $this->get_words_count($found_words, false, true);

        //foreach($get_words_count as $word => $count){
        //$get_words_links = [];

        if(is_array($get_words_count)){
            foreach($get_words_count as $word => $count){


                if(in_array($word, $words_list) && isset($other_recommendations[$link_name])){
                    //\Log::debug("Found '$word' in '$link_name' array");
                    $this->get_words_links[$link_name][$word] = $other_recommendations[$link_name];
                }
//            else{
//                \Log::debug("Function: ".__FUNCTION__ ." | Could not find '$word' in words list:\n". print_r($words_list, true));
//            }


            }
        }


        // start filling recommendation links array in order of most popular
        //$other_recommendations_links = [];


        if(!empty($this->get_words_links[$link_name])&& is_array($this->get_words_links[$link_name])){
            // select first array element (Links in array should be exactly same apart from the key name)
            $this->other_recommendations_links[] = reset($this->get_words_links[$link_name]);
        }

        //return $other_recommendations_links;

    }

    public function get_drinking_words($posts_list = []){

        $drinking_words = $this->read_drinking_words();
        $found_words = [];

        foreach($drinking_words as $word){
            foreach($posts_list as $post){
                $word_trim = trim($word);

                if(preg_match("/\b$word_trim\b/i", $post)){
                    $found_words[] = $word_trim;
                    //echo "<p>FOUND MATCH for '$word_trim'!</p>";
                }

            }

        }

        return $this->get_words_count($found_words, false, true);

    }

    public function get_rude_words($posts_list = []){

        $rude_words = $this->read_rude_words();
        //echo "<p>'Rude words':</p><pre>".print_r($rude_words, true)."</pre>"; exit;
        $found_words = [];

        foreach($rude_words as $word){
            foreach($posts_list as $post){
                $word_trim = trim($word);

                if(preg_match("/\b$word_trim\b/i", $post)){
                    $found_words[] = $word_trim;
                    //echo "<p>FOUND MATCH for '$word_trim'!</p>";
                }

            }

        }

        return $this->get_words_count($found_words, false, true);

    }

    public function get_trends($posts_list = []){

        $int_brands = $this->read_international_brands();
        $trends = [];

        foreach($int_brands as $brand){
            foreach($posts_list as $post){
                $brand_trim = trim($brand);
                //echo "Checking string ( $post ) for brand, '$brand'<br>";
                //if(stristr($post,$brand_trim)){
                if(preg_match("/\b$brand_trim\b/i", $post)){
                    $trends[] = $brand_trim;
                    //echo "<p>FOUND MATCH for '$brand_trim'!</p>";
                }
//                else{
//                    //echo "<p>NO MATCH FOUND for '$brand_trim'!</p>";
//                }
            }

        }

        return $this->get_words_count($trends, false, true);

        //return $fb_trends;

        //echo "<p>'FB Posts list':</p><pre>".print_r($posts_list, true)."</pre>";
        //echo "<p>'FB Trends':</p><pre>".print_r($fb_trends, true)."</pre>";exit;
    }

    public function get_words_count($messages, $exclude_words = false, $get_phrases = false){
        //$words = array("party", "#news", "#party", "fiddlesticks", "#news");
        if(!$get_phrases){
            $words = $this->get_words_individual($messages);
        }
        else{
            $words = $messages;
        }


        $word_list = [];

        if(isset($words)) {
            if (is_array($words)) {
                foreach ($words as $word) {

                    $this->check_word_count($word_list, $word, $exclude_words);

                }
            } else {
                $this->check_word_count($word_list, $words, $exclude_words);
            }

            if (!empty($word_list)) {
                return $this->get_words_sorted($word_list);
            }
        }

        return false;
    }

    // Based on fact that associative array should already be sorted.
    public function get_words_count_specific($words, $search_for){
        $found_words = [];
        foreach($words as $word => $count){
            if(stristr($word, $search_for)){
                $found_words[$word] = $count;
            }
        }

        return $found_words;
    }

    // gets array of number of times a word appears.
    private function check_word_count(&$word_list, $word_found, $exclude_words = false){

        if($exclude_words && $this->exclude_word($word_found)){
            return false;
        }

        if(!$this->check_word_isset($word_list, $word_found)){
            $word_list[strtolower($word_found)] = 0;
        }

        $word_count = $word_list[strtolower($word_found)] ;
        //\Log::Debug("Word found: '$word_found':".$word_list[strtolower($word_found)]);
        $increment = $word_count + 1;
        $word_list[strtolower($word_found)] = $increment;
        //\Log::Debug("Word found increment: '$word_found':".$word_list[strtolower($word_found)] ." - increment: $increment");


    }

    private function check_word_isset(&$word_list, $word_found){
        if(isset($word_list[strtolower($word_found)])){
            return true;
        }
        return false;
    }

    // to be overwritten/tailored in child classes if necessary... so.. protected, not private...
    protected function exclude_word($word_found){
        $exclude_array = ["a","this","and", "to", "you", "your", "on", "in", "the", "for","don't",
            "dont","is","miss","us","our","can","when","with","up",""];

        if(in_array($word_found, $exclude_array)){
            return true;
        }

        return false;
    }

    // sort array, maintaining index association
    private function get_words_sorted($list, $descending = true){

        if($descending){
            arsort($list);
            return $list;
        }

        asort($list);
        return $list;

    }

    private function get_words_individual($message)
    {
        if(!is_array($message) || empty($message)){
            return false;
        }

        //echo "<p>".__FUNCTION__. " : message: <pre>".print_r($message, true)."</pre>"; exit;

        $words = [];

        foreach($message as $sentence){
            $breakdown = explode(" ", $sentence);

            if(!empty($breakdown)){
                foreach($breakdown as $word){
                    $words[] = trim($word);
                }
            }

        }

        //echo "<p>".__FUNCTION__. " : words: <pre>".print_r($words, true)."</pre>"; exit;

        return $words;

    }

    // ** SOCIAL SCORE CALCULATIONS | START ** //

    // Mainly for reference...should usually be overridden on child class..
    protected function social_score_calculate($calc = [
            "posts"=>0,
            "friends"=>0,
            "tweets"=>0,
            "followers"=>0,
    ])
    {
        //\Log::debug("CLASS:".__CLASS__. " | Function: '".__FUNCTION__."' | ** calc array:\n". print_r($calc, true));
        $score_max = $this->social_score_max();
        $score = [];

        foreach($calc as $field => $data){
            if($data < $score_max[$field]){
                $score[$field] = ($data / $score_max[$field] * 100);
            }
            else{
                $score[$field] = 100;
            }
        }



        //\Log::debug("CLASS:".__CLASS__. " | Function: '".__FUNCTION__."' | ** score array:\n". print_r($score, true));

        $total_average_score = $this->social_score_total($score);
        if(!$total_average_score){
            return 0; // No score could be calculated
        }

        //\Log::debug("CLASS:".__CLASS__. " | Function: '".__FUNCTION__."'' | ** Average Score:\n". round($total_average_score));

        return round($total_average_score);

    }

    /* maximum scores in each category from which score out of 100 is calculated.
    Evaluated section scores 100% if it adds up to at least the baseline in this method */
    protected function social_score_max()
    {
        return [
            "posts" => "200",
            "friends" => "500",
            "tweets" => "200",
            "following" => "500",
            "followers" => "1000",
            "favourites" => "150",
        ];
    }

    protected function social_score_total($score_array =[])
    {
        if(empty($score_array) || !is_array($score_array)){
            \Log::debug("Function: '".__FUNCTION__."'' | ** Score Array empty or not array for user id ". Auth::user()->id ." :\n". print_r($score_array, true));
            return false;
        }

        foreach($score_array as $value){
            if(!is_numeric($value)){
                \Log::debug("Function: '".__FUNCTION__."'' | ** Score Array value not a number for user id ". Auth::user()->id ." \n". print_r($value, true));
                return false;
            }
        }

        $count = count($score_array); // get the number of items in the array for average calculations.
        $array_total = 0;
        foreach($score_array as $total){
            $array_total += $total;
        }
        return $array_total / $count;

    }

    // ** SOCIAL SCORE CALCULATIONS | END ** //

}