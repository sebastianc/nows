<?php

namespace App\Http\Controllers\Pages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;

class User_analysis extends Controller
{
    private $analysis_control;
    private $min_selections = "15"; // The minimum number of selections.

    // score sections, based on possibly scientific 'Four Temperaments'
    private $choleric = 0;
    private $sanguine = 0;
    private $melancholic = 0;
    private $phlegmatic = 0;

    public function index($bypass = ""){

        $this->build_form();

        if(strtolower($bypass) !== "bypass" && $this->check_for_previous_results()){
            return view("pages/user_analysis_view", ['prev_results'=> true]) ;
        }

        //echo "<p>Build form results:<pre>".print_r($this->analysis_control, true)."</pre>";

        return view("pages/user_analysis_view", ['analysis'=> $this->analysis_control, 'bypass' => $bypass]) ;

    }

    public function view_results(){

        $get_result = $this->check_for_previous_results(true); // get previous results (using 'true' parameter) if they exist

        //echo "<p>View_results function | get_result :<pre>".print_r($get_result, true)."</pre>"; exit;
        if($get_result !== false && isset($get_result->personality_analysis_data_1)){
            $personality_analysis_data = explode(",", $get_result->personality_analysis_data_1);

            //echo "<p>View_results function :<pre>".print_r($personality_analysis_data, true)."</pre>"; exit;

            if(!empty($personality_analysis_data)){
                return view("pages/user_analysis_view", ['show_results'=> true, 'personality_analysis_data' => $personality_analysis_data]) ;
            }

        }

        //return Redirect('user_analysis'); // If we get to here, simply redirect to User analysis link.
    }

    public function viewResultsRaw(){

        $get_result = $this->check_for_previous_results(true);

        if($get_result !== false && isset($get_result->personality_analysis_data_1)){
            $personality_analysis_data = explode(",", $get_result->personality_analysis_data_1);

            if(!empty($personality_analysis_data)){
                return view("pages/user_analysis_view_raw", ['show_results'=> true, 'personality_analysis_data' => $personality_analysis_data]) ;
            }

        }
    }

    private function build_form():void{
        $this->analysis_control = new \stdClass();

        $this->analysis_control->section_1 = new \stdClass();
        $section_1 = [
            'policy_driven',
            'fearful_of_conflict',
            'too_direct_verbally',
            'charismatic',
            'analysis_driven',
            'detail_oriented',
            'will_not_give_up',
            'direct_with_words',
            'sloppy_housekeeper',
            'bold',
            'visionary_leader',
            'wants_to_please_people',
            'low_self_esteem',
            'wants_to_be_right',
        ];

        foreach($section_1 as $word){
            $this->analysis_control->section_1->$word = str_replace("_", " ", ucwords($word));
        }
//        $this->analysis_control->section_1->open = "Open";
//        $this->analysis_control->section_1->understanding = "Understanding";
//        $this->analysis_control->section_1->new_control = "New Control";

        $this->analysis_control->section_2 = new \stdClass();

        $section_2 = [
            'passionate',
            'calm_and_relaxed',
            'neat_and_tidy',
            'emotionally_stable',
            'centre_of_attention',
            'likes_group_work',
            'talk_too_much',
            'perfectionist',
            'loves_to_perform',
            'like_nurturing_others',
            'quiet_watcher',
            'low_key',
            'a_thinker',
            'persuasive',
        ];

        foreach($section_2 as $word){
            $this->analysis_control->section_2->$word = str_replace("_", " ", ucwords($word));
        }



//        $this->analysis_control->section_2->quiet = "Quiet";
//        $this->analysis_control->section_2->gregarious = "Gregarious";

        $this->analysis_control->section_3 = new \stdClass();

        $section_3 = [
            'procrastinates_too_often',
            'usually_right',
            'self_critical',
            'hates_to_be_alone',
            'must_finish_project',
            'artistic',
            'likes_to_inspire',
            'enjoys_volunteering',
            'competitive_nature',
            'speak_your_mind',
            'must_be_with_people',
            'strong_willed',
            'doesnt_worry',
            'life_of_party',
        ];

        foreach($section_3 as $word){
            $this->analysis_control->section_3->$word = str_replace("_", " ", ucwords($word));
        }
//        $this->analysis_control->section_3->bold = "Bold";
//        $this->analysis_control->section_3->calming_influence = "Calming Influence";

        $this->analysis_control->section_4 = new \stdClass();

        $section_4 = [
            'do_all_to_satisfy',
            'hard_to_relax',
            'avoids_conflicts',
            'slow_decision_maker',
            'moody',
            'indecisive',
            'quiet',
            'likes_discussion',
            'high_self_esteem',
            'facts_driven',
            'domineering',
            'optimistic',
            'must_please_others',
            'happy_to_follow_lead',
        ];

        foreach($section_4 as $word){
            $this->analysis_control->section_4->$word = str_replace("_", " ", ucwords($word));
        }
//        $this->analysis_control->section_4->take_charge = "Take Charge";
//        $this->analysis_control->section_4->very_intelligent = "Very Intelligent";

        $this->analysis_control->section_5 = new \stdClass();

        $section_5 = [
            'must_win',
            'good_listener',
            'introspective_thinker',
            'likes_working_alone',
            'wants_to_be_popular',
            'motivated_by_praise',
            'sceptical_and_pessimistic', // skeptical
            'talkative',
            'argumentative',
            'inventive',
            'likes_privacy',
            'care_free',
            'not_organised', // not_organized
            'decisive',
        ];

        foreach($section_5 as $word){
            $this->analysis_control->section_5->$word = str_replace("_", " ", ucwords($word));
        }
//        $this->analysis_control->section_5->easy_going = "Easy Going";
//        $this->analysis_control->section_5->popular = "Popular";
        //echo "<p>Count of 'analysis_control' object:</p><pre>".count((array)$this->analysis_control)."</pre>";
        //echo "<p>Print of 'analysis_control' object:</p><pre>".print_r($this->analysis_control, true)."</pre>";

        return;
    }

    public function get_results(){

        $this->build_scores(); // create score for view on the Users personality / temperate score analysis page.

        $results = [

            'sanguine' => $this->sanguine,
            'melancholic' => $this->melancholic,
            'phlegmatic' => $this->phlegmatic,
            'choleric' => $this->choleric,
        ];
//        $results = [
//
//            'sanguine' => 10,
//            'melancholic' => 8,
//            'phlegmatic' => 4,
//            'choleric' => 6,
//        ];
        $total_scores = 0;
        foreach($results as $score){
           $total_scores = ($total_scores + $score);
        }
        if($total_scores > 0) {
            foreach ($results as $result_name => $score) {
                $score_percent = ($score * 100 / $total_scores); // convert score to percent
                $results[$result_name . "_score_info"] = $this->get_score_info($score_percent, $result_name);
            }

            //\Log::debug("Function: ".__FUNCTION__."\n\n".print_r($results, true));
            echo json_encode($results);
        } else {
            abort(404, 'No results yet');
        }
    }

    // currently handled by javascript on knowso.analysis.js ( should be handled here, really? )
    public function get_score_info($score, $section){

        //\Log::debug("Function: ".__FUNCTION__."| Analysing score ($score) for section, '$section'!'\n");

        // evaluate score per section in percentage terms.
        switch (true){
            case $score < 12.5:
                return"Your current scores indicate that you do not have traits that are particularly similar to the '". ucfirst($section)."' temperament";
            // break;
            case $score < 25:
                return"Your recorded scores would indicate that you have a few tendencies towards the '". ucfirst($section)."' temperament";
            case $score < 37.5:
                return"Your saved scores indicate that the '". ucfirst($section)."' temperament would apply to you in a significant range of areas";
            case $score < 50:
                return"The scores that you have submitted indicate that you have a strong and significant leaning to the '". ucfirst($section)."' temperament";
            default:
                // anything higher than 45 out of 100
                return"Your current scores indicate that your personality leans very strongly towards the '". ucfirst($section)."'temperament";
        }
    }

    // Build the score for view on the users temperament analysis page.
    private function build_scores(){
        $scores = $this->analysis_scores_array(); // get scores for each possible personality item.
        $get_results = $this->check_for_previous_results(true); // get raw previous results

        if(!isset($get_results->personality_analysis_data_1)){
            return false;
        }
        $results_array = explode(",",$get_results->personality_analysis_data_1); // put user results from DB into array for iteration and calculations.

        //\Log::debug("Get raw results:\n ". print_r($get_results->personality_analysis_data_1, true));
        //\Log::debug("Results array:\n ". print_r($results_array, true)); exit;

        foreach($results_array as $result){ // Iterate 'results_array' and add score
            if(!isset($scores[$result])){
                break;
            }
            foreach ($scores[$result] as $temperament => $score){
                $this->$temperament = ($this->$temperament + $score);
//                if($score > 0){
//                    \Log::debug("Adding score, $score from item, '$result' to temperament, '$temperament'");
//                }
            }
        }

        //\Log::debug("*** Final score for phlegmatic: ". $this->phlegmatic . " ***");
    }


    private function check_for_previous_results($return_result = false){
        $save_result = User::select('personality_analysis_data_1')->where('email',Auth::user()->email)->first();
        if(isset($save_result->personality_analysis_data_1) && !empty($save_result->personality_analysis_data_1)){
            if(!$return_result){
                return true;
            }

            return $save_result;

        }

        return false;
    }

    // process form 'submit' on user_analysis_view.blade.php
    public function save_data(Request $data):void{

        //echo "Returned by function, '". __FUNCTION__."'\n".print_r($data->toArray(), true);
        $form_data = $data->except("_token");
        $result = [];
        $cnt = count($form_data);
        //echo "count: $cnt";

        if($cnt === 0){
            $result['error'] = "No options were selected. Needs to be at least 5!";
            echo json_encode($result, true);
            return ;
        }

        if($cnt < $this->min_selections){
            $result['error'] = "Only $cnt options were selected. Needs to be at least {$this->min_selections}!";
            echo json_encode($result, true);
            return ;
        }

        $save_data = implode(",",array_keys($form_data));

        //echo json_encode(["email"=>Auth::user()->email]); exit;
        $save_attempt = User::where('email',Auth::user()->email)->first();
        $old_data = $save_attempt->personality_analysis_data_1;
        $save_attempt->personality_analysis_data_1 = $save_data;

        $save_result = false;
        if($save_attempt->isDirty()){
            \Log::debug("Old data, '$old_data': is different from save-data, '$save_data', so update would go ahead") ;
            //$save_result = $save_attempt->save();
            $save_result = $save_attempt->update(['personality_analysis_data_1'=>$save_attempt->personality_analysis_data_1]);
        }
        else{
            \Log::debug("Cannot detect change to analysis data") ;
            $result['no_change'] = "You appear to have selected the same previous answers. Try again if necessary";
        }
        //$save_result = User::where('email',Auth::user()->email)->update(['personality_analysis_data_1'=> $save_data]);
        //$users = DB::table('users')->select('name', 'email as user_email')->get();
        $result['save_result'] = $save_result;
        //$result['data'] = $save_data;
        echo json_encode($result, true);
    }


    public function save_result_test($test_string = ""){

        $save_result = User::select('personality_analysis_data_1')->where('email',"colin.morris@mediablanket.co.uk")->first();

        $old_data = $save_result->personality_analysis_data_1;
        $save_result->personality_analysis_data_1 = $test_string;

        if($save_result->isDirty()){
            echo "Old data: '$old_data'\nis different from test_string, '$test_string', \nso update would go ahead";
        }
        else{
            echo "Old data: '$old_data'\n";
            echo "New data: '$test_string'\n";
            echo "-- No apparent change --\n";
        }
        //echo "Function: ".__FUNCTION__ .":a\n". print_r($save_result, true);
        echo "Function: ".__FUNCTION__ .":\n". print_r($save_result->personality_analysis_data_1, true);
    }

    // Current scoring for the Personality / Temperament tests based on the 'Four Temperaments'
    // http://temperaments.fighunter.com, http://fourtemperaments.com/4-primary-temperaments/

    public function analysis_scores_array(){

        return [

            // Section 1 | Start

            'policy_driven'=>[

                'choleric' => 0,
                'sanguine' => 0,
                'melancholic' => 0,
                'phlegmatic' => 3, // [ph 1]
            ]
            ,
            'fearful_of_conflict'=>[

                'choleric' => 0,
                'sanguine' => 0,
                'melancholic' => 3, // [mel *]
                'phlegmatic' => 0,
            ],
            'too_direct_verbally'=>[

                'choleric' => 3, // [cho 1]
                'sanguine' => 0,
                'melancholic' => 0,
                'phlegmatic' => 0,
            ],
            'charismatic'=>[

                'choleric' => 0,
                'sanguine' => 3, // [san 1]
                'melancholic' => 0,
                'phlegmatic' => 1,
            ],
            'analysis_driven'=>[

                'choleric' => 1,
                'sanguine' => 0,
                'melancholic' => 3,  // [mel *]
                'phlegmatic' => 0,
            ],
            'detail_oriented'=>[

                'choleric' => 2,
                'sanguine' => 0,
                'melancholic' => 2,
                'phlegmatic' => 0,
            ],
            'will_not_give_up'=>[
                'choleric' => 2,
                'sanguine' => 2,
                'melancholic' => 0,
                'phlegmatic' => 0,
            ],
            'direct_with_words'=>[

                'choleric' => 3,  // [cho *]
                'sanguine' => 0,
                'melancholic' => 0,
                'phlegmatic' => 0,
            ],
            'sloppy_housekeeper'=>[
                'choleric' => 0,
                'sanguine' => 0,
                'melancholic' => 2,
                'phlegmatic' => 1,
            ],
            'bold'=>[

                'choleric' => 2,
                'sanguine' => 2,
                'melancholic' => 0,
                'phlegmatic' => 0,
            ],
            'visionary_leader'=>[
                'choleric' => 1,
                'sanguine' => 2,
                'melancholic' => 0,
                'phlegmatic' => 0,
            ],
            'wants_to_please_people'=>[

                'choleric' => 0,
                'sanguine' => 0,
                'melancholic' => 1,
                'phlegmatic' => 3,
            ],
            'low_self_esteem'=>[
                'choleric' => 1,
                'sanguine' => 0,
                'melancholic' => 3, // [mel 1]
                'phlegmatic' => 0,
            ],
            'wants_to_be_right'=>[

                'choleric' => 1,
                'sanguine' => 0,
                'melancholic' => 3,
                'phlegmatic' => 0,
            ]
            // Section 1 | End

            // Section 2 | Start

            ,
            'passionate'=>[

                'choleric' => 2,
                'sanguine' => 2,
                'melancholic' => 0,
                'phlegmatic' => 0,
            ],
            'calm_and_relaxed'=>[

                'choleric' => 0,
                'sanguine' => 1,
                'melancholic' => 0,
                'phlegmatic' => 3,// [ph 2]
            ],
            'neat_and_tidy'=>[

                'choleric' => 0,
                'sanguine' => 0,
                'melancholic' => 0,
                'phlegmatic' => 3,// [ph 3]
            ],
            'emotionally_stable'=>[

                'choleric' => 0,
                'sanguine' => 2,
                'melancholic' => 0,
                'phlegmatic' => 2, // [ph 4]
            ],
            'centre_of_attention'=>[

                'choleric' => 0,
                'sanguine' => 2,
                'melancholic' => 0,
                'phlegmatic' => 1,
            ],
            'likes_group_work'=>[

                'choleric' => 0,
                'sanguine' => 2,
                'melancholic' => 0,
                'phlegmatic' => 2,
            ],
            'talk_too_much'=>[

                'choleric' => 2,
                'sanguine' => 1,
                'melancholic' => 0,
                'phlegmatic' => 0,
            ],
            'perfectionistic'=>[

                'choleric' => 1,
                'sanguine' => 0,
                'melancholic' => 3, // [mel *]
                'phlegmatic' => 0,
            ],
            'loves_to_perform'=>[

                'choleric' => 1,
                'sanguine' => 2,
                'melancholic' => 0,
                'phlegmatic' => 0,
            ],
            'like_nurturing_others'=>[

                'choleric' => 0,
                'sanguine' => 3,
                'melancholic' => 0,
                'phlegmatic' => 0,
            ],
            'quiet_watcher'=>[

                'choleric' => 0,
                'sanguine' => 0,
                'melancholic' => 2,
                'phlegmatic' => 2,
            ],
            'low_key'=>[

                'choleric' => 0,
                'sanguine' => 0,
                'melancholic' => 1,
                'phlegmatic' => 3,
            ],
            'a_thinker'=>[

                'choleric' => 0,
                'sanguine' => 1,
                'melancholic' => 3,
                'phlegmatic' => 0,
            ],
            'persuasive'=>[

                'choleric' => 2,
                'sanguine' => 2,
                'melancholic' => 0,
                'phlegmatic' => 0,
            ]

            // Section 2 | End

            // Section 3 | Start
            ,
            'procrastinates_too_often'=>[
                'choleric' => 1,
                'sanguine' => 0,
                'melancholic' => 3, // [mel 2]
                'phlegmatic' => 0,
            ] ,
            'usually_right'=>[
                'choleric' => 2,
                'sanguine' => 0,
                'melancholic' => 2,
                'phlegmatic' => 0,
            ] ,
            'self_critical'=>[
                'choleric' => 1,
                'sanguine' => 0,
                'melancholic' => 3, // [mel 3]
                'phlegmatic' => 0,
            ] ,
            'hates_to_be_alone'=>[
                'choleric' => 0,
                'sanguine' => 3,
                'melancholic' => 0,
                'phlegmatic' => 1,
            ] ,
            'must_finish_project'=>[
                'choleric' => 3,  // [cho *]
                'sanguine' => 0,
                'melancholic' => 1,
                'phlegmatic' => 0,
            ] ,
            'artistic'=>[
                'choleric' => 0,
                'sanguine' => 0,
                'melancholic' => 3, // [mel *]
                'phlegmatic' => 0,
            ] ,
            'likes_to_inspire'=>[
                'choleric' => 0,
                'sanguine' => 3, // [san 2]
                'melancholic' => 0,
                'phlegmatic' => 2,
            ] ,
            'enjoys_volunteering'=>[
                'choleric' => 0,
                'sanguine' => 3,
                'melancholic' => 0,
                'phlegmatic' => 0,
            ] ,
            'competitive_nature'=>[
                'choleric' => 3,  // [cho *]
                'sanguine' => 0,
                'melancholic' => 1,
                'phlegmatic' => 0,
            ] ,
            'speak_your_mind'=>[
                'choleric' => 3,
                'sanguine' => 0,
                'melancholic' => 0,
                'phlegmatic' => 0,
            ] ,
            'must_be_with_people'=>[
                'choleric' => 2,
                'sanguine' => 2,
                'melancholic' => 0,
                'phlegmatic' => 0,
            ] ,
            'strong_willed'=>[
                'choleric' => 2,
                'sanguine' => 2,
                'melancholic' => 0,
                'phlegmatic' => 0,
            ] ,
            'doesnt_worry'=>[
                'choleric' => 0,
                'sanguine' => 2, // [san 3]
                'melancholic' => 0,
                'phlegmatic' => 2,
            ] ,
            'life_of_party'=>[
                'choleric' => 0,
                'sanguine' => 3, // [san 4]
                'melancholic' => 0,
                'phlegmatic' => 0,
            ],
            // Section 3 | End

            // Section 4 | Start

            'do_all_to_satisfy'=>[
                'choleric' => 0,
                'sanguine' => 2,
                'melancholic' => 1,
                'phlegmatic' => 1,
            ],
            'hard_to_relax'=>[
                'choleric' => 2, // [cho 2]
                'sanguine' => 0,
                'melancholic' => 2,
                'phlegmatic' => 0,
            ],
            'avoids_conflicts'=>[
                'choleric' => 0,
                'sanguine' => 1,
                'melancholic' => 1,
                'phlegmatic' => 3,// [ph 5]
            ],
            'slow_decision_maker'=>[
                'choleric' => 0,
                'sanguine' => 0,
                'melancholic' => 3,
                'phlegmatic' => 2,
            ],
            'moody'=>[
                'choleric' => 1,
                'sanguine' => 0,
                'melancholic' => 3,
                'phlegmatic' => 0,
            ],
            'indecisive'=>[
                'choleric' => 0,
                'sanguine' => 0,
                'melancholic' => 1, // [mel *]
                'phlegmatic' => 3,
            ],
            'quiet'=>[
                'choleric' => 0,
                'sanguine' => 0,
                'melancholic' => 2,
                'phlegmatic' => 2,
            ],
            'likes_discussion'=>[
                'choleric' => 1,
                'sanguine' => 3,  // [mel *]
                'melancholic' => 0,
                'phlegmatic' => 0, // [ph 6 x]
            ],
            'high_self_esteem'=>[
                'choleric' => 0,
                'sanguine' => 3, // [san 5]
                'melancholic' => 0,
                'phlegmatic' => 1,
            ],
            'facts_driven'=>[
                'choleric' => 0,
                'sanguine' => 0,
                'melancholic' => 3, // [mel *]
                'phlegmatic' => 1,
            ],
            'domineering'=>[
                'choleric' => 3, // [cho 3]
                'sanguine' => 1,
                'melancholic' => 0,
                'phlegmatic' => 0,
            ],
            'optimistic'=>[
                'choleric' => 0,
                'sanguine' => 3, // [san 6]
                'melancholic' => 0,
                'phlegmatic' => 1,
            ],
            'must_please_others'=>[
                'choleric' => 0,
                'sanguine' => 2,
                'melancholic' => 0,
                'phlegmatic' => 2,
            ],
            'happy_to_follow_lead'=>[
                'choleric' => 0,
                'sanguine' => 0,
                'melancholic' => 1,
                'phlegmatic' => 3,
            ],

            // Section 4 | End

            // Section 5 | Start
            'must_win'=>[
                'choleric' => 3, // [cho *]
                'sanguine' => 1,
                'melancholic' => 0,
                'phlegmatic' => 0,
            ] ,
            'good_listener'=>[
                'choleric' => 0,
                'sanguine' => 2,
                'melancholic' => 1,
                'phlegmatic' => 1,  // [ph 7]
            ] ,
            'introspective_thinker'=>[
                'choleric' => 0,
                'sanguine' => 0,
                'melancholic' => 3,
                'phlegmatic' => 1,
            ] ,
            'likes_working_alone'=>[
                'choleric' => 0,
                'sanguine' => 0,
                'melancholic' => 3, // [mel *]
                'phlegmatic' => 1,
            ] ,
            'wants_to_be_popular'=>[
                'choleric' => 0,
                'sanguine' => 3,  // [san *]
                'melancholic' => 0,
                'phlegmatic' => 0,
            ] ,
            'motivated_by_praise'=>[
                'choleric' => 2,
                'sanguine' => 0,
                'melancholic' => 2,
                'phlegmatic' => 0,
            ] ,
            'sceptical_and_pessimistic'=>[
                'choleric' => 1,
                'sanguine' => 0,
                'melancholic' => 3, // [mel 4]
                'phlegmatic' => 0,
            ] ,
            'talkative'=>[
                'choleric' => 1,
                'sanguine' => 3, // [san *]
                'melancholic' => 0,
                'phlegmatic' => 0,
            ] ,
            'argumentative'=>[
                'choleric' => 3, // [cho 4]
                'sanguine' => 0,
                'melancholic' => 0,
                'phlegmatic' => 0,
            ] ,
            'inventive'=>[
                'choleric' => 0,
                'sanguine' => 2,
                'melancholic' => 2,
                'phlegmatic' => 0,
            ] ,
            'likes_privacy'=>[
                'choleric' => 0,
                'sanguine' => 0,
                'melancholic' => 2,
                'phlegmatic' => 2,
            ] ,
            'care_free'=>[
                'choleric' => 0,
                'sanguine' => 2,
                'melancholic' => 0,
                'phlegmatic' => 3, // [ph 8]
            ] ,
            'not_organised'=>[
                'choleric' => 2,
                'sanguine' => 0,
                'melancholic' => 1,
                'phlegmatic' => 0,
            ] ,
            'decisive'=>[
                'choleric' => 3,
                'sanguine' => 0,
                'melancholic' => 1,
                'phlegmatic' => 0,
            ]
            // Section 5 | End
        ];

    }
}
