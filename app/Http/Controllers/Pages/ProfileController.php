<?php

namespace App\Http\Controllers\Pages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;

class ProfileController extends Controller
{

    /**
     * Require the user to be logged in for everything in this controller
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * View the User Profile
     */
    public function view()
    {
        $user = User::where('email',Auth::user()->email)->first();

        //$date_values = $this->fill_date_values();

        return view('pages.profile', [
            //'date_values' => $date_values,
            'user' => $user
        ]);
    }

    /**
     * Update the User Profile
     */
    public function update(Request $request)
    {
        $user = User::where('email',Auth::user()->email)->first();

        return redirect()->back()->with(['status' => 'Profile Updated']);
    }

    /**
     * View the User Profile
     */
    public function cancel(Request $request)
    {
        if($request->input('email') == Auth::user()->email){
            $user = User::where('email',Auth::user()->email)->first();
            $user->cancelled = 1;
            $user->cancelled_date = date('Y-m-d H:i:s', time());
            $user->save();
            if ($request->ajax()) {
                return \Response::json(['success' => true], 200);
            } else {
                return redirect()->route('login');
            }
        } else {
            if ($request->ajax()) {
                return \Response::json(['error' => true], 404);
            } else {
                return redirect()->route('login');
            }
        }
    }

    /**
     * Generate calendar dates
     */
    private function fill_date_values()
    {
        $date_values = [];
        $days = ""; $months = ""; $years = "";
        $months_text = ["", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        for($i = 1; $i < 32; $i++){
            $ipad = str_pad($i, 2, '0', STR_PAD_LEFT);
            $selected = (old('dob_day') == $ipad)?  " selected" : ((isset($day_test) && $day_test == $ipad && !old('dob_day'))? " selected": "");
            $days .= "<option value=\"$ipad\"{$selected}>$i</option>\n";
        }
        for($i = 1; $i < 13; $i++){
            $ipad = str_pad($i, 2, '0', STR_PAD_LEFT);
            $selected = (old('dob_month') == $ipad)?  " selected" : ((isset($month_test) && $month_test == $ipad && !old('dob_month'))? " selected": "");
            $months .= "<option value=\"$ipad\"{$selected}>{$months_text[$i]}</option>\n";
        }

        $upper = date("Y")-18;
        $lower = date("Y") - 90;
        for($i = $upper; $i > $lower; $i--){
            $selected = (old('dob_year') == $i)?  " selected" : ((isset($year_test) && $year_test == $i && !old('dob_year'))? " selected": "");
            $years .= "<option value=\"$i\"{$selected}>$i</option>\n";
        }

        $date_values['days'] = $days;
        $date_values['months'] = $months;
        $date_values['years'] = $years;

        return $date_values;
    }
}
