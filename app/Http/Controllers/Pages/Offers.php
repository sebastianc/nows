<?php

namespace App\Http\Controllers\Pages;

//use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\ProductOffers;

class Offers extends Controller
{
    public function index(){

        $all = ProductOffers::all_offers();
        $eating = ProductOffers::eating_offers();
        $shopping = ProductOffers::shopping_offers();
        $holiday = ProductOffers::holiday_offers();
        $health = ProductOffers::health_offers();

        $offer_url = "//www.supacompare.co.uk/offer?link=";
        //echo "<p>Offer List <pre>".print_r($list, true)."</pre>";

        return view("pages/product_offers", [
            'offers_all' => $all,
            'offers_eating' => $eating,
            'offers_shopping' => $shopping,
            'offers_holidays' => $holiday,
            'offers_health' => $health,
            'intro_text' => "",
            'offer_url' => $offer_url,
        ]);

//
//        echo "<p>NEW Offer List <pre>".print_r($newlist->index(), true)."</pre>";

    }
}
