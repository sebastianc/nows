<?php

namespace App\Http\Controllers;

use App\User;
use App\Verify_users;
use App\Mail\VerifyMail;
use App\Services\Customers\RegisterCustomer;
use App\Services\Payments\RegularPayment;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class PaymentController extends Controller
{

    public function register(Request $request)
    {
        /*$request->validate([
            'email' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
        ]);*/

        $validation = Validator::make($request->all(), [
            'mobile_number' => 'required|phone_number|unique:kn_users',
            'email' => 'required|unique:kn_users',
            'first_name' => 'required',
            'last_name' => 'required',
            'password' => 'required'
        ]);

        if ($validation->fails()) {
            $responseArray = array(
                'success' => false,
                'message' => 'Request has failed validation test ',
                'response_code' => 422,
                'data' => $validation->errors()
            );
            return response()->json($responseArray, $responseArray['response_code']);
        }

        $user = new User;
        $user->title = $request->input('title');
        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');
        $user->name = $request->input('first_name') . ' ' . $request->input('last_name');
        $user->mobile_number = $request->input('mobile_number');
        $user->email = $request->input('email');
        $user->password = $request->input('password');
        $user->date_of_birth = $request->input('dob_day') . '-' . $request->input('dob_month') . '-' . $request->input('dob_year');
        $user->post_code = $request->input('post_code');
        $user->house_number = $request->input('house_number');
        $user->house_name = $request->input('house_name');
        $user->flat_number = $request->input('flat_number');
        $user->street_name = $request->input('street_name');
        $user->city = $request->input('city');
        $user->county = $request->input('county');
        $user->residential_status = $request->input('residential_status');
        $user->education = $request->input('education');
        $user->employment_status = $request->input('employment_status');
        $user->marital_status = $request->input('marital_status');
        $user->social_media_check_reason = $request->input('social_media_check_reason');
        if($request->has('aid')) {
            $user->aff_id = $request->input('aid');
        }
        $user->save();

        $id = $user->id;

        $verifyUser = Verify_users::create([
            'user_id' => $id,
            'token' => str_random(40)
        ]);

        Mail::to($user->email)->send(new VerifyMail($user, $verifyUser));

        return response()->json(array('success' => true, 'last_insert_id' => $id), 200);
    }

    /** --------------------------------------|
     * Full Payment
     *
     * Process a full payment through stripe and on success instantly create membership
     *
     * @param Request $request
     * @param $existing
     * @return \Illuminate\Http\RedirectResponse
     * ---------------------------------------|
     */
    public function processPayment(Request $request)
    {
        //$amount = 1;

        $email = $request->input('stripeEmail');

        // Used to be a call to RegularPayment service but now only registers customers for a trial period
        $stripeData = (new RegisterCustomer())->post('', [
            'stripe_token' => $request->input('stripeToken'),
            //'amount' =>  $amount,
            'description' => 'Create new trial member',
            'member_email_address' => $email,
            'save_card' => 1
        ]);

        /**
         * Stripe Status OK is wrong call, ERROR is failure
         */
        if (isset($stripeData->status) || !isset($stripeData->response_payload)) {
            //dd($stripeData);
            if($stripeData->status == "ERROR"){
                if (isset($stripeData->data->message)) {
                    $error = $stripeData->data->message;
                }
                elseif (isset($stripeData->data)) {
                    $error = $stripeData->data;
                }
                else {
                    $error = "An unknown error has occurred";
                }
                if ($request->ajax()) {
                    return response()->json(['errors'=>['error'=>$error]], 422);
                } else {
                    return redirect()->back()->withErrors($error);
                }
            } elseif($stripeData->status == "OK"){
                $error = "A GET request was made instead of a POST because Stripe Service is called via https instead of http (or vice versa), or Url is wrong";
                return redirect()->back()->withErrors($error);
            }
        }

        /*  Possible debug values
            $stripeData->response_payload->data->miscellaneous->balance_transaction,
            $stripeData->response_payload->data->miscellaneous->id);
         */

        $user = User::where('email',$email)->first();
        $user->stripe_id = $stripeData->response_payload->data->resource->stripe_id;
        $user->save();

        if ($request->ajax()) {
            return \Response::json(['success' => 1], 200);
        } else {
            //return route('crm.edit', ['class' => 'lead', 'id' => $id])->with(['success' => 'Payment completed !']);
            return redirect()->route('login')->with(['status' => 'We sent you an activation code. Check your email and click on the link to verify.']);
        }

    }

}