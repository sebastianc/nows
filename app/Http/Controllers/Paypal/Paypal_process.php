<?php

namespace App\Http\Controllers\Paypal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User_register;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class Paypal_process extends Controller
{

    public function cancel($app=""){

        $delete_user = false;
        //echo "<p>Request: </p><pre>". print_r($app, true)."</pre>"; exit;
        if(!empty($app)){
            //$reg_find = new User_register();
            //$delete_user = $reg_find->find_temp_user($app);
            //$delete_user = $reg_find->copy_temp_user($app);
            $delete_user = (new User_register())->delete_temp_user($app); // delete pre-registered user if user ultimately cancels the paypal payment / registration.
        }

        if(!$delete_user || $delete_user === 0){
            return Redirect::to("/");
        }

        return view('pages/paypal_cancel', ['app'=> $app, 'delete_result' => $delete_user,'result'=> "New user deletion attempt: $delete_user"]);
    }


    public function complete_registration($app_id = "", Request $request){

        $register_user = false;
        $auth_result = false;

        if(!empty($request->input('auth'))){
            $auth_result = $request->input('auth');
        }

        $request->session()->flash('warning', 'An email has been sent to the inbox that you registered for our service with. Please verify your email by clicking
         on the enclosed link to log in!');
        return redirect('login');

//        if(!empty($app_id)){
//
//            $register_user = (new User_register())->copy_temp_user($app_id); // copy pre-registered user to real users table if user completed paypal payment.
//        }

        $register_user = "Test User";

//        if(!$register_user || $register_user === 0){
//            return Redirect::to("/");
//        }
//
//        return view('pages/paypal_confirmed_registration', [
//            'app'=> $app_id,
//            'result'=> "User Registration attempt: $register_user",
//            'auth_result' => $auth_result,
//            'request' => $request->all(),
//        ]);
    }
}
