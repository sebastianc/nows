<?php

namespace App\Http\Controllers\Paypal;
//require('PaypalIPN.php');
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mail;
use App\User_register;
use App\User;

class Paypal_listener extends Controller
{

    private $today_date; // ++ debugging subs dates..

    public function index(Request $request){

        //echo "<p>Post returned: <pre>".print_r($request->all(), true)."</pre>";

       try{
           $ipn = new PaypalIPN();

           //echo "<p>ipn: <pre>".print_r($ipn, true)."</pre>";
           \Log::debug("ipn:\n".print_r($ipn, true));
       }
       catch(\Exception $e){
           \Log::debug("Could not initialise PaypalIPN() class:\n".print_r($e->getMessage(), true));
       }


// Use the sandbox endpoint during testing.
        $ipn->useSandbox();
        $verified = $ipn->verifyIPN();

        if($request->input('txn_type') ==  "subscr_cancel"){
            $this->process_cancel($request->all());
            header("HTTP/1.1 200 OK");
            exit;
        }

        if($request->input('txn_type') == "recurring_payment_suspended"){
            $suspended = $this->process_suspended($request->all());
            if($suspended){

                $email = "<p>User suspended via Paypal</p>";
                $email .= "<p>subscriber id: ".$request->input('subscr_id')."</p>";
                $email .= "<p>First name: ".$request->input('first_name')."</p>";
                $email .= "<p>Last name: ".$request->input('last_name')."</p>";
                $email .= "<p>user suspended</p>";
                Mail::send('emails.paypal_email', array('email' => $email), function($message){
                    $message->to(['colcmorris@gmail.com', 'admin@knowso.co.uk'])->subject('Knowso - Account suspended!');
                });
            }

            header("HTTP/1.1 200 OK");
            exit;
        }


        if ($verified) {

            if(!empty($request->input('custom'))){

                //$pdt = "09:13:50 Apr 13, 2018 PDT";
                $update_user = [];
                //$subscr_date = strtotime($request->input('subscr_date'));
                $update_user['sub_date'] = date("Y-m-d H:i:s", strtotime($request->input('subscr_date'))); // convert to 'Europe/London' from PDT timezone

                // calc date between registration date and end of trial period for 'payment_date'
                $period1 = $request->input('period1');
                $period1 = str_ireplace("d", "days", $period1);

                $subscr_date = $update_user['sub_date'];

                $subscr_date_15 = strtotime("$subscr_date + $period1");

                $update_user['payment_date'] = date("Y-m-d H:i:s", $subscr_date_15);
                // Removed when switched to Stripe
                //$update_user['paypal_payer_email'] = $request->input('payer_email');
                // Removed when switched to Stripe
                //$update_user['paypal_payer_id'] = $request->input('payer_id');
                $update_user['stripe_paid'] = "1";
                // Removed when switched to Stripe
                //$update_user['paypal_subscr_id'] = $request->input('subscr_id');


                //$update_res = $this->update_user_register($update_user, $request->input('custom'));
                $update_res = ($user_reg = new User_register())->update_temp_user($update_user, $request->input('custom'));
                if($update_res){
                    $register_user = $user_reg->copy_temp_user($request->input('custom')); // copy pre-registered user to real users table if user completed paypal payment.

                }
                else{

                    \Log::debug("** Knowso Error | registering Paypal user:\n".$request->input('custom')." **");
                    // ** process error registering
                }


            }

            \Log::debug("** verified ipn:\n".print_r($verified, true));
            $email = "** Verified success **:\n<pre>".print_r($verified, true)."</pre>";
            $email .= "<p>Post Data? </p><pre>".print_r($request->all(), true)."</pre>";
            $email .= "<h4>++ Attempt to copy the user with email address ".$request->input('payer_email')." returned: </h4><pre>".print_r($register_user, true)."</pre>";
            //Mail::to("colcmorris@gmail.com")->send($email);
            Mail::send('emails.paypal_email', array('email' => $email), function($message){
                $message->to('colcmorris@gmail.com', 'Colin')->subject('Knowso Signup - Success!');
            });


            /*
             * Process IPN
             * A list of variables is available here:
             * https://developer.paypal.com/webapps/developer/docs/classic/ipn/integration-guide/IPNandPDTVariables/
             */
        }
        else{
            \Log::debug("** NOT verified ipn:\n".print_r($verified, true));
            $email = "Not verified: result: \n".print_r($verified, true);
            $email .= "<p>Post Data? </p><pre>".print_r($request->all(), true)."</pre>";
            Mail::send('emails.paypal_email', array('email' => $email), function($message){
                $message->to('colcmorris@gmail.com', 'Colin')->subject('Paypal test - Fail!');
            });

        }

// Reply with an empty 200 response to indicate to paypal the IPN was received correctly.
        header("HTTP/1.1 200 OK");

    }

    private function process_cancel($request_data){
        $update = []; // cancelled	cancelled_date	expiry_date
        $update['cancelled'] = 1;
        $update['cancelled_date'] = date("Y-m-d H:i:s", strtotime($request_data['subscr_date']));
        /**
         * Below should be different/ not same day if NOT cancelled before trial period end! ***
         */
        $update['expiry_date'] = date("Y-m-d H:i:s", strtotime($request_data['subscr_date']));
        return (new User())->paypal_ipn_subs_update($request_data['subscr_id'],$update);
    }

    private function process_suspended($request_data){
        $update = []; // cancelled	cancelled_date	expiry_date

        // Removed when switched to Stripe
        //$update['paypal_subscr_suspended'] = 1;

        //$update['cancelled_date'] = date("Y-m-d H:i:s", strtotime($request_data['subscr_date']));
        /**
         * Below should be different/ not same day if NOT cancelled before trial period end! ***
         */
        //$update['expiry_date'] = date("Y-m-d H:i:s", strtotime($request_data['subscr_date']));
        return (new User())->paypal_ipn_subs_update($request_data['recurring_payment_id'],$update);
    }

    public function calc_dates($date_time){

        //$subscr_date = date("Y-m-d H:i:s", strtotime("07:01:17 Apr 18, 2018 PDT"));
        $subscr_date = date("Y-m-d H:i:s", strtotime("07:01:17 Apr 03, 2018 PDT"));
        $period1 = "15 D";
        $period1 = str_ireplace("d", "days", $period1);

        //$this->today_date = date("2018-05-17 12:01:14");
        $this->today_date = date("2018-07-17 15:01:29");

        echo "\nOriginal subs date: $subscr_date \n\n";
        echo "\nTodays date: $this->today_date \n";

        $subscr_date_15 = strtotime("$subscr_date + $period1");

//echo "<p>Original date + $period1: " . date("H:i:s M d, Y", $subscr_date_15)." </p>";
        echo "Original start subs date + $period1: " . date("Y-m-d H:i:s", $subscr_date_15)."\n";

        //echo "PHP Date debug : ".date("d",strtotime($subscr_date))."\n";
        //echo "Date day: ". (date("d"))."\n";
        $start_subs_day = date("d", strtotime(date("Y-m-d H:i:s", $subscr_date_15)));
        $start_subs_time = date("H:i:s", strtotime(date("Y-m-d H:i:s", $subscr_date_15)));

        echo "Original start subs day : $start_subs_day\n";
        echo "Original start subs time : $start_subs_time\n\n";
        //$today_date = date("2018-05-03 15:01:18");
        //$today_date = date("2018-05-04 14:20.02");
        //$today_date = date(now());

        //echo "Today Date: $today_date\n";
        $before_trial_end = $this->subs_cancel_before_subs_start($this->today_date, date("Y-m-d H:i:s", $subscr_date_15));
        if(!$before_trial_end){
            $this->active_subs_cancel_exp_date($start_subs_day, $start_subs_time);
        }

    }

    private function subs_cancel_before_subs_start($today_date, $subscr_start_date)
    {
        if(strtotime($today_date) < strtotime($subscr_start_date)){
            echo "todays date: '$today_date' is smaller than subscr_start_date, '$subscr_start_date'. Cancel subs immediately!\n";
            return true;
        }

        echo "todays date: '$today_date' is NOT smaller than subscr_start_date, '$subscr_start_date'. Continue evaluation\n\n";
        return false;
        //$subsr_day = date("d", strtotime($subscr_date));
        //$trial_end_day = date("d", strtotime($subscr_date_trial_end));
        //if(date("d",strtotime($subscr_date));}
    }

    private function active_subs_cancel_exp_date($day, $time)
    {
        //$today_date = date("2018-05-03 15:01:18"); // ++ DEBUGGING THIS CODE | COMMENT OUT..
        //$today_date = date("2018-05-02 12:01:14"); // ++ DEBUGGING THIS CODE | COMMENT OUT..
        $calc_year_month = date("Y-m", strtotime($this->today_date)); // ++ DEBUGGING THIS CODE | COMMENT OUT..

        $subs_end_date = date("$calc_year_month-$day $time") ;
        echo "++ Subs start date: $subs_end_date\n ++";
        $date_time_now = date($this->today_date);
        if(strtotime($date_time_now) < strtotime($subs_end_date)){

            //echo "Current date / time, '$date_time_now' is smaller than calculated subs end date, '$subs_end_date'\n";
            echo "Current date / time, '$date_time_now' is smaller than calculated subs end date, '$subs_end_date'. return '$subs_end_date' to database\n";

            return $subs_end_date;
            //return true;
        }

        $subs_end_date_new = date("Y-m-d H:i:s", strtotime("$subs_end_date + 1 month" ));

        echo "Current date / time, '$date_time_now' is NOT smaller than calculated subs end date, '$subs_end_date'\n";
        echo "++ REAL Subs end date should be $subs_end_date_new ++\n";
        return $subs_end_date_new ;
        //$subsr_day = date("d", strtotime($subscr_date));
        //$trial_end_day = date("d", strtotime($subscr_date_trial_end));
        //if(date("d",strtotime($subscr_date));}
    }


    private function update_user_register($update_user = [], $custom_id){
        if(empty($update_user) || empty($custom_id)){
            return false;
        }
    }


}
/*
 * <?php namespace Listener;

require('PaypalIPN.php');

use PaypalIPN;

$ipn = new PaypalIPN();

// Use the sandbox endpoint during testing.
$ipn->useSandbox();
$verified = $ipn->verifyIPN();
if ($verified) {
    /*
     * Process IPN
     * A list of variables is available here:
     * https://developer.paypal.com/webapps/developer/docs/classic/ipn/integration-guide/IPNandPDTVariables/
     */
/*

 Reply with an empty 200 response to indicate to paypal the IPN was received correctly.
header("HTTP/1.1 200 OK");
 */