<?php

namespace App;

//use App\Verify_users;
use App\Mail\VerifyMail;
use Illuminate\Support\Facades\Mail;

//use User;

use Illuminate\Database\Eloquent\Model;
use Symfony\Component\Debug\Exception\FatalThrowableError;

class User_register extends Model
{
    protected $table = "kn_user_register";

    //protected $guarded = ['password_confirmation'];
//    protected $fillable = ['title', 'first_name', 'last_name', 'first_name', 'mobile_number', 'email', 'date_of_birth', 'house_number'
//        , 'house_name', 'flat_number', 'street_name', 'city', 'county', 'post_code', 'residential_status', 'education', 'employment_status'
//        , 'marital_status', 'social_media_check_reason', 'password', 'stripe_id'];


    public function copy_temp_user($paypal_unique_id)
    {

        if(empty($paypal_unique_id)){
            return false;
        }

        try {

            $copy_user = $this->where('stripe_id', $paypal_unique_id)->first();
        }
        catch(FatalThrowableError $e){
            \Log::debug("Error copy existing pre-user:\n". print_r($e->getMessage(), true));
        }
        if(!$copy_user){
            \Log::debug("Could not copy user:$copy_user");
            return false;
        }
        //\Log::debug("New user COPIED result:\n". print_r($copy_user->toArray(), true)); exit;
        //$user_model = new User();

        unset($copy_user['id']);

        //$copy_user = (object)$copy_user;
        //echo "<p>User create: </p><pre>". print_r((array)$copy_user, true)."</pre>"; exit;

        try{
            \Log::debug("SAVING COPIED user :\n". print_r($copy_user, true));
            $new_user = User::create($copy_user->toArray());
            \Log::debug("++ SAVING COPIED user result:\n". print_r($new_user , true));
        }catch (\Exception $e){
            return $e->getMessage();
        }


        if($new_user){
            $this->send_verification_to_user($new_user);
            return $this->delete_temp_user($paypal_unique_id);
        }
        //\Log::debug("New user creation result:\n". print_r($new_user, true));
        return $new_user;

    }

    public function delete_temp_user($paypal_unique_id){

        if(empty($paypal_unique_id)){
            return false;
        }

        try{
            $delete = $this->where('stripe_id', $paypal_unique_id)->delete();
        }catch (\Exception $e){
            return $e->getMessage();
        }

        //\Log::debug("Delete user with paypal_redirect_id '$paypal_unique_id' result:\n". print_r($delete, true));
        return $delete;
    }

    public function find_temp_user($paypal_unique_id){

        if(empty($paypal_unique_id)){
            return false;
        }

        $find = $this->where('stripe_id', $paypal_unique_id)->first();
        //\Log::debug("Find user with paypal_redirect_id '$paypal_unique_id' result:\n". print_r($find, true));
        return $find;
    }

    private function send_verification_to_user($user){
        if(empty($user)){
            \Log::debug("Function: '".__FUNCTION__."' | Send Verification to user process has empty parameter");
            return false;
        }

        try{
            $verifyUser = Verify_users::create([
                'user_id' => $user->id,
                'token' => str_random(40)
            ]);

            return Mail::to($user->email)->send(new VerifyMail($user, $verifyUser));

        }
        catch(\Exception $e){
            \Log::debug("Send Verification to user id, '$user->id' has failed!");
        }

        return false;


    }

    public function update_temp_user($update_user = [],$paypal_unique_id){

        if(empty($update_user) || empty($paypal_unique_id)){
            return false;
        }

        $find = $this->where('stripe_id', $paypal_unique_id)->update($update_user);
        //\Log::debug("Find user with paypal_redirect_id '$paypal_unique_id' result:\n". print_r($find, true));
        return $find;
    }



}
