<?php
/**
 * Created by PhpStorm.
 * User: colino
 * Date: 06/04/18
 * Time: 12:49
 */
?>
@foreach($product_array as $product)

    @if($row_count % 3 === 0)

        <div class="row prod-list-row" >

            @endif
            <div class="col-md-4">
                <a href="{{ $offer_url }}{{ $product['id'] }}" target="_blank" style="" >
                    <div class="col-md-12 prod-link-box" style="">
                        @if(!empty($product['company_name']))
                            <div class="">
                                <p><strong>{!! $product['company_name']  !!}</strong></p>

                            </div>
                        @endif

                        <div class="prod-img-div" style=""><img src="//products.supercompare.co.uk{{  $product['image_url'] }}" alt="{{$product['company_name']}}" style="" /></div>
                        <div class="col-md-6 prod-apply-div">
                            {{-- <a href="{{ $product['promo_link'] }}" target="_blank" style="" >
                             <div class="btn btn-success">Apply</div>
                             </a> --}}
                        </div>

                        @if(!empty($product['offer_name']))
                            <div class="col-md-12 product-offer-name">
                                <p>{!! $product['offer_name'] !!}</p>
                            </div>
                        @endif
                        @if(!empty($product['info_text']))
                            <div class="col-md-12 product-offer-info">
                                <p>{!! $product['info_text'] !!}</p>
                            </div>
                        @endif
                    </div>
                </a>
            </div>

            @if($row_count % 3 === 2)

        </div > <!-- row prod-list-row - END -->


    @endif
    @php $row_count ++; @endphp

@endforeach
@if($row_count % 3 > 0)
    {!! $end_div !!}

@endif
@php
    $row_count = 0;
@endphp
