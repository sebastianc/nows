@extends("templates.layout")

@section ('site_content')

<style type="text/css">
    .alert {
        position: relative !important;
    }
</style>

<section class="social-page">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card login-box">
                {{--<div class="card-header">Login</div>--}}
                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        @if (session('warning'))
                            <div class="alert alert-warning">
                                {!! session('warning') !!}
                            </div>
                        @endif
                        <div class="form-group row">
                            <div class="col-12">
                                <h4>Log in to Knowso</h4>
                            </div>

                            <div class="col-12">
                                <label for="email" class="col-form-label">Email Address</label>
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">


                            <div class="col-12">

                                <label for="password" class="col-form-label" style="text-align:left">Password</label>
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif

                            </div>
                        </div>

                        {{--<div class="form-group row">--}}
                            {{--<div class="col-md-6 offset-md-4">--}}
                                {{--<div class="checkbox">--}}
                                    {{--<label>--}}
                                        {{--<input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me--}}
                                    {{--</label>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        <div class="form-group row mb-0">
                            <div class="col-12">
                                <button type="submit" class="btn btn-primary">
                                    Log in
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>

                                 {{--<div>
                                     <a href="{{url('/redirect_to_fb')}}" class="btn btn-facebook" style="background-color:#4267b2"><i class="fa fa-facebook-square"></i> Login with Facebook</a>
                                 </div>--}}

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
@endsection
