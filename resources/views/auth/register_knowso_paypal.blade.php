@php
$test_paypal = true;
@endphp

@extends("templates.layout")

@section ('site_content')

<script>

window.onload = function(){

    var clear_errors = [];
    $(function () {

        var frm_btn = $('#pay_submit');
        frm_btn.click(function (ev) {
            ev.preventDefault();

            //$('input').prop('required',true);
            var serial_data = $('#submit_paypal').serialize();

            $.ajax({
                type: 'post',
                url: "/register_send_paypal",
                // url: frm.attr('action'),
                data: serial_data,
                success: function (data) {
                    //alert('ok:\n' + data);
                    $('#submit_paypal').submit();

                },
                error: function (data,status, response) {

                    clear_error_fields(clear_errors);
//                    var err = "";
//                    for(var d in data){
//                        err += d + " - " + data[d] + "\n";
//                    }
                    var error = jQuery.parseJSON(data.responseText);
                    $('.alert-danger').css('display', 'block').html(error.message);

                    for(var e in error.errors){
                        $('.err_' + e).css('display', 'block').children().html(error.errors[e]) ;
                        clear_errors.push (e);
                    }


                }
            });

        });

        function clear_error_fields(clear_errors){

            if(clear_errors.length == 0){
                return;
            }

            for(var c in clear_errors){

                $('.err_' + clear_errors[c]).css('display', 'none').children().html('') ;
            }


        }
    });


    $('#next-1').click(function(){

        $("#register-1").hide();

        var form = $("#submit_paypal");
        var url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function(data)
            {
                //alert(data);
                $("#register-2").show();
                $('#next-1').hide();
                $('.already-member').hide();
                $('#submit_paypal').hide();
            },
            error: function (data) {
                //console.log('An error occurred.');
                //console.log(data.responseJSON.data);
                var resp = data.responseJSON.data;

                switch(true) {
                    case ("mobile_number" in resp) :
                        $("#reg-errors").text('Phone needs to be a valid phone number.');
                        break;
                    case ("email" in resp) :
                        $("#reg-errors").text('The email has already been taken.');
                        break;
                    case ("first_name" in resp) :
                        $("#reg-errors").text('Your first name is missing.');
                        break;
                    case ("last_name" in resp) :
                        $("#reg-errors").text('Your last name is missing.');
                        break;
                    case ("password" in resp) :
                        $("#reg-errors").text('Password is not set.');
                        break;
                    default:
                        $("#reg-errors").text('Registration details error.');
                }
                $("#register-1").show();
                $('#next-1').show();

            },
        });

    });

//    if($('#register-2').is(":visible") ){
//        $('#next-1').hide();
//    }

    $('#next-2').click(function(){

        //$("#register-2").hide();
        $("#register-3").show();
    });


    $('#register-btn').click(function(){

        $("#register-1").show();
        $("#next-1").hide();
        //$("#register-2").show();
    });

        /**
         * Address Lookup
         */

        if ($('#post_code').val().length != 0){
            $("#address-section").show();
            //$("#search_address").attr("disabled", 'disabled');
        }

        $("#postcode_search").click(function() {
            $("#search_address").removeAttr('disabled');
        });

        $("#search_address").click(function() {

            /**
             * remove the nopc modal if available
             */
            if ($("#nopc_modal").is(":visible"))
            {

                $("#nopc_modal").css("display","none");

            }

            if($("#nopc_modal").css("display","block")){
                $("#nopc_modal").css("display","none");
            }

            $("#pc_modal").css("display","none");

            /**
             * set the button to a loader
             */
            $(this).addClass("is-loading");

            /**
             * setup the variables
             */
            var post_code = $("#postcode_search").val();
            var url = "/address_lookup/" + post_code;

            /**
             * check field is not empty
             */
            if (post_code == "")
            {

                $(this).removeClass("is-loading");
                $("#nopc_modal").css("display","block");
                $("#choose-address").css("display","none");

                location.href = '#nopc_modal';

                return false;

            }

            $.ajax({

                type: "GET",
                dataType: 'json',
                url: url,
                success: function(response) {

                    $("#address_search_populate").html(response.delivery_point_select);
                    $("#address_search_populate_container").slideDown();

                    $("#search_address").removeClass("is-loading");

                    /**
                     * show next fields
                     */
                    $(".box-step2").removeClass("box-step2");

                    $("#search_address").attr("disabled", 'disabled');

                },

                error: function(response) {

                    $(".address-input-disabled").removeAttr('disabled');
                    $("#search_address").removeClass("is-loading");

                    /**
                     * show modal
                     */
                    $("#pc_modal").css("display", "block");
                    $("#choose-address").css("display","none");
                    $("#address-section").show();

                    /**
                     * show next fields
                     */
                    $(".box-step2").removeClass("box-step2");

                }

            });

            /*setTimeout(function(){
                parent.postMessage(getPageHeight(),"*");
            }, 1000);*/

        });

        /**
         * Choose Address
         */
        $(document).on('change', '#choose-address', function() {

            // Remove Disabled Inputs


            $("#address-section").show();

            $(".address-input-disabled").removeAttr('disabled');

            // Populate Address Fields
            var street_name = $('option:selected', this).attr('data-street_name');
            var town = $('option:selected', this).attr('data-town');
            var county = $('option:selected', this).attr('data-county');
            var postcode = $('option:selected', this).attr('data-postcode');
            var company_name = $('option:selected', this).attr('data-company_name');
            var building_number = $('option:selected', this).attr('data-building_number');
            var sub_building_name = $('option:selected', this).attr('data-sub_building_name');
            var building_name = $('option:selected', this).attr('data-building_name');

            $("#street_name").val(street_name);
            $("#city").val(town);
            $("#county").val(county);
            $("#post_code").val(postcode);
            $("#company_name").val(company_name);
            $("#house_number").val(building_number);
            $("#flat_number").val(sub_building_name);
            $("#house_name").val(building_name);


        });
};


</script>
    <section class="register-info">
        <div class="container-fluid register-text">
            <div class="">
                COMPLETE YOUR FREE SIGN UP TODAY AND SEE WHAT OTHERS SEE ABOUT YOU!
            </div>
            <div class="">
                <h2>Check, Monitor & Improve Your Social Media Activity FREE.</h2>

                <h5>Make sure you check your details today. A poor Social Media Image may impact your ability to get credit!</h5>
            </div>

            <div class="register-points-list">
                <ul>
                    <li class="animated slideInLeft_center" style="animation-delay: 200ms"><i class="fa fa-check"></i>Social Media might impact your ability to get Credit</li>
                    <li class="animated slideInLeft_center" style="animation-delay: 600ms"><i class="fa fa-check"></i>Social Report and score</li>
                    <li class="animated slideInLeft_center" style="animation-delay: 1000ms"><i class="fa fa-check"></i>Professional support and advice available</li>
                    <li class="animated slideInLeft_center" style="animation-delay: 1400ms"><i class="fa fa-check"></i>Free alerts</li>
                    <li class="animated slideInLeft_center" style="animation-delay: 1800ms"><i class="fa fa-check"></i>Retail offers & voucher codes</li>
                    <li class="animated slideInLeft_center" style="animation-delay: 2200ms"><i class="fa fa-check"></i>Sign up for free* to find out your social image </li>
                </ul>
            </div>
            <p>* A one time fee of £39.99 automatically applies after your free trial. You may cancel at any time during your 5 day free trial. If you cancel your subscription after your 5 day free trial, you will be charged.</p>

        </div>
    </section>

    <section class="register-bar">

    </section>
    <section class="register-form" style="">
    <div class="container-fluid">

    <h5 class="already-member">Already a member? <a href="/login">Click Here</a> to log in now</h5>
        <div class="alert alert-success" style="display:none">

        </div>
        <div class="alert alert-warning" style="display:none">

        </div>
        <div id="alert-danger" class="alert alert-danger" style="display:none">

        </div>
        {{--@endphp
        @isset($mb_staff_warning)
        @if($mb_staff_warning)
        <div class="alert alert-warning" style="">
        {!! $mb_staff_warning !!}
        </div>
        @endif
        @endisset--}}

        <form id="submit_paypal" action="/register" method="post" target="_top">
            @csrf
            <input type="hidden" name="aid" value="{{$aid}}">
            <input type="hidden" name="cancel_return" id="cancel_return" value="{{ url('cancel', $unique_id)  }}" />
            <input type="hidden" name="custom" value="{{ $unique_id }}" />
            <input type="hidden" name="rm" value="2">
            <input type="hidden" name="cmd" value="_s-xclick">

            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            @if (session('warning'))
                <div class="alert alert-warning">
                    {{ session('warning') }}
                </div>
            @endif
        <div class="section-bar">
            1. Your Personal Information
        </div>
        <div id="register-1" style="">
            <div class="row">
                <div class="col-4">
                </div>
                <div id="reg-errors" class="col-4" role="alert">
                </div>
                <div class="col-4">
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-2"><label for="title">Title</label>
                    <select class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" required>
                        <option value="">Please select...</option>
                        <option value="miss"{!! $sel = (old('title') == "miss")? " selected": "" !!}>Miss</option>
                        <option value="ms"{!! $sel = (old('title') == "ms")? " selected": "" !!}>Ms</option>
                        <option value="mrs"{!! $sel = (old('title') == "mrs")? " selected": "" !!}>Mrs</option>
                        <option value="mr"{!! (old('title') == "mr")? " selected": "" !!}>Mr</option>
                        <option value="dr"{!! $sel = (old('title') == "dr")? " selected": "" !!}>Dr.</option>
                    </select>


                        <span class="invalid-feedback  err_title">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>


                </div>
                <div class="col-md-2"><label for="first_name">First Name</label>
                    <input id="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}"
                           name="first_name" value="{{ (!empty(old('first_name')))? old('first_name'): ""  }}" required >


                        <span class="invalid-feedback err_first_name">
                            <strong>{{ $errors->first('first_name') }}</strong>
                        </span>

                </div>
                <div class="col-md-2"><label for="last_name">Last Name</label>
                    <input id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ (!empty(old('last_name')))? old('last_name'): ""  }}" required>


                        <span class="invalid-feedback err_last_name">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>

                </div>
                <div class="col-md-2"><label for="mobile_number">Mobile Number</label>
                    <input class="form-control{{ $errors->has('mobile_number') ? ' is-invalid' : '' }}" name="mobile_number" value="{{ (!empty(old('mobile_number')))? old('mobile_number'): "" }}" required />

                        <span class="invalid-feedback err_mobile_number">
                                        <strong>{{ $errors->first('mobile_number') }}</strong>
                                    </span>

                </div>
                <div class="col-md-2"><label for="email">Email Address</label>
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                        <span class="invalid-feedback err_email">
                                <strong>{{ $errors->first('email') }}</strong>
                        </span>

                </div>
                <div class="col-md-4 col-lg-3 col-xl-2 dob"><label for="date_of_birth">Date of Birth</label><br>
                    <select class="form-control date-day{{ $errors->has('date_of_birth') ? ' is-invalid' : '' }}{{ $errors->has('dob_day') ? ' is-invalid' : '' }}" name="dob_day" required>
                        <option value=""></option>
                        {!! $date_values['days'] !!}
                    </select>
                    <select class="form-control date-month{{ $errors->has('dob_month') ? ' is-invalid' : '' }}" name="dob_month" required>
                        <option value=""></option>
                        {!! $date_values['months'] !!}
                    </select>
                    <select class="form-control date-year{{ $errors->has('dob_year') ? ' is-invalid' : '' }}" name="dob_year" required>
                        <option value=""></option>
                        {!! $date_values['years'] !!}
                    </select>

                        <span class="invalid-feedback err_date_of_birth">
                                        <strong>{{ $errors->first('date_of_birth') }}</strong>
                                    </span>

                </div>
            </div>
            <div class="row justify-content-center">

                <!-- post code search - START -->


                        <div class="col-md-6" style="padding: 15px 0;">
                            <span>Enter Post code</span>
                            <input class="input" type="text" placeholder="Postcode" id="postcode_search" value="">
                        </div>

                <div class="col-md-6" style="">

                            <button class="btn btn-info " id="search_address">
                                Search Address by Postcode
                            </button>

                </div>
                <!-- lookup failure -->
                <div class="col-md-12">
                    <article class="text-danger" id="pc_modal" style="display:none;">

                        <div class="message-body">
                            Unfortunately we have failed to find any addresses with the postcode you have entered. Please enter it manually below or try again.
                        </div>
                    </article>

                    <!-- no postcode entered -->
                    <article class="text-danger" id="nopc_modal" style="display:none;">

                        <div class="message-body">
                            Sorry, we can't search for your address if you don't enter a postcode. Please enter your home postcode and hit search again.
                        </div>
                    </article>
                </div>



                        <div class="col-md-12" id="address_search_populate_container" style="display:none;">
                            <label class="label">Please Select Your Address</label>

                                <span class="" id="address_search_populate">

                                </span>

                        </div>


                <!-- post code search - END -->
            </div>
            <div id="address-section" class="row justify-content-center" style="display:none">
                <div class="col-md-2"><label for="house_number">House Number</label>
                    <input class="form-control{{ $errors->has('house_number') ? ' is-invalid' : '' }}"  id="house_number" name="house_number" value="{{ ((old()))? old('house_number'): "" }}" >

                        <span class="invalid-feedback err_house_number">
                                        <strong>{{ $errors->first('house_number') }}</strong>
                                    </span>

                </div>
                <div class="col-md-2"><label for="house_name">House Name</label>
                    <input class="form-control{{ $errors->has('house_name') ? ' is-invalid' : '' }}" id="house_name"  name="house_name" value="{{ ((old()))? old('house_name'): "" }}" >

                        <span class="invalid-feedback err_house_name">
                                        <strong>{{ $errors->first('house_name') }}</strong>
                                    </span>

                </div>
                <div class="col-md-2"><label for="flat_number">Flat Number</label>
                    <input class="form-control{{ $errors->has('flat_number') ? ' is-invalid' : '' }}" id="flat_number" name="flat_number" value="{{ (old())? old('flat_number'): "" }}" >

                        <span class="invalid-feedback err_flat_number">
                                        <strong>{{ $errors->first('flat_number') }}</strong>
                                    </span>

                </div>
                <div class="col-md-2"><label for="street_name">Street Name</label>
                    <input class="form-control{{ $errors->has('street_name') ? ' is-invalid' : '' }}" id="street_name" name="street_name" value="{{ (old())? old('street_name'): "" }}" required>

                        <span class="invalid-feedback err_street_name">
                                        <strong>{{ $errors->first('street_name') }}</strong>
                                    </span>

                </div>
                <div class="col-md-2"><label for="city">City</label>
                    <input class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" id="city" name="city" value="{{ (!empty(old('city')))? old('city'): "" }}" required>

                        <span class="invalid-feedback err_city">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>

                </div>
                <div class="col-md-2"><label for="county">County</label>
                    <input class="form-control{{ $errors->has('county') ? ' is-invalid' : '' }}" id="county" name="county" value="{{ (!empty(old('county')))? old('county'): "" }}" required>

                        <span class="invalid-feedback err_county">
                                        <strong>{{ $errors->first('county') }}</strong>
                                    </span>

                </div>
                <div class="col-md-2"><label for="post_code">Postcode</label>
                    <input class="form-control{{ $errors->has('post_code') ? ' is-invalid' : '' }}" maxlength="7" id="post_code" name="post_code" value="{{ (!empty(old('post_code')))? old('post_code'): "" }}" required>

                        <span class="invalid-feedback err_post_code">
                                        <strong>{{ $errors->first('post_code') }}</strong>
                                    </span>

                </div>


            </div>
            <div class="row justify-content-center">
                <div class="col-md-2"><label for="residential_status">Residential Status</label>
                    <select class="form-control{{ $errors->has('residential_status') ? ' is-invalid' : '' }}" size="4" name="residential_status" required>
                        <option value="home_owner"{{ $sel = (old('residential_status') == "home_owner")? " selected": "" }}>Homeowner</option>
                        <option value="private_tenant"{{ $sel = (old('residential_status') == "private_tenant")? " selected": "" }}>Private Tenant</option>
                        <option value="council_tenant"{{ $sel = (old('residential_status') == "council_tenant")? " selected": "" }}>Council Tenant</option>
                        <option value="living_with_parents"{{ $sel = (old('residential_status') == "living_with_parents")? " selected": "" }}>Living with Parents</option>
                        <option value="living_with_friends"{{ $sel = (old('residential_status') == "living_with_friends")? " selected": "" }}>Living with Friends</option>
                        <option value="student_accommodation"{{ $sel = (old('residential_status') == "student_accommodation")? " selected": "" }}>Student Accommodation</option>
                        <option value="other"{{ $sel = (old('residential_status') == "other")? " selected": "" }}>Other</option>
                    </select>

                        <span class="invalid-feedback err_residential_status">
                                        <strong>{{ $errors->first('residential_status') }}</strong>
                                    </span>

                </div>
                <div class="col-md-2"><label for="education">Education</label>
                    <select class="form-control{{ $errors->has('education') ? ' is-invalid' : '' }}" size="4" name="education" required>
                        <option value="o-levels"{{ $sel = (old('education') == "o-levels")? " selected": "" }}>O-Levels</option>
                        <option value="gcse"{{ $sel = (old('education') == "gcse")? " selected": "" }}>GCSE</option>
                        <option value="a-levels"{{ $sel = (old('education') == "a-levels")? " selected": "" }}>A-Levels</option>
                        <option value="degree"{{ $sel = (old('education') == "degree")? " selected": "" }}>Degree</option>
                        <option value="vocational"{{ $sel = (old('education') == "vocational")? " selected": "" }}>Vocational</option>
                        <option value="masters"{{ $sel = (old('education') == "masters")? " selected": "" }}>Masters</option>
                        <option value="doctorate"{{ $sel = (old('education') == "doctorate")? " selected": "" }}>Doctorate</option>
                        <option value="other"{{ $sel = (old('education') == "other")? " selected": "" }}>Other</option>

                    </select>

                        <span class="invalid-feedback err_education">
                                        <strong>{{ $errors->first('education') }}</strong>
                                    </span>


                </div>
                <div class="col-md-2"><label for="employment_status">Employment Status</label>
                    <select class="form-control{{ $errors->has('employment_status') ? ' is-invalid' : '' }}" size="4" name="employment_status" required>

                        <option value="full_time"{{ $sel = (old('employment_status') == "full_time")? " selected": "" }}>Full Time</option>
                        <option value="part_time"{{ $sel = (old('employment_status') == "part_time")? " selected": "" }}>Part Time</option>
                        <option value="temporary"{{ $sel = (old('employment_status') == "temporary")? " selected": "" }}>Temporary</option>
                        <option value="self_employed"{{ $sel = (old('employment_status') == "self_employed")? " selected": "" }}>Self Employed</option>
                        <option value="unemployed"{{ $sel = (old('employment_status') == "unemployed")? " selected": "" }}>Unemployed</option>
                        <option value="pension"{{ $sel = (old('employment_status') == "pension")? " selected": "" }}>Pension</option>
                        <option value="disability"{{ $sel = (old('employment_status') == "disability")? " selected": "" }}>Disability</option>
                        <option value="benefits"{{ $sel = (old('employment_status') == "benefits")? " selected": "" }}>Benefits</option>
                        <option value="student"{{ $sel = (old('employment_status') == "student")? " selected": "" }}>Student</option>
                    </select>

                        <span class="invalid-feedback err_employment_status">
                                        <strong>{{ $errors->first('employment_status') }}</strong>
                                    </span>

                </div>
                <div class="col-md-2"><label for="marital_status">Marital Status</label>
                    <select class="form-control{{ $errors->has('marital_status') ? ' is-invalid' : '' }}" size="4" name="marital_status" required>

                        <option value="single"{{ $sel = (old('marital_status') == "single")? " selected": "" }}>Single</option>
                        <option value="married"{{ $sel = (old('marital_status') == "married")? " selected": "" }}>Married</option>
                        <option value="living_together"{{ $sel = (old('marital_status') == "living_together")? " selected": "" }}>Living Together</option>
                        <option value="separated"{{ $sel = (old('marital_status') == "separated")? " selected": "" }}>Separated</option>
                        <option value="divorced"{{ $sel = (old('marital_status') == "divorced")? " selected": "" }}>Divorced</option>
                        <option value="widowed"{{ $sel = (old('marital_status') == "widowed")? " selected": "" }}>Widowed</option>
                        <option value="other"{{ $sel = (old('marital_status') == "other")? " selected": "" }}>Other</option>
                    </select>

                        <span class="invalid-feedback err_marital_status">
                                        <strong>{{ $errors->first('marital_status') }}</strong>
                                    </span>

                </div>

                <div class="col-md-3"><label for="social_media_check_reason">Why are you checking your Social Media Actiivty?</label>
                    <select class="form-control{{ $errors->has('social_media_check_reason') ? ' is-invalid' : '' }}" size="4" name="social_media_check_reason" required>

                        <option value="help_get_a_job"{{ $sel = (old('social_media_check_reason') == "help_get_a_job")? " selected": "" }}>To help me get a job</option>
                        <option value="be_better_at_social_media"{{ $sel = (old('social_media_check_reason') == "be_better_at_social_media")? " selected": "" }}>Be better at social media</option>
                        <option value="manage_social_accounts_centrally"{{ $sel = (old('social_media_check_reason') == "manage_social_accounts_centrally")? " selected": "" }}>Manage social media in one place</option>
                        <option value="improve_credit_score"{{ $sel = (old('social_media_check_reason') == "improve_credit_score")? " selected": "" }}>Improve my credit score</option>

                    </select>

                        <span class="invalid-feedback err_social_media_check_reason">
                                        <strong>{{ $errors->first('social_media_check_reason') }}</strong>
                                    </span>

                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-4"><label for="password">Password</label>
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>


                        <span class="invalid-feedback err_password">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>

                </div>
                <div class="col-md-4"><label for="password_confirmation">Confirm Password</label>
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                </div>
                <div class="col-12">
                    <button type="button" id="next-1" class="btn btn-info">2. Next Step</button>
                </div>
            </div>
        </div>

        </form>

        <div id="register-2" style="{{ (!old())? "display:none":""  }}">
            <div class="section-bar">
                2. Payment information
            </div>

            <form id="payment-form" action="/charge" method="post">
                @csrf
                <div class="form-row">
                    <div class="col-12">
                        <label for="card-element">
                            You will only be charged after your 5 day free trial. You can cancel your trial at any time.
                        </label>
                    </div>
                    <div class="col-12">
                        <label for="card-element">
                            Credit or debit card
                        </label>
                    </div>

                    <div class="col-12 col-sm-4">
                        <img alt="stripe logos" class="cards" src="/img/logos/secure3D-logos.png"/>
                    </div>
                    <div id="card-element" class="col-12 col-sm-4 mt-3 mt-sm-0">
                        <!-- A Stripe Element will be inserted here. -->
                    </div>
                    <div class="col-12 col-sm-4 mt-3 mt-sm-0">
                        <img alt="card logos" class="cards" src="/img/logos/cards-logos.png"/>
                    </div>

                    <div class="col-12 col-sm-4">
                    </div>
                    <div id="card-errors" class="col-12 col-sm-4 mt-3 mt-sm-0" role="alert"></div>
                    <div class="col-12 col-sm-4">
                    </div>
                </div>

                <div class="form-row">
                    <div class="col-1 col-sm-5">
                    </div>
                    <div class="col-10 col-sm-2 text-center">
                        <div class="login-register site-color-text-1" style="width:50%;margin: 8% auto;">
                            <button id="submit-payment" class="nav-link site-background-1">Submit Payment</button>
                        </div>
                    </div>
                    <div class="col-1 col-sm-5">
                    </div>
                </div>
            </form>

        </div>

</div>
    </section>
@endsection
