@extends("templates.layout")

@section ('site_content')


<script>

window.onload = function(){


    $('#next-1').click(function(){

        <?php
        (!old())? "$(\"#register-1\").hide();":"";
        ?>
        $("#register-2").show();
        $('#next-1').hide();
        //alert("hello world!");
    });

    if($('#register-2').is(":visible") ){
        $('#next-1').hide();
    }

    $('#next-2').click(function(){

        //$("#register-2").hide();
        $("#register-3").show();
        //alert("hello world!");
    });


    $('#register-btn').click(function(){

        $("#register-1").show();
        $("#next-1").hide();
        //$("#register-2").show();
        //alert("hello world!");
    });

    //alert("Hello world: test script");

        /**
         * Address Lookup
         */

        if ($('#post_code').val().length != 0){
            $("#address-section").show();
        }

        $("#search_address").click(function() {

            // alert("Hello world!");

            /**
             * remove the nopc modal if available
             */
            if ($("#nopc_modal").is(":visible"))
            {

                $("#nopc_modal").css("display","none");

            }

            if($("#nopc_modal").css("display","block")){
                $("#nopc_modal").css("display","none");
            }

            $("#pc_modal").css("display","none");

            /**
             * set the button to a loader
             */
            $(this).addClass("is-loading");

            /**
             * setup the variables
             */
            var post_code = $("#postcode_search").val();
            var url = "/address_lookup/" + post_code;

            /**
             * check field is not empty
             */
            if (post_code == "")
            {

                $(this).removeClass("is-loading");
                $("#nopc_modal").css("display","block");
                $("#choose-address").css("display","none");

                location.href = '#nopc_modal';

                return false;

            }

            $.ajax({

                type: "GET",
                dataType: 'json',
                url: url,
                success: function(response) {

                    $("#address_search_populate").html(response.delivery_point_select);
                    $("#address_search_populate_container").slideDown();

                    $("#search_address").removeClass("is-loading");

                    /**
                     * show next fields
                     */
                    $(".box-step2").removeClass("box-step2");

                },

                error: function(response) {

                    $(".address-input-disabled").removeAttr('disabled');
                    $("#search_address").removeClass("is-loading");

                    /**
                     * show modal
                     */
                    $("#pc_modal").css("display", "block");
                    $("#choose-address").css("display","none");
                    $("#address-section").show();

                    /**
                     * show next fields
                     */
                    $(".box-step2").removeClass("box-step2");

                }

            });

            setTimeout(function(){
                parent.postMessage(getPageHeight(),"*");
            }, 1000);

        });

        /**
         * Choose Address
         */
        $(document).on('change', '#choose-address', function() {

            // Remove Disabled Inputs


            $("#address-section").show();

            $(".address-input-disabled").removeAttr('disabled');

            // Populate Address Fields
            var street_name = $('option:selected', this).attr('data-street_name');
            var town = $('option:selected', this).attr('data-town');
            var county = $('option:selected', this).attr('data-county');
            var postcode = $('option:selected', this).attr('data-postcode');
            var company_name = $('option:selected', this).attr('data-company_name');
            var building_number = $('option:selected', this).attr('data-building_number');
            var sub_building_name = $('option:selected', this).attr('data-sub_building_name');
            var building_name = $('option:selected', this).attr('data-building_name');

            $("#street_name").val(street_name);
            $("#city").val(town);
            $("#county").val(county);
            $("#post_code").val(postcode);
            $("#company_name").val(company_name);
            $("#house_number").val(building_number);
            $("#flat_number").val(sub_building_name);
            $("#house_name").val(building_name);


        });
};


</script>
    <section class="register-info">
        <div class="container-fluid register-text">
            <div class="intro-heading">
                Register below to start your FREE * trial today!
            </div>

            <div class="register-points-list">
                <ul>
                    <li class="animated slideInLeft_center" style="animation-delay: 200ms"><i class="fa fa-check"></i>Unlimited access to your Social Report and score</li>
                    <li class="animated slideInLeft_center" style="animation-delay: 600ms"><i class="fa fa-check"></i>Unlimited advice on improving your social image</li>
                    <li class="animated slideInLeft_center" style="animation-delay: 1000ms"><i class="fa fa-check"></i>Free alerts</li>
                </ul>

            </div>
            <p>* A monthly subscription fee of £9.99 automatically applies after your free trial. You may cancel at any
            time during your 15 day free trial. Please note, if you cancel your subscription at any time after your 15 day
            free trial, you will be charged.</p>

        </div>
    </section>

    <section class="register-bar">

    </section>
    <section class="register-form" style="">
    <div class="container">
    <h2 class="">Already a member? <a href="/login">Click Here</a> to log in now</h2>
        <form method="POST" action="{{ route('register') }}">
            @csrf
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            @if (session('warning'))
                <div class="alert alert-warning">
                    {{ session('warning') }}
                </div>
            @endif
        <div class="section-bar">
            1. Your Personal Information
        </div>
        <div id="register-1" style="">
            <div class="row"> <!-- Row | Reg form - START -->
                <div class="col-xs-12 col-sm-7">

                    <div class=""><label for="title">Title</label>
                        <select class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" required>
                            <option value="">Please select...</option>
                            <option value="miss"{!! $sel = (old('title') == "miss")? " selected": (($test_data->title == "miss")? " selected":"") !!}>Miss</option>
                            <option value="ms"{!! $sel = (old('title') == "ms")? " selected": (($test_data->title == "ms")? " selected":"") !!}>Ms</option>
                            <option value="mrs"{!! $sel = (old('title') == "mrs")? " selected": (($test_data->title == "mrs")? " selected":"") !!}>Mrs</option>
                            <option value="mr"{!! (old('title') == "mr")? " selected": (($test_data->title == "mr")? " selected":"") !!}>Mr</option>
                            <option value="dr"{!! $sel = (old('title') == "dr")? " selected": (($test_data->title == "dr")? " selected":"") !!}>Dr.</option>
                        </select>

                        @if ($errors->has('title'))
                            <span class="invalid-feedback">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
                        @endif

                    </div>
                    <div class=""><label for="first_name">First Name</label>
                        <input id="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}"
                               name="first_name" value="{{ (!empty(old('first_name')))? old('first_name'): $test_data->first_name  }}" required >

                        @if ($errors->has('first_name'))
                            <span class="invalid-feedback">
                            <strong>{{ $errors->first('first_name') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class=""><label for="last_name">Last Name</label>
                        <input id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ (!empty(old('last_name')))? old('last_name'): $test_data->last_name  }}" required>

                        @if ($errors->has('last_name'))
                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class=""><label for="mobile_number">Mobile Number</label>
                        <input class="form-control{{ $errors->has('mobile_number') ? ' is-invalid' : '' }}" name="mobile_number" value="{{ (!empty(old('mobile_number')))? old('mobile_number'): $test_data->mobile_number }}" required />
                        @if ($errors->has('mobile_number'))
                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('mobile_number') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class=""><label for="email_address">Email Address</label>
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="date_of_birth">Date of Birth</label>
                    </div>
                    <div class="row">

                        <div class="col-4">


                            <select class="form-control{{ $errors->has('date_of_birth') ? ' is-invalid' : '' }}{{ $errors->has('dob_day') ? ' is-invalid' : '' }}" name="dob_day" required>
                                <option value=""></option>
                                {!! $date_values['days'] !!}
                            </select>
                        </div>
                        <div class="col-4">
                            <select class="form-control{{ $errors->has('dob_month') ? ' is-invalid' : '' }}" name="dob_month" required>
                                <option value=""></option>
                                {!! $date_values['months'] !!}
                            </select>

                        </div>
                        <div class="col-4">
                            <select class="form-control{{ $errors->has('dob_year') ? ' is-invalid' : '' }}" name="dob_year" required>
                                <option value=""></option>
                                {!! $date_values['years'] !!}
                            </select>

                        </div>

                    </div>
                    <div class="">


                        @if ($errors->has('date_of_birth'))
                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('date_of_birth') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <!-- post code search - START -->


                    <div class="" style="padding: 15px 0;">

                        <input class="input" type="text" placeholder="Postcode" id="postcode_search" value="">


                        <button class="btn btn-info-new" id="search_address">
                            Search Address by Postcode
                        </button>

                    </div>
                    <!-- lookup failure -->

                        <article class="text-danger" id="pc_modal" style="display:none;">

                            <div class="message-body">
                                Unfortunately we have failed to find any addresses with the postcode you have entered. Please enter it manually below or try again.
                            </div>
                        </article>

                        <!-- no postcode entered -->
                        <article class="text-danger" id="nopc_modal" style="display:none;">

                            <div class="message-body">
                                Sorry, we can't search for your address if you don't enter a postcode. Please enter your home postcode and hit search again.
                            </div>
                        </article>

                    <div class="col-md-12" id="address_search_populate_container" style="display:none;">
                        <label class="label">Please Select Your Address</label>

                        <span class="" id="address_search_populate">

                                </span>

                    </div>


                    <!-- post code search - END -->
                </div>
                <div class="hidden-xs col-sm-5">


                </div>


            </div> <!-- Row | Reg form - END -->
            <div class="row justify-content-center">


            </div>
            <div id="address-section" class="row justify-content-center" style="display:none">
                <div class="col-md-2"><label for="house_number">House Number</label>
                    <input class="form-control{{ $errors->has('house_number') ? ' is-invalid' : '' }}"  id="house_number" name="house_number" value="{{ ((old()))? old('house_number'): $test_data->house_number }}" >
                    @if ($errors->has('house_number'))
                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('house_number') }}</strong>
                                    </span>
                    @endif
                </div>
                <div class="col-md-2"><label for="house_name">House Name</label>
                    <input class="form-control{{ $errors->has('house_name') ? ' is-invalid' : '' }}" id="house_name"  name="house_name" value="{{ ((old()))? old('house_name'): $test_data->house_name }}" >
                    @if ($errors->has('house_name'))
                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('house_name') }}</strong>
                                    </span>
                    @endif
                </div>
                <div class="col-md-2"><label for="flat_number">Flat Number</label>
                    <input class="form-control{{ $errors->has('flat_number') ? ' is-invalid' : '' }}" id="flat_number" name="flat_number" value="{{ (old())? old('flat_number'): $test_data->flat_number }}" >
                    @if ($errors->has('flat_number'))
                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('flat_number') }}</strong>
                                    </span>
                    @endif
                </div>
                <div class="col-md-2"><label for="street_name">Street Name</label>
                    <input class="form-control{{ $errors->has('street_name') ? ' is-invalid' : '' }}" id="street_name" name="street_name" value="{{ (old())? old('street_name'): $test_data->street_name }}" required>
                    @if ($errors->has('street_name'))
                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('street_name') }}</strong>
                                    </span>
                    @endif
                </div>
                <div class="col-md-2"><label for="city">City</label>
                    <input class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" id="city" name="city" value="{{ (!empty(old('city')))? old('city'): $test_data->city }}" required>
                    @if ($errors->has('city'))
                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                    @endif
                </div>
                <div class="col-md-2"><label for="county">County</label>
                    <input class="form-control{{ $errors->has('county') ? ' is-invalid' : '' }}" id="county" name="county" value="{{ (!empty(old('county')))? old('county'): $test_data->county }}" required>
                    @if ($errors->has('county'))
                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('county') }}</strong>
                                    </span>
                    @endif
                </div>
                <div class="col-md-2"><label for="post_code">Postcode</label>
                    <input class="form-control{{ $errors->has('post_code') ? ' is-invalid' : '' }}" maxlength="7" id="post_code" name="post_code" value="{{ (!empty(old('post_code')))? old('post_code'): $test_data->post_code }}" required>
                    @if ($errors->has('post_code'))
                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('post_code') }}</strong>
                                    </span>
                    @endif
                </div>


            </div>
            <div class="row justify-content-center">
                <div class="col-md-2"><label for="residential_status">Residential Status</label>
                    <select class="form-control{{ $errors->has('residential_status') ? ' is-invalid' : '' }}" size="4" name="residential_status" required>
                        <option value="home_owner"{{ $sel = (old('residential_status') == "home_owner")? " selected": (($test_data->residential_status == "home_owner")? " selected":"") }}>Homeowner</option>
                        <option value="private_tenant"{{ $sel = (old('residential_status') == "private_tenant")? " selected": (($test_data->residential_status == "private_tenant")? " selected":"") }}>Private Tenant</option>
                        <option value="council_tenant"{{ $sel = (old('residential_status') == "council_tenant")? " selected": (($test_data->residential_status == "council_tenant")? " selected":"") }}>Council Tenant</option>
                        <option value="living_with_parents"{{ $sel = (old('residential_status') == "living_with_parents")? " selected": (($test_data->residential_status == "living_with_parents")? " selected":"") }}>Living with Parents</option>
                        <option value="living_with_friends"{{ $sel = (old('residential_status') == "living_with_friends")? " selected": (($test_data->residential_status == "living_with_friends")? " selected":"") }}>Living with Friends</option>
                        <option value="student_accommodation"{{ $sel = (old('residential_status') == "student_accommodation")? " selected": (($test_data->residential_status == "student_accommodation")? " selected":"") }}>Student Accommodation</option>
                        <option value="other"{{ $sel = (old('residential_status') == "other")? " selected": (($test_data->residential_status == "other")? " selected":"") }}>Other</option>
                    </select>
                    @if ($errors->has('residential_status'))
                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('residential_status') }}</strong>
                                    </span>
                    @endif
                </div>
                <div class="col-md-2"><label for="education">Education</label>
                    <select class="form-control{{ $errors->has('education') ? ' is-invalid' : '' }}" size="4" name="education" required>
                        <option value="o-levels"{{ $sel = (old('education') == "o-levels")? " selected": "" }}>O-Levels</option>
                        <option value="gcse"{{ $sel = (old('education') == "gcse")? " selected": "" }}>GCSE</option>
                        <option value="a-levels"{{ $sel = (old('education') == "a-levels")? " selected": "" }}>A-Levels</option>
                        <option value="degree"{{ $sel = (old('education') == "degree")? " selected": "" }}>Degree</option>
                        <option value="vocational"{{ $sel = (old('education') == "vocational")? " selected": "" }}>Vocational</option>
                        <option value="masters"{{ $sel = (old('education') == "masters")? " selected": "" }}>Masters</option>
                        <option value="doctorate"{{ $sel = (old('education') == "doctorate")? " selected": "" }}>Doctorate</option>
                        <option value="other"{{ $sel = (old('education') == "other")? " selected": "" }}>Other</option>

                    </select>
                    @if ($errors->has('education'))
                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('education') }}</strong>
                                    </span>
                    @endif

                </div>
                <div class="col-md-2"><label for="employment_status">Employment Status</label>
                    <select class="form-control{{ $errors->has('employment_status') ? ' is-invalid' : '' }}" size="4" name="employment_status" required>

                        <option value="full_time"{{ $sel = (old('employment_status') == "full_time")? " selected": "" }}>Full Time</option>
                        <option value="part_time"{{ $sel = (old('employment_status') == "part_time")? " selected": "" }}>Part Time</option>
                        <option value="temporary"{{ $sel = (old('employment_status') == "temporary")? " selected": "" }}>Temporary</option>
                        <option value="self_employed"{{ $sel = (old('employment_status') == "self_employed")? " selected": "" }}>Self Employed</option>
                        <option value="unemployed"{{ $sel = (old('employment_status') == "unemployed")? " selected": "" }}>Unemployed</option>
                        <option value="pension"{{ $sel = (old('employment_status') == "pension")? " selected": "" }}>Pension</option>
                        <option value="disability"{{ $sel = (old('employment_status') == "disability")? " selected": "" }}>Disability</option>
                        <option value="benefits"{{ $sel = (old('employment_status') == "benefits")? " selected": "" }}>Benefits</option>
                        <option value="student"{{ $sel = (old('employment_status') == "student")? " selected": "" }}>Student</option>
                    </select>
                    @if ($errors->has('employment_status'))
                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('employment_status') }}</strong>
                                    </span>
                    @endif
                </div>
                <div class="col-md-2"><label for="marital_status">Marital Status</label>
                    <select class="form-control{{ $errors->has('marital_status') ? ' is-invalid' : '' }}" size="4" name="marital_status" required>

                        <option value="single"{{ $sel = (old('marital_status') == "single")? " selected": "" }}>Single</option>
                        <option value="married"{{ $sel = (old('marital_status') == "married")? " selected": "" }}>Married</option>
                        <option value="living_together"{{ $sel = (old('marital_status') == "living_together")? " selected": "" }}>Living Together</option>
                        <option value="separated"{{ $sel = (old('marital_status') == "separated")? " selected": "" }}>Separated</option>
                        <option value="divorced"{{ $sel = (old('marital_status') == "divorced")? " selected": "" }}>Divorced</option>
                        <option value="widowed"{{ $sel = (old('marital_status') == "widowed")? " selected": "" }}>Widowed</option>
                        <option value="other"{{ $sel = (old('marital_status') == "other")? " selected": "" }}>Other</option>
                    </select>
                    @if ($errors->has('marital_status'))
                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('marital_status') }}</strong>
                                    </span>
                    @endif
                </div>
                <?php /*
                <div class="col-md-2"><label for="car">Car</label>
                    <select class="form-control{{ $errors->has('car') ? ' is-invalid' : '' }}" size="4" name="car" required>

                        <option value="none"{{ $sel = (old('car') == "none")? " selected": "" }}>None</option>
                        <option value="fully_owned"{{ $sel = (old('car') == "fully_owned")? " selected": "" }}>Fully Owned</option>
                        <option value="financed"{{ $sel = (old('car') == "financed")? " selected": "" }}>Financed</option>
                        <option value="leased"{{ $sel = (old('car') == "leased")? " selected": "" }}>Leased</option>

                    </select>
                    @if ($errors->has('car'))
                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('car') }}</strong>
                                    </span>
                    @endif
                </div> */ ?>
                <div class="col-md-3"><label for="social_media_check_reason">Why are you checking your Social Media Actiivty?</label>
                    <select class="form-control{{ $errors->has('social_media_check_reason') ? ' is-invalid' : '' }}" size="4" name="social_media_check_reason" required>

                        <option value="help_get_a_job"{{ $sel = (old('social_media_check_reason') == "help_get_a_job")? " selected": "" }}>To help me get a job</option>
                        <option value="be_better_at_social_media"{{ $sel = (old('social_media_check_reason') == "be_better_at_social_media")? " selected": "" }}>Be better at social media</option>
                        <option value="manage_social_accounts_centrally"{{ $sel = (old('social_media_check_reason') == "manage_social_accounts_centrally")? " selected": "" }}>Manage social media in one place</option>
                        <option value="improve_credit_score"{{ $sel = (old('social_media_check_reason') == "improve_credit_score")? " selected": "" }}>Improve my credit score</option>

                    </select>
                    @if ($errors->has('social_media_check_reason'))
                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('social_media_check_reason') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-4"><label for="password">Password</label>
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                    @if ($errors->has('password'))
                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif
                </div>
                <div class="col-md-4"><label for="password_confirmation">Confirm Password</label>
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                </div>
                <div class="col-12">
                    <button type="button" id="next-1" class="btn btn-info">Next</button>
                </div>
            </div>
        </div>
        <div class="section-bar">
            2. Payment information
        </div>
        <div id="register-2" style="{{ (!old())? "display:none":""  }}">
            <p>*** Payment information fields here ***</p>
            <?php /*
            <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
                <INPUT TYPE="hidden" name="variableName" value="allowedValue">

                <INPUT TYPE="hidden" name="cmd" value="_xclick">
            </form>
            <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
                <input type="hidden" name="cmd" value="_xclick">
                <input type="hidden" name="business" value="Mediablanket Ltd T/A Knowso.co.uk">
                <input type="hidden" name="item_name" value="Subscription">
                <input type="hidden" name="item_number" value="1">
                <input type="hidden" name="amount" value="15.00">
                <input type="hidden" name="no_shipping" value="0">
                <input type="hidden" name="no_note" value="1">
                <input type="hidden" name="currency_code" value="GBP">
                <input type="hidden" name="lc" value="AU">
                <input type="hidden" name="bn" value="PP-BuyNowBF">

                Credit Card:<input name="card_number" maxlength="20">
                Account Number:<input name="account_number" maxlength="20">
                Sort Code:<input name="sort_code" maxlength="20">
                Card Expiry:<input name="card_expiry" maxlength="20">
                <input type="image" src="https://www.paypal.com/en_AU/i/btn/btn_buynow_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online.">
                <img alt="" border="0" src="https://www.paypal.com/en_AU/i/scr/pixel.gif" width="1" height="1">
            </form>
 */?>
            <div class="col-12">
                {{--<button id="back-1" class="btn btn-info">Back</button>--}}
                <button id="register-btn" type="submit" class="btn btn-primary">
                    Register
                </button>
               {{-- <button id="next-2" class="btn btn-info">Next</button> --}}
            </div>
        </div>
        </form>
        <div class="section-bar">
            3. Log in to Knowso
        </div>
        <div id="register-3" style="display:none">
            <p>*** Log in to knowso here! ***</p>
            {{--<button id="back-2" class="btn btn-info">Back</button>--}}
            <div class="col-12">
                <a href="/login">Log in here</a>
            </div>
        </div>


    <div class="row justify-content-center">
        <div class="col-md-12">
            {{-- <div class="card card-default">
                <div class="card-header">Register</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf



                        <div class="form-group row">
                            <label for="first_name" class="col-md-4 col-form-label text-md-right">First Name</label>

                            <div class="col-md-6">
                                <input id="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name') }}" required >

                                @if ($errors->has('first_name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="last_name" class="col-md-4 col-form-label text-md-right">Last Name</label>

                            <div class="col-md-6">
                                <input id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ old('last_name') }}" required>

                                @if ($errors->has('last_name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div> --}}
        </div>
    </div>
</div>
    </section>
@endsection
