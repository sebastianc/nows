<?php
/* // Background Debug */
$background = "";
$get = filter_input_array(INPUT_GET);
//echo "GET: <pre>". print_r(filter_input_array(INPUT_GET), true)."</pre>";exit;
if(isset($get['back1'])){
    $background = "background-image: url('../img/header-bg.jpg')";
}
/* // Background Debug */ /* END */
?>

@extends('templates.layout')

{{-- @section('title', 'WelcomE') --}}

@section('site_content')
<!-- Header -->
<style type="text/css">
    .demo{
        background-color:red;
        width:100px;
        height:100px;
        margin:300px;
    }
</style>
<header class="masthead" style="{{ $background }}">
    <div class="container">

        <div class="intro-text">
            <div class="intro-heading site-color-text-2">The number one site to see how others see you online</div>
            <div class="intro-heading-sub site-color-text-1">Need to change your online image? Find out today!</div>
            {{--<div class="col-md-4">--}}
            <div class="intro-points-list site-color-text-1">
                <ul>
                    <li class="animated slideInLeft" style="animation-delay: 200ms"><i class="fa fa-check"></i>Social Media might impact your ability to get a loan</li>
                    <li class="animated slideInLeft" style="animation-delay: 600ms"><i class="fa fa-check"></i>Social Report and score</li>
                    <li class="animated slideInLeft" style="animation-delay: 1000ms"><i class="fa fa-check"></i>Professional support and advice available</li>
                    <li class="animated slideInLeft" style="animation-delay: 1400ms"><i class="fa fa-check"></i>Free alerts</li>
                    <li class="animated slideInLeft" style="animation-delay: 1800ms"><i class="fa fa-check"></i>Retail offers & voucher codes</li>
                    <li class="animated slideInLeft" style="animation-delay: 2200ms"><i class="fa fa-check"></i>Sign up for free* to find out your social image </li>
                </ul>
            </div>
            <div class="intro-sign-up-btn">
                <a href="/register/@isset($get['aid']){{$get['aid']}}@endisset">
                    {{--#FF8080--}}
                    <button class="btn btn-warning animated pulse no-hover">5 day free trial</button>
                </a>

            </div>


        </div>
    </div>
    <div class="monthly-subs-banner">
        <p>* A one time fee of £39.99 automatically applies after your free trial. You may cancel at any time during your 5 day free trial. If you cancel your subscription after your 5 day free trial, you will be charged.</p>
    </div>
</header>

<!-- what-is-it -->
<section id="what-is-it">
    <div class="container-fluid what-is-it">
        <div class="row">
            <div class="col-sm-6 text-center">
                <h2 class="section-heading site-color-text-1">What is your social report?</h2>
                <p>Your social report is essentially an image of your online presence across
                all of your social networks. Knowso can help you link all of your social profiles in a single place
                and provide you with a report on how other people see you as well as giving you a score out of 100</p>
                <p>Knowso is also a useful tool which helps you identify your day to day social media movements and offers
                suggestions to help you make any improvements. The facility plays a major role in helping you locate products and
                services that you may require.</p>
                <p>So, if you like the sound of it, sign up by clicking below.</p>
                <div class="what-is-it-btn" style="width:100%">
                    <a href="/register/@isset($get['aid']){{$get['aid']}}@endisset">
                        <button class="btn btn-warning no-hover">Check Report Now <i class="fa fa-chevron-right"></i></button>
                    </a>

                </div>
            </div>
            <div class="col-sm-6 text-center">
                <h2 class="section-heading site-color-text-1">Social networks covered?</h2>
                <div style="display:inline-block; margin: 0 10px;">
                    <div class="social-icon-holder site-border-color-icon social-icon-fb">
                        <i class="fa fa-facebook fa-inverse"></i>

                    </div>
                    <p>Facebook</p>
                </div>

                <div style="display:inline-block; margin: 0 10px;">
                    <div class="social-icon-holder site-border-color-icon social-icon-tw">
                        <i class="fa fa-twitter fa-inverse"></i>
                    </div>
                    <p>Twitter</p>
                </div>
                <div style="display:inline-block; margin: 0 10px;">
                    <div class="social-icon-holder site-border-color-icon social-icon-li">
                        <i class="fa fa-linkedin fa-inverse"></i>
                    </div>
                    <p>LinkedIn</p>
                </div><br>
                <img src="/img/background/tablet-colored.png" style="height:250px" />

            </div>
        </div>

    </div>
</section>

<!-- what-is-it -->
<section id="faq">
    <div class="container">


                <h2 class="section-heading site-color-text-1">Frequently Asked Questions</h2>
        <div class="row faq-row site-color-text-2">
            <div class="col-sm-6">
                <div class="faq-box">
                    <h4 class="site-color-text-1">What is a Social Report?</h4>
                    <p class="site-color-text-1">A social report is a record of all your social media history.
                        Applying for a social report with Knowso, will show you how others see you, just by what you post on your social media!</p>
                </div>

            </div>
            <div class="col-sm-6">
                <div class="faq-box">
                    <h4 class="site-color-text-1">How can a social report help me?</h4>
                    <p class="site-color-text-1">A social report can help our customers in many different ways. For example, if you’re looking for a new job,
                        we can show you an insight of what it’d be like to look at your profile as an outsider. Would an employer want to see you using bad language?
                        Or arguing over posts on Facebook? We can help you improve your social image if need be! If you’ve recently posted about wanting a new house
                        or a new car, Knowso has it covered for you and will provide you with all the best companies to help you along the way. </p>
                </div>

            </div>
        </div>
        <div class="row faq-row site-color-text-2">
            <div class="col-sm-6">
                <div class="faq-box">
                    <h4 class="site-color-text-1">How much will it cost me?</h4>
                    <p class="site-color-text-1">Very little! Here at Knowso, we will give you your social report, a social score and provide advice, jobs and
                        recommendations to help in the future for a free 5 day trial period! After the 5 day free trial period, Knowso will cost a one time fee of £39.99 .</p>
                </div>

            </div>
            <div class="col-sm-6">
                <div class="faq-box">
                    <h4 class="site-color-text-1">I want a refund</h4>
                    <p class="site-color-text-1">A member may request a refund, cancel or terminate this agreement at any time by calling us at 0161 711 0420 or by notifying us in writing at: Knowso Terminations, Mediablanket Ltd. 1 Spinningfields. Manchester. M3 3JE. England.</p>
                </div>

            </div>
        </div>

        <div class="row faq-row site-color-text-2">
            <div class="col-sm-6">
                <div class="faq-box">
                    <h4 class="site-color-text-1">How can I contact you?</h4>
                    <p class="site-color-text-1">If you have any questions regarding how we work, you can contact us on
                        0161 711 0415 or email the team at hello@mediablanket.co.uk and we will be happy to help you. </p>
                </div>

            </div>
            <div class="col-sm-6">
                <div class="faq-box">
                    <h4 class="site-color-text-1">I can't quite decide yet</h4>
                    <p class="site-color-text-1">No problem! In our opinion, Knowso really helps a lot of people,
                        from day to day to life like looking for a loan, or looking for a house or a holiday, to helping you look your best on your social media accounts.
                        However, if you’re undecided, you can have a think about it and come back to us whenever you’re ready. </p>
                </div>

            </div>
        </div>
        <div class="row faq-row site-color-text-2">
            <div class="col-sm-3">
            </div>
            <div class="col-sm-6">
                <div class="faq-box">
                    <a href="/social-media-and-loans">
                        <h4 class="site-color-text-1">Social Media & Loans</h4>
                        <p class="site-color-text-1">Many say Social Media has the ability to identify potential new borrowers who qualify for credit but haven’t had easy access to it...</p>
                    </a>
                </div>
            </div>
            <div class="col-sm-3">
            </div>
        </div>
        <?php /*
        <div class="row site-color-text-2">
                <div class="col-sm-6">
                    <div class="faq-box">
                        <h4 class="site-color-text-1">What is a Social Report?</h4>
                        <p class="site-color-text-1">A social report is a record of all your social media history.
                            Applying for a social report with Knowso, will show you how others see you, just by what you post on your social media!</p>
                    </div>
                    <div class="faq-box">
                        <h4 class="site-color-text-1">How much will it cost me?</h4>
                        <p class="site-color-text-1">Nothing! Here at Knowso, we will give you your social report, a social score and provide advice and
                            recommendations to help in the future for FREE, for the first whole month! After a month, Knowso will cost a small fee of £9.99 a month.</p>
                    </div>
                    <div class="faq-box">
                        <h4 class="site-color-text-1">How can I contact you?</h4>
                        <p class="site-color-text-1">If you have any questions regarding how we work, you can contact us on
                            0161 711 0415 or email the team at hello@mediablanket.co.uk and we will be happy to help you. </p>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="faq-box">
                        <h4 class="site-color-text-1">How can a social report help me?</h4>
                        <p class="site-color-text-1">A social report can help our customers in many different ways. For example, if you’re looking for a new job,
                            we can show you an insight of what it’d be like to look at your profile as an outsider. Would an employer want to see you using bad language?
                            Or arguing over posts on Facebook? We can help you improve your social image if need be! If you’ve recently posted about wanting a new house
                            or a new car, Knowso has it covered for you and will provide you with all the best companies to help you along the way. </p>
                    </div>
                    <div class="faq-box">
                        <h4 class="site-color-text-1">I want to cancel my subscription</h4>
                        <p class="site-color-text-1">Cancelling your subscription with Knowso is easy,
                            simply click the unsubscribe button at the bottom of our page, enter your details and we won’t contact you again.</p>
                    </div>
                    <div class="faq-box">
                        <h4 class="site-color-text-1">I can't quite decide yet</h4>
                        <p class="site-color-text-1">No problem! In our opinion, Knowso really helps a lot of people,
                            from day to day to life like looking for a loan, or looking for a house or a holiday, to helping you look your best on your social media accounts.
                            However, if you’re undecided, you can have a think about it and come back to us whenever you’re ready. </p>
                    </div>
                </div>

        </div>
        */?>
    </div>


</section>
<?php /*
<!-- Contact -->
<section id="contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading text-uppercase">Contact Us</h2>
                <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <form id="contactForm" name="sentMessage" novalidate>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input class="form-control" id="name" type="text" placeholder="Your Name *" required data-validation-required-message="Please enter your name.">
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="form-group">
                                <input class="form-control" id="email" type="email" placeholder="Your Email *" required data-validation-required-message="Please enter your email address.">
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="form-group">
                                <input class="form-control" id="phone" type="tel" placeholder="Your Phone *" required data-validation-required-message="Please enter your phone number.">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <textarea class="form-control" id="message" placeholder="Your Message *" required data-validation-required-message="Please enter a message."></textarea>
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-12 text-center">
                            <div id="success"></div>
                            <button id="sendMessageButton" class="btn btn-primary btn-xl text-uppercase" type="submit">Send Message</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
 */?>
@endsection

