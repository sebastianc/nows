@extends("templates.layout")

@section ('site_content')

<script>

window.onload = function(){

    $('#next-1').click(function(){

        $("#register-1").hide();

        var form = $("#submit_paypal");
        var url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function(data)
            {
                //alert(data);
                $("#register-2").show();
                $('#next-1').hide();
                $('.already-member').hide();
                $('#submit_paypal').hide();
            },
            error: function (data) {
                //console.log('An error occurred.');
                //console.log(data.responseJSON.data);
                var resp = data.responseJSON.data;

                switch(true) {
                    case ("email" in resp) :
                        $("#reg-errors").text('The email has already been taken.');
                        break;
                    case ("first_name" in resp) :
                        $("#reg-errors").text('Your first name is missing.');
                        break;
                    case ("last_name" in resp) :
                        $("#reg-errors").text('Your last name is missing.');
                        break;
                    case ("password" in resp) :
                        $("#reg-errors").text('Password is not set.');
                        break;
                    default:
                        $("#reg-errors").text('Registration details error.');
                }

            },
        });

    });

    $('#cancel').click(function(){

        var form = $("#cancel_paypal");
        var url = form.attr('action');

        var r = confirm("Yes, I Want to Cancel my KnowSo account");
        if (r == true) {
            $.ajax({
                type: "POST",
                url: url,
                data: form.serialize(),
                success: function(data)
                {
                    $(".alert-success").text('Account cancelled! You will be logged out in a few seconds');
                    $(".alert-success").show();
                    setTimeout(function(){ document.getElementById('logout-form').submit(); }, 3000);
                },
                error: function (data) {
                    //console.log('An error occurred.');
                    //console.log(data.responseJSON.data);
                    var resp = data.responseJSON.data;

                    $(".alert-warning").text('Cancellation failed, try again');
                },
            });
        }

    });

    /**
     * Address Lookup
     */

    if ($('#post_code').val().length != 0){
        $("#address-section").show();
        //$("#search_address").attr("disabled", 'disabled');
    }

    $("#postcode_search").click(function() {
        $("#search_address").removeAttr('disabled');
    });

    $("#search_address").click(function() {

        /**
         * remove the nopc modal if available
         */
        if ($("#nopc_modal").is(":visible"))
        {

            $("#nopc_modal").css("display","none");

        }

        if($("#nopc_modal").css("display","block")){
            $("#nopc_modal").css("display","none");
        }

        $("#pc_modal").css("display","none");

        /**
         * set the button to a loader
         */
        $(this).addClass("is-loading");

        /**
         * setup the variables
         */
        var post_code = $("#postcode_search").val();
        var url = "/address_lookup/" + post_code;

        /**
         * check field is not empty
         */
        if (post_code == "")
        {

            $(this).removeClass("is-loading");
            $("#nopc_modal").css("display","block");
            $("#choose-address").css("display","none");

            location.href = '#nopc_modal';

            return false;

        }

        $.ajax({

            type: "GET",
            dataType: 'json',
            url: url,
            success: function(response) {

                $("#address_search_populate").html(response.delivery_point_select);
                $("#address_search_populate_container").slideDown();

                $("#search_address").removeClass("is-loading");

                /**
                 * show next fields
                 */
                $(".box-step2").removeClass("box-step2");

                $("#search_address").attr("disabled", 'disabled');

            },

            error: function(response) {

                $(".address-input-disabled").removeAttr('disabled');
                $("#search_address").removeClass("is-loading");

                /**
                 * show modal
                 */
                $("#pc_modal").css("display", "block");
                $("#choose-address").css("display","none");
                $("#address-section").show();

                /**
                 * show next fields
                 */
                $(".box-step2").removeClass("box-step2");

            }

        });

        /*setTimeout(function(){
            parent.postMessage(getPageHeight(),"*");
        }, 1000);*/

    });

    /**
     * Choose Address
     */
    $(document).on('change', '#choose-address', function() {

        // Remove Disabled Inputs


        $("#address-section").show();

        $(".address-input-disabled").removeAttr('disabled');

        // Populate Address Fields
        var street_name = $('option:selected', this).attr('data-street_name');
        var town = $('option:selected', this).attr('data-town');
        var county = $('option:selected', this).attr('data-county');
        var postcode = $('option:selected', this).attr('data-postcode');
        var company_name = $('option:selected', this).attr('data-company_name');
        var building_number = $('option:selected', this).attr('data-building_number');
        var sub_building_name = $('option:selected', this).attr('data-sub_building_name');
        var building_name = $('option:selected', this).attr('data-building_name');

        $("#street_name").val(street_name);
        $("#city").val(town);
        $("#county").val(county);
        $("#post_code").val(postcode);
        $("#company_name").val(company_name);
        $("#house_number").val(building_number);
        $("#flat_number").val(sub_building_name);
        $("#house_name").val(building_name);


    });
};


</script>

    <section class="register-info">
        <h5>My Profile</h5>
    </section>
    <section class="register-bar">

    </section>
    <section class="register-form mt-0 pt-0" style="">
    <div class="row">
    <div class="container-fluid col-md-1 m-0 pt-5 pr-0 pl-4 border-right" style="background-color:#d3f7e9;text-align: left;">
        <ul class="list-unstyled small">
            <li><a href="#" id="cancel">Cancel My Account</a></li>
            <li><a href="/logout" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Log out</a></li>
        </ul>
    </div>
    <div class="col-md-10 mr-1 pt-5">

        <div class="alert alert-success" style="display:none">

        </div>
        <div class="alert alert-warning" style="display:none">

        </div>
        <div id="alert-danger" class="alert alert-danger" style="display:none">

        </div>

        <form id="submit_paypal" method="post" target="_top">
            @csrf
            <input type="hidden" name="rm" value="2">
            <input type="hidden" name="cmd" value="_s-xclick">

            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            @if (session('warning'))
                <div class="alert alert-warning">
                    {{ session('warning') }}
                </div>
            @endif
            <div id="register-1" style="">
                <div class="row">
                    <div class="col-4">
                    </div>
                    <div id="reg-errors" class="col-4" role="alert">
                    </div>
                    <div class="col-4">
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-2"><label for="title">Title</label>
                        <select class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" required>
                            <option value="">Please select...</option>
                            <option value="miss"{!! $sel = (old('title') == "miss")? " selected": (($user->title == "miss")? " selected":"") !!}>Miss</option>
                            <option value="ms"{!! $sel = (old('title') == "ms")? " selected": (($user->title == "ms")? " selected":"") !!}>Ms</option>
                            <option value="mrs"{!! $sel = (old('title') == "mrs")? " selected": (($user->title == "mrs")? " selected":"") !!}>Mrs</option>
                            <option value="mr"{!! (old('title') == "mr")? " selected": (($user->title == "mr")? " selected":"") !!}>Mr</option>
                            <option value="dr"{!! $sel = (old('title') == "dr")? " selected": (($user->title == "dr")? " selected":"") !!}>Dr.</option>
                        </select>


                            <span class="invalid-feedback  err_title">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>


                    </div>
                    <div class="col-md-2"><label for="first_name">First Name</label>
                        <input id="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}"
                               name="first_name" value="{{ (!empty(old('first_name')))? old('first_name'): $user->first_name  }}" required >


                            <span class="invalid-feedback err_first_name">
                                <strong>{{ $errors->first('first_name') }}</strong>
                            </span>

                    </div>
                    <div class="col-md-2"><label for="last_name">Last Name</label>
                        <input id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ (!empty(old('last_name')))? old('last_name'): $user->last_name  }}" required>


                            <span class="invalid-feedback err_last_name">
                                            <strong>{{ $errors->first('last_name') }}</strong>
                                        </span>

                    </div>
                    <div class="col-md-2"><label for="mobile_number">Mobile Number</label>
                        <input class="form-control{{ $errors->has('mobile_number') ? ' is-invalid' : '' }}" name="mobile_number" value="{{ (!empty(old('mobile_number')))? old('mobile_number'): $user->mobile_number }}" required />

                            <span class="invalid-feedback err_mobile_number">
                                            <strong>{{ $errors->first('mobile_number') }}</strong>
                                        </span>

                    </div>
                    <div class="col-md-2"><label for="email">Email Address</label>
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ (!empty(old('email')))? old('email'): $user->email }}" required>

                            <span class="invalid-feedback err_email">
                                    <strong>{{ $errors->first('email') }}</strong>
                            </span>

                    </div>
                    <div class="col-md-4 col-lg-3 col-xl-2 dob"><label for="date_of_birth">Date of Birth</label><br>
                        <input id="date_of_birth" type="date_of_birth" class="form-control{{ $errors->has('date_of_birth') ? ' is-invalid' : '' }}" name="date_of_birth" value="{{ (!empty(old('date_of_birth')))? old('date_of_birth'): $user->date_of_birth }}" required>

                        <span class="invalid-feedback err_date_of_birth">
                            <strong>{{ $errors->first('date_of_birth') }}</strong>
                        </span>

                        {{--<select class="form-control date-day{{ $errors->has('date_of_birth') ? ' is-invalid' : '' }}{{ $errors->has('dob_day') ? ' is-invalid' : '' }}" name="dob_day" required>
                            <option value=""></option>
                            {!! $date_values['days'] !!}
                        </select>
                        <select class="form-control date-month{{ $errors->has('dob_month') ? ' is-invalid' : '' }}" name="dob_month" required>
                            <option value=""></option>
                            {!! $date_values['months'] !!}
                        </select>
                        <select class="form-control date-year{{ $errors->has('dob_year') ? ' is-invalid' : '' }}" name="dob_year" required>
                            <option value=""></option>
                            {!! $date_values['years'] !!}
                        </select>--}}
                    </div>
                </div>
                <div class="row justify-content-center">

                    <!-- post code search - START -->

                    <div class="col-md-6" style="padding: 15px 0;">
                        <span>Enter Post code</span>
                        <input class="input" type="text" placeholder="Postcode" id="postcode_search" value="">
                    </div>

                    <div class="col-md-6" style="">
                        <button class="btn btn-info " id="search_address">
                            Search Address by Postcode
                        </button>
                    </div>

                    <!-- lookup failure -->
                    <div class="col-md-12">
                        <article class="text-danger" id="pc_modal" style="display:none;">
                            <div class="message-body">
                                Unfortunately we have failed to find any addresses with the postcode you have entered. Please enter it manually below or try again.
                            </div>
                        </article>

                        <!-- no postcode entered -->
                        <article class="text-danger" id="nopc_modal" style="display:none;">

                            <div class="message-body">
                                Sorry, we can't search for your address if you don't enter a postcode. Please enter your home postcode and hit search again.
                            </div>
                        </article>
                    </div>

                    <div class="col-md-12" id="address_search_populate_container" style="display:none;">
                        <label class="label">Please Select Your Address</label>
                            <span class="" id="address_search_populate">
                            </span>
                    </div>

                    <!-- post code search - END -->
                </div>
                <div id="address-section" class="row justify-content-center" style="display:none">
                    <div class="col-md-2"><label for="house_number">House Number</label>
                        <input class="form-control{{ $errors->has('house_number') ? ' is-invalid' : '' }}"  id="house_number" name="house_number" value="{{ ((old()))? old('house_number'): $user->house_number }}" >

                        <span class="invalid-feedback err_house_number">
                            <strong>{{ $errors->first('house_number') }}</strong>
                        </span>
                    </div>
                    <div class="col-md-2"><label for="house_name">House Name</label>
                        <input class="form-control{{ $errors->has('house_name') ? ' is-invalid' : '' }}" id="house_name"  name="house_name" value="{{ ((old()))? old('house_name'): $user->house_name }}" >

                        <span class="invalid-feedback err_house_name">
                            <strong>{{ $errors->first('house_name') }}</strong>
                        </span>
                    </div>
                    <div class="col-md-2"><label for="flat_number">Flat Number</label>
                        <input class="form-control{{ $errors->has('flat_number') ? ' is-invalid' : '' }}" id="flat_number" name="flat_number" value="{{ (old())? old('flat_number'): $user->flat_number }}" >

                        <span class="invalid-feedback err_flat_number">
                            <strong>{{ $errors->first('flat_number') }}</strong>
                        </span>
                    </div>
                    <div class="col-md-2"><label for="street_name">Street Name</label>
                        <input class="form-control{{ $errors->has('street_name') ? ' is-invalid' : '' }}" id="street_name" name="street_name" value="{{ (old())? old('street_name'): $user->street_name }}" required>

                        <span class="invalid-feedback err_street_name">
                            <strong>{{ $errors->first('street_name') }}</strong>
                        </span>
                    </div>
                    <div class="col-md-2"><label for="city">City</label>
                        <input class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" id="city" name="city" value="{{ (!empty(old('city')))? old('city'): $user->city }}" required>

                        <span class="invalid-feedback err_city">
                            <strong>{{ $errors->first('city') }}</strong>
                        </span>
                    </div>
                    <div class="col-md-2"><label for="county">County</label>
                        <input class="form-control{{ $errors->has('county') ? ' is-invalid' : '' }}" id="county" name="county" value="{{ (!empty(old('county')))? old('county'): $user->county }}" required>

                        <span class="invalid-feedback err_county">
                            <strong>{{ $errors->first('county') }}</strong>
                        </span>
                    </div>
                    <div class="col-md-2"><label for="post_code">Postcode</label>
                        <input class="form-control{{ $errors->has('post_code') ? ' is-invalid' : '' }}" maxlength="7" id="post_code" name="post_code" value="{{ (!empty(old('post_code')))? old('post_code'): $user->post_code }}" required>

                        <span class="invalid-feedback err_post_code">
                            <strong>{{ $errors->first('post_code') }}</strong>
                        </span>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-2"><label for="residential_status">Residential Status</label>
                        <select class="form-control{{ $errors->has('residential_status') ? ' is-invalid' : '' }}" size="4" name="residential_status" required>
                            <option value="home_owner"{{ $sel = (old('residential_status') == "home_owner")? " selected": (($user->residential_status == "home_owner")? " selected":"") }}>Homeowner</option>
                            <option value="private_tenant"{{ $sel = (old('residential_status') == "private_tenant")? " selected": (($user->residential_status == "private_tenant")? " selected":"") }}>Private Tenant</option>
                            <option value="council_tenant"{{ $sel = (old('residential_status') == "council_tenant")? " selected": (($user->residential_status == "council_tenant")? " selected":"") }}>Council Tenant</option>
                            <option value="living_with_parents"{{ $sel = (old('residential_status') == "living_with_parents")? " selected": (($user->residential_status == "living_with_parents")? " selected":"") }}>Living with Parents</option>
                            <option value="living_with_friends"{{ $sel = (old('residential_status') == "living_with_friends")? " selected": (($user->residential_status == "living_with_friends")? " selected":"") }}>Living with Friends</option>
                            <option value="student_accommodation"{{ $sel = (old('residential_status') == "student_accommodation")? " selected": (($user->residential_status == "student_accommodation")? " selected":"") }}>Student Accommodation</option>
                            <option value="other"{{ $sel = (old('residential_status') == "other")? " selected": (($user->residential_status == "other")? " selected":"") }}>Other</option>
                        </select>

                        <span class="invalid-feedback err_residential_status">
                            <strong>{{ $errors->first('residential_status') }}</strong>
                        </span>

                    </div>
                    <div class="col-md-2"><label for="education">Education</label>
                        <select class="form-control{{ $errors->has('education') ? ' is-invalid' : '' }}" size="4" name="education" required>
                            <option value="o-levels"{{ $sel = (old('education') == "o-levels")? " selected": "" }}>O-Levels</option>
                            <option value="gcse"{{ $sel = (old('education') == "gcse")? " selected": "" }}>GCSE</option>
                            <option value="a-levels"{{ $sel = (old('education') == "a-levels")? " selected": "" }}>A-Levels</option>
                            <option value="degree"{{ $sel = (old('education') == "degree")? " selected": "" }}>Degree</option>
                            <option value="vocational"{{ $sel = (old('education') == "vocational")? " selected": "" }}>Vocational</option>
                            <option value="masters"{{ $sel = (old('education') == "masters")? " selected": "" }}>Masters</option>
                            <option value="doctorate"{{ $sel = (old('education') == "doctorate")? " selected": "" }}>Doctorate</option>
                            <option value="other"{{ $sel = (old('education') == "other")? " selected": "" }}>Other</option>

                        </select>

                        <span class="invalid-feedback err_education">
                            <strong>{{ $errors->first('education') }}</strong>
                        </span>
                    </div>
                    <div class="col-md-2"><label for="employment_status">Employment Status</label>
                        <select class="form-control{{ $errors->has('employment_status') ? ' is-invalid' : '' }}" size="4" name="employment_status" required>

                            <option value="full_time"{{ $sel = (old('employment_status') == "full_time")? " selected": "" }}>Full Time</option>
                            <option value="part_time"{{ $sel = (old('employment_status') == "part_time")? " selected": "" }}>Part Time</option>
                            <option value="temporary"{{ $sel = (old('employment_status') == "temporary")? " selected": "" }}>Temporary</option>
                            <option value="self_employed"{{ $sel = (old('employment_status') == "self_employed")? " selected": "" }}>Self Employed</option>
                            <option value="unemployed"{{ $sel = (old('employment_status') == "unemployed")? " selected": "" }}>Unemployed</option>
                            <option value="pension"{{ $sel = (old('employment_status') == "pension")? " selected": "" }}>Pension</option>
                            <option value="disability"{{ $sel = (old('employment_status') == "disability")? " selected": "" }}>Disability</option>
                            <option value="benefits"{{ $sel = (old('employment_status') == "benefits")? " selected": "" }}>Benefits</option>
                            <option value="student"{{ $sel = (old('employment_status') == "student")? " selected": "" }}>Student</option>
                        </select>

                        <span class="invalid-feedback err_employment_status">
                            <strong>{{ $errors->first('employment_status') }}</strong>
                        </span>
                    </div>
                    <div class="col-md-2"><label for="marital_status">Marital Status</label>
                        <select class="form-control{{ $errors->has('marital_status') ? ' is-invalid' : '' }}" size="4" name="marital_status" required>

                            <option value="single"{{ $sel = (old('marital_status') == "single")? " selected": "" }}>Single</option>
                            <option value="married"{{ $sel = (old('marital_status') == "married")? " selected": "" }}>Married</option>
                            <option value="living_together"{{ $sel = (old('marital_status') == "living_together")? " selected": "" }}>Living Together</option>
                            <option value="separated"{{ $sel = (old('marital_status') == "separated")? " selected": "" }}>Separated</option>
                            <option value="divorced"{{ $sel = (old('marital_status') == "divorced")? " selected": "" }}>Divorced</option>
                            <option value="widowed"{{ $sel = (old('marital_status') == "widowed")? " selected": "" }}>Widowed</option>
                            <option value="other"{{ $sel = (old('marital_status') == "other")? " selected": "" }}>Other</option>
                        </select>

                        <span class="invalid-feedback err_marital_status">
                            <strong>{{ $errors->first('marital_status') }}</strong>
                        </span>
                    </div>

                    <div class="col-md-3"><label for="social_media_check_reason">Why are you checking your Social Media Actiivty?</label>
                        <select class="form-control{{ $errors->has('social_media_check_reason') ? ' is-invalid' : '' }}" size="4" name="social_media_check_reason" required>

                            <option value="help_get_a_job"{{ $sel = (old('social_media_check_reason') == "help_get_a_job")? " selected": "" }}>To help me get a job</option>
                            <option value="be_better_at_social_media"{{ $sel = (old('social_media_check_reason') == "be_better_at_social_media")? " selected": "" }}>Be better at social media</option>
                            <option value="manage_social_accounts_centrally"{{ $sel = (old('social_media_check_reason') == "manage_social_accounts_centrally")? " selected": "" }}>Manage social media in one place</option>
                            <option value="improve_credit_score"{{ $sel = (old('social_media_check_reason') == "improve_credit_score")? " selected": "" }}>Improve my credit score</option>
                        </select>

                        <span class="invalid-feedback err_social_media_check_reason">
                            <strong>{{ $errors->first('social_media_check_reason') }}</strong>
                        </span>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-4"><label for="password">Password</label>
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                        <span class="invalid-feedback err_password">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    </div>
                    <div class="col-md-4"><label for="password_confirmation">Confirm Password</label>
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                    </div>
                    {{--<div class="col-12">
                        <button type="button" id="next-1" class="btn btn-info">2. Payment information</button>
                    </div>--}}
                </div>
                <div class="col-12">
                    <button type="button" id="next-1" class="btn btn-info" disabled>Save</button>
                </div>
            </div>

        </form>

        <form id="cancel_paypal" action="/profile/cancel_confirm" method="post" target="_top" style="display: none;">
            @csrf
            <input id="email" type="email" class="form-control" name="email" value="{{ $user->email }}">
        </form>

    </div>
    </div>
    </section>
@endsection
