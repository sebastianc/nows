@extends("templates.layout")

@section ('site_content')
    <section class="pages-info">

        <div class="container">
            <div class="row">
                <div class="col-12">
            <h1>Privacy Policy</h1>
            <br>
            <p>This Privacy Policy (the "Policy") has been prepared by Mediablanket Ltd to meet the requirements of the Data Protection Act 1998, (the "Act") and it only relates to the collection, protection, disclosure and use of personal data belonging to living individuals. It does not relate to data which emanates from companies or other entities, although the general principles regarding our collection and use of data is broadly the same irrespective of whoever is the user of our website, or our customers.</p>
            <p>MediaBlanket Ltd, 3 Parsonage, M3 2HW, Manchester, England, UK. 0161 711 0415. www.mediablanket.co.uk for further details.</p>
            <p>This Policy (together with our Terms of Use and Cookie Policy) and any other documents referred to in it) sets out the basis on which any personal data we collect from you, or that you provide to us, will be processed by us. Please read the following carefully to understand our views and practices regarding your personal data.</p>
            <p>We trade in the UK to provide consumers with a look into what their social media online image looks to others.</p>

            <!-- ol>ol>li*7^ol>li*5 -->

            <!-- ol>li*9>ol>li*7 -->
            <ol class="kn-terms">
                <li><h4>INFORMATION WE MAY COLLECT FROM YOU</h4>
                    <ol>
                        <li>We may collect and process the following data about you:
                            <ul>
                                <li>Information that you may provide by filling in online forms and registration forms on www.knowso.co.uk (our "Site"), we may also ask you for information in the event you report a problem with our Site;</li>
                                <li>Please note that we have the ability to retain any data that you provide on this website, even if you do not complete your registration / transaction by clicking submit / next or by giving any other indication of transmissions. Such contact details and data may be used to contact you to inquire why you did not complete your registration/transaction or market other products or services to you as described in this Privacy Policy;</li>
                                <li>Correspondence, or a record of it should you contact us;</li>
                                <li>Surveys that we use for research purposes, which we may ask you to complete, although you do not have to respond to them;</li>
                                <li>Details of your visits to our Site (including, but not limited to, traffic data, location data, weblogs and other communication data, whether this is required for our own billing purposes or otherwise) and the resources that you access;</li>
                                <li>Information to help verify your identity;</li>
                                <li>Occasionally third parties with whom we work may also ask you to provide documentation for the purpose of verifying your identity, such as utility bills or bank statements (and you should look at our third party partners' Privacy Policy regarding this).</li>
                                <li>where you post material, comments and/or feedback on our Site this information will be collected.</li>
                            </ul>
                        </li>
                    </ol>
                </li>
                <li><h4>IP ADDRESSES AND COOKIES</h4>
                    <ol>Please also refer to the Cookie Policy.
                        <li>A "cookie" is a small electronic file that collects information when someone visits a website. A cookie can identify the pages that are being viewed, and this can assist us to select the pages that the viewer sees. Some cookies only exist whilst viewers are online, but "persistent" cookies - which are not session-based - remain on the viewer's computer, so that he or she can be recognised as a previous visitor when he or she next visits our Site. This allows us to collect information about a viewer's browsing habits whilst on our Site, and this can be useful in assisting us to monitor and improve our services.</li>
                        <li>We do not store sensitive information such as account numbers or passwords in "persistent" cookies, and cookies in themselves, do not contain enough information to identify you. You will only acquire a personal identity in relation to your browsing habits after you have formally provided us with your personal data for the purposes outlined below.</li>
                        <li>In addition to using cookies, we might also use web tools to collect information about your browsing activities whilst on our Site. In this respect the information that is provided is similar to the information supplied by cookies, and we use it for the same purposes.</li>
                        <li>Any information that we acquire about you using cookies or web tools is subject to the same restrictions and conditions as any other information we collect about you.</li>
                        <li>Some of our advertisers may also use cookies or web tools that are set by other people such as advertising agencies, or the businesses to which the advertisements in question relate. We do not have access to any information that might be collected in this way, and, if you are concerned, you should contact the advertiser for more information.</li>
                        <li>If you want to delete any cookies already stored on your computer or stop the cookies that keep track of your browsing patterns on the Site you can do so by deleting your existing cookies and/or altering your browser's privacy settings to block third party cookies (the process you follow will differ from browser to browser). If you would like more information on how to do this please visit http://www.allaboutcookies.org/. Please note that deleting our cookies or disabling our future cookies means that you may not be able to access certain areas or features of the Site.</li>

                    </ol>
                </li>
                <li><h4>WHERE WE STORE YOUR PERSONAL DATA</h4>
                    <ol>
                        <li>All information you provide to us is, as far as reasonably practicable, stored on our secure servers. Any financial information, such as credit/ debit card numbers will be encrypted using SSL technology. We ask you not to permit anyone to use your name. RSDT employs industry standard security measures to ensure the security of all PCCI compliant data. Any data that is stored on RSDT's servers is treated as proprietary and confidential and is not available to the public. RSDT has an internal security policy with respect of the confidentiality of customer data allowing access only to those employees or third parties who have a need to know such information for the purpose of effectively delivering RSDT services by means of user login and password requirements. RSDT routinely evaluates its data security practices to identify security threats or opportunities for improvement.</li>
                        <li>Unfortunately, the transmission of information via the internet is not completely secure. Although we will do our best to protect your personal data, we cannot guarantee the security of your data as it is transmitted from you to our Site; any such transmission is at your own risk. Please see our Terms of Use concerning our liabilities in this regard.</li>
                        <li>We will retain your information securely stored for a reasonable period or for as long as the law requires.</li>

                    </ol>
                </li>
                <li><h4>HOW WE USE YOUR INFORMATION</h4>
                    <ol>
                        <li>We use information held about you in the following ways:
                            <ul>
                                <li>To process and submit your request to third party partners;</li>
                                <li>To service your membership;</li>
                                <li>To provide you with the best possible service;</li>
                                <li>To ensure that content from our Site is presented in the most effective manner for you and for your computer;</li>
                                <li>To notify you about changes to our service.</li>
                                <li>To display material provide by you by way of comments/feedback and/or any other information you may provide by way of a visitor's communications facility and/or forum.</li>
                            </ul>
                        </li>
                        <li>We use your data to provide you with information about goods and services which may be of interest to you. We may contact you by e-mail, text or post with information about services similar to those which were the subject of a previous enquiry made by you.</li>
                        <li>If you wish to opt-out of receipt of these marketing communications from us please send us an email or contact us at MediaBlanket Ltd. 3 Parsonage. M32HW. 0161 711 0415</li>
                        <li>If you opt-out of our use of your data for marketing purposes, we will honour such choice once we have had a reasonable opportunity to process your request. We reserve the right to take reasonable steps to authenticate your identity with respect to any such request or other enquiry.</li>
                        <li>Where we have received the appropriate consent we may also share you data with respected third party partners who may contact you from time to time with offers concerning unrelated products and services.</li>

                    </ol>
                </li>
                <li><h4>DATA TRANSFER</h4>
                    <ol>
                        <li>Personal data held shall only be transferred to a country outside the EEA if there is adequate protection in that country.</li>

                    </ol>
                </li>
                <li><h4>DISCLOSURE OF YOUR INFORMATION</h4>
                    <ol>We may disclose your personal information to third parties:
                        <li>To prepare or send any communications to you, or to assist us in connection with any of our administrative or business functions, or in the provision of any of our services to you;</li>
                        <li>If Mediablanket or substantially all of its assets, are acquired by a third party, in which case personal data held by us about our customers will be one of the transferred assets;</li>
                        <li>If we are under a duty to disclose or share your personal data in order to comply with any legal obligation, or in order to enforce or apply our Terms of Use and other agreements; or to protect the rights, property, or safety of MediaBlanket, our customers, or others. This includes exchanging information with other companies and organisations for the purposes of fraud protection and credit risk reduction.</li>

                    </ol>
                </li>
                <li><h4>YOUR RIGHTS</h4>
                    <ol>
                        <li>Our Site may, from time to time, contain links to and from the websites of our advertisers and affiliates. If you follow a link to any of these websites, please note that these websites have their own Privacy Policies and that we do not accept any responsibility or liability for these policies. Please check these policies before you submit any personal data to these websites.</li>
                        <li>Section 7 of the Act gives you the right to access information held about you. Your right of access can be exercised in accordance with the Act by writing to us at MediaBlanket Ltd. 3 Parsonage. M32HW. 0161 711 0415. Any access request may be subject to a small statutory fee to meet our costs in providing you with details of the information we hold about you.</li>
                        <li>Calls between us may be recorded and/or monitored.</li>
                        <li>If you want to change, modify or update your member information you can do so by submitting a form on the "Contact Us" page of the website. Alternatively you can call or write to us on the contact details listed above.</li>
                        <li>If you were introduced to us by a third party, we will give them your contact details and sufficient information about you for their accounting and administration purposes.</li>
                        <li>We may give information about you and how you manage your account to the following:
                            <ul>
                                <li>People who provide a service to us or are acting as our agents on the understanding that they will keep the information confidential. If they are located in another country we will make sure that they agree to apply the same levels of protection as we are required to apply to information held in the UK and to use your information only for the purpose of providing the service to us;</li>
                                <li>Anyone to whom we transfer or may transfer our rights and duties under this agreement; and</li>
                                <li>We may also give out information about you if we have a duty to do so or if the law allows us to do so.</li>
                            </ul>
                        </li>
                        <li>Otherwise we will keep information about you confidential.</li>
                    </ol>
                </li>
                <li><h4>CHANGES TO OUR PRIVACY POLICY</h4>
                    <ol>
                        <li>We reserve the right to change this Policy at any time and any changes we may make to our Privacy Policy in the future will be posted on this page.</li>

                    </ol>
                </li>
                <li><h4>CONTACT</h4>
                    <ol>
                        <li>Questions, comments and requests regarding this Policy are welcomed at hello@knowso.co.uk or MediaBlanket Ltd. 3 Parsonage. M32HW. 0161 711 0415</li>

                    </ol>
                </li>

            </ol>
                <p>Last updated in July 2017.</p>
            </div>
        </div>



    </div>
    </section>
@endsection

