@extends("templates.layout")

@section ('site_content')
    <section class="pages-info">

        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1>Terms & Conditions</h1>
                    <br>
                    <h4>PAYMENT SERVICE TERMS AND CONDITIONS INTRODUCTION</h4>
                    <p>Please read these terms carefully before using the online payment facility. By using the online payment facility on this website you accept these terms. If you do not accept these terms do not use this facility. If we make significant changes to these Terms and Conditions, we will inform you by updating this document.</p>
                    <h4>DEFINITIONS AND INTERPRETATION</h4>
                    <p>In these Conditions, the following terms shall (unless the context otherwise requires) have the following meanings:</p>
                    <p>"Card supplier" means the institution that issued the debit or credit card you are using on the Online Payment Facility.</p>
                    <p>"Recurring card payment" means payment collections initiated by Us to collect amounts due.</p>
                    <p>"You" means the individual setting up a payment via the online payment facility</p>
                    <p>"Transaction" means in the case of the Services: any transaction similar to any of the foregoing in connection with any payment for goods and/or services, (and "Transactions" shall be construed accordingly);</p>
                    <p>"Authorisation" means the process whereby We obtain at the time of the Transaction (directly or indirectly) from the relevant Acquirer, Card Issuer or Other Financial Institution confirmation that the applicable Card has not been listed as lost or stolen and that there are sufficient funds for the relevant Transaction.</p>
                    <p>"Cardholder" means a Person who or which is the authorised user of a Card (and "Cardholders" shall be construed accordingly);</p>
                    <p>"Card Issuer" means a financial institution which issues Cards under the authority of the relevant Card Scheme (and "Card Issuers" shall be construed accordingly);</p>
                    <p>"Card Schemes" means Visa Europe, Visa Inc, MasterCard Worldwide, UK Maestro, Solo and/or International Maestro and/or such other schemes governing the issue and use of Cards, as approved and notified by Us to You in writing from time to time (and "Card Scheme" shall be construed accordingly);</p>
                    <p>"Conditions" means these terms and conditions;</p>
                    <p>"Confidential Information" means information that is designated as "confidential" or which by its nature is clearly confidential including any information concerning Our technology, technical processes, procedures, business affairs, finance, security procedures and may take the form of (but is not limited to): (a) documents, technical specifications, unpublished patent specifications, data, drawings, plans, processes, photographs, databases, computer software in disk, cassette, tape or electronic form and items of computer hardware; or (b) oral descriptions, demonstrations or observations;</p>
                    <p>"Contract" means the contract between You and Us</p>
                    <p>"Data" means documents, data and records of any kind relating to Transactions</p>
                    <p>"Intellectual Property Rights" means any and all intellectual property rights of whatever nature and includes patents, inventions, know-how, trade secrets and other confidential information, registered designs, copyrights, database rights, design rights, rights affording equivalent protection to copyright, trade marks, service marks, logos, domain names, business names, trade names, moral rights, and all registrations or applications to register any of the aforesaid items, rights in the nature of any of the aforesaid items in any country or jurisdiction, rights in the nature of unfair competition rights and rights to sue for passing off;</p>
                    <p>"Other Financial Institution" means any third party which regulates or is responsible for any Other Payment Method (and "Other Financial Institutions" shall be construed accordingly);</p>
                    <p>"Payment System" means all equipment and software used by You in connection with Transactions and the storage and/or processing of Data;</p>
                    <h4>THE SERVICE</h4>
                    <p>We operate as a Payment Service Provider who collects, processes and reports payments on behalf of third parties such as product or service providers, “Product or Service Suppliers”. We process payments on behalf of these third party companies that sell products and services, usually via the internet or such other means as they may determine from time to time. We do this via an Online Payment Facility.</p>
                    <p>Typically, when you buy something from a Product or Service Supplier via their website, We will process the payment between you and them.</p>
                    <p>You authorise us to debit your account, in agreement with the terms and conditions of the Product or Service Suppliers, in order to purchase a product or service.</p>
                    <h4>CONDITIONS OF THE SERVICE</h4>
                    <p>All payments are subject to the following conditions:</p>
                    <p>You warrant that in using the Online Payment Facility you are authorised to use the debit or credit card for the payment or payments you are making.</p>
                    <p>We cannot accept liability for a payment not reaching the Product or Service Supplier where payment is refused or declined by the Card Supplier for any reason.</p>
                    <p>We will not have any liability whatsoever arising out of the use, inability to use, or the results of use of this service, any websites linked to this service or the materials or information contained therein, any provision or the lack thereof of Product or Service by the Product or Service Supplier whether based on warranty, contract, tort or any other legal theory and whether or not advised of the possibility of such liability or damages.</p>
                    <h4>PRICING</h4>
                    <p>The prices for Services will be indicated on the Websites you are purchasing from, including all taxes, including VAT, which may be payable in respect of the Services. All payments taken by us will be in pounds Sterling (GBP).</p>
                    <p>Certain payments for Services must be made in advance by credit or debit card using the payment facilities on the Websites. This may include a small debit from your credit or debit card for identity verification purposes. The amount debited for identity verification will be displayed on the website you are using and may be in addition to the cost of the Product or Service.</p>
                    <h4>SUBSCRIPTION PAYMENTS</h4>
                    <p>If you are purchasing a subscription, your subscription service payments must be made by credit or debit card using the payment facilities on the Website.</p>
                    <p>By giving us your payment details you agree that we have continuing authority to take subscription payments from your account by debiting your payment card until you or the Product or Service Supplier you are dealing with ends the Services.</p>
                    <p>The Product or Service Supplier may stop providing the Products or Services without notice to you if at any time we are unable to obtain payment using the details you provided.</p>
                    <h4>PROCESSING YOUR PAYMENTS</h4>
                    <p>To facilitate us processing your payments and repayments, you authorise us to retain your charge, credit and debit card details.</p>
                    <p>It is your responsibility to ensure that all payment details you provide are correct and complete.</p>
                    <p>No payment will be deemed to have been made until we have cleared funds and Product or Service Supplier you are dealing with may choose not to provide Products or Services until they have received payment in full.</p>
                    <p>We are not responsible for any overdraft or “over the limit” charges or bank fees if your payment card account or facility contains insufficient credit or funds when we take payment for the Products or Services.</p>
                    <p>In the event that you proceed to purchase a product or service from a Product or Service Supplier, the charges and payment terms will be subject to that Product or Service Supplier’s relevant terms and conditions.</p>
                    <h4>TRIALS</h4>
                    <p>Some of the Product or Service Suppliers we process payments for may offer you a trial of their Products or Services. Trials are made available at their sole discretion and may not be available for all Services.</p>
                    <p>The length of any Trial period will be confirmed to you when you register for the Trial.</p>
                    <p>If you do not cancel at the end of your Trial period and you receive a subscription service, then your access to the Services will automatically continue on a subscription basis and you will be charged the applicable subscription charges for the Service until cancelled by you.</p>
                    <h4>SECURITY</h4>
                    <p>All payment details which are entered through this payment service are encrypted when you enter them. Communications to and from the Product or Service Supplier’s site are also encrypted.</p>
                    <p>We shall not be liable for any failure by you to properly protect data from being seen on your screen by other persons or otherwise obtained by such persons, during the online payment process or in respect of any omission to provide accurate information in the course of the online payment process.</p>
                    <h4>VIRUS PROTECTION</h4>
                    <p>We make all reasonable attempts to check and test our services at all stages of production, and to exclude viruses. It is always wise for you to run an up to date anti-virus program on all material downloaded from the Internet. We cannot accept any responsibility for any loss, disruption or damage to your data or your computer system, which may occur whilst using our service.</p>
                    <h4>INTELLECTUAL PROPERTY</h4>
                    <p>The Contract does not transfer, and is not intended to transfer, to you any of the Intellectual Property Rights that we own at the commencement of the Contract or any Intellectual Property Rights that we create, acquire or develop.</p>
                    <h4>LIMITATION OF LIABLITY</h4>
                    <p>You agree that neither Us or the Product or Service Supplier nor any of their affiliates, account providers or any of their affiliates will be liable for (a) any damages for loss of profits, goodwill, use, data or other intangible losses, or (b) for any indirect, incidental, special, consequential loss, even if in each case Us or the Product or Service Supplier has been advised of the possibility of such damages or losses, resulting from: (i) the use or the inability to use the service; (ii) the cost of getting substitute goods and services, (iii) any products, data, information or services purchased or obtained or messages received or transactions entered into, through or from the service; (iv) unauthorized access to or alteration of your transmissions or data; (v) statements or conduct of anyone on the service; (vi) the use, inability to use, unauthorized use, performance or non- performance of any third party account provider site, even if the provider has been advised previously of the possibility of such damages; or (vii) any other matter relating to the service.</p>
                    <h4>INDEMNIFICATION</h4>
                    <p>You agree to protect and fully compensate Us and the Product or Service Supplier and their affiliates from any and all third party claims, liability, damages, expenses and costs (including, but not limited to, reasonable legal fees) caused by or arising from any misuse by you of the Service, your violation of these Terms and Conditions or your infringement of any intellectual property or other right of anyone.</p>
                    <h4>WAIVER</h4>
                    <p>No failure or delay by Us in exercising any of Our rights under these Conditions shall be construed as a waiver or release of that right unless otherwise agreed in writing by Us.</p>
                    <p>No single or partial exercise of any of Our rights or remedies under these Conditions shall preclude or restrict the further exercise of such right or remedy. A waiver of any breach of any provisions of the Contract shall not constitute a waiver of any other breach and shall not affect the other provisions of the Contract.</p>
                    <p>Our rights and remedies under these Conditions are cumulative and not exclusive of any rights or remedies provided by law.</p>
                    <h4>ENTIRE AGREEMENT</h4>
                    <p>The contract constitutes the entire agreement and understanding between Us and You in respect of the matters dealt with in it and supersedes and invalidates all other prior representations, arrangements, understandings and agreements relating to the subject matter of the Contract which may have been made between You and Us either orally or in writing prior to the date of the Contract, other than any securities or written pledges, undertakings or assurances which You may previously have given to Us and, subject to and together with such securities etc sets out the entire agreement and understanding You and We have.</p>
                    <p>Each Party warrants that it has not relied on any representations, arrangements, understanding or agreements (whether written or oral) not expressly set out or referred to in the Contract. The only remedy available to either Party in respect of any such representations, arrangement, understanding or agreement shall be for the breach of contract under the provisions of these Conditions.</p>
                    <h4>SEVERABILITY</h4>
                    <p>If any provision of these Conditions is found by any court or administrative body of competent jurisdiction to be illegal, invalid or unenforceable:</p>
                    <p>Such illegality, invalidity or unenforceability shall not affect the other provisions of these Conditions, which shall remain in full force and effect and if such provision would cease to be illegal, invalid or unenforceable if some part of the provision were modified or deleted, the provision in question shall apply with such minimum modification or deletion as may be necessary to make it legal, valid and enforceable.</p>
                    <h4>MISCELLANEOUS</h4>
                    <p>Nothing in these Conditions is intended to create a partnership or joint venture or legal relationship of any kind that would impose liability upon one Party for the act or failure to act of the other Party, or to authorise either Party to act as agent for the other. Save where expressly stated in the Contract, neither Party shall have authority to make representations, act in the name or on behalf of or otherwise to bind the other.</p>
                    <p>A Person who is not party to the Contract shall have no rights under the Contracts (Rights of Third Parties) Act 1999 to enforce any provisions of the Contract. This clause does not affect any right or remedy of any Person which exists or is available otherwise than pursuant to that Act.</p>
                    <h4>GOVERNING LAW AND JURISDICTION</h4>
                    <p>The Contract and any matter arising from or in connection with it shall be governed by and construed in accordance with English law.</p>
                    <p>You and We irrevocably agree that, for Our benefit only, the English courts shall have exclusive jurisdiction over any claim or matter arising from or in connection with the Contract, or the legal relationships established by or in connection with it. Accordingly, any proceedings by or against Us in respect of such claim or matter must be brought in the English Courts, but We shall not be prevented from taking proceedings against You either in the English courts or in any other court of competent jurisdiction. To the extent permitted by law, We may take concurrent proceedings in any number of jurisdictions.</p>
                    <p>In the event of any conflict between the English version of the Contract (or any part thereof) and any version that has been translated into any language other than English, then the English version shall prevail.</p>
                </div>
            </div>
        </div>

    </section>
@endsection

