@extends("templates.layout")

@section ('site_content')
    <section class="pages-info">

        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1>Ways Social Media Might Impact Your Ability To Get A Loan</h1>
                    <br>
                    <p>Many say Social Media has the ability to identify potential new borrowers who qualify for credit but haven’t had easy access to it.</p>
                    <p>Credit scores are simple to understand because they’re fairly cut and dry. Where your score falls within a pre-defined range triggers approval or denial on your loan, and sometimes the interest rate you receive. It can be used on its own, but is typically combined with other indicators of creditworthiness, such as debt-to-income ratio, recent payment history, and job stability.</p>
                    <p>If the lender can’t make a clear decision based on available data, they might look at more unexpected sources of information.  Enter social media.</p>
                    <p>3 WAYS SOCIAL MEDIA MIGHT IMPACT YOUR ABILITY TO GET A LOAN:</p>
                    <ol>
                        <li>LENDERS MIGHT ASSUME “BIRDS OF A FEATHER FLOCK TOGETHER.”</li>
                        <li>YOUR LINKEDIN PROFILE REFLECTS JOB STABILITY (OR LACK THEREOF) AND HOW SERIOUSLY YOU TAKE YOUR CAREER PATH.</li>
                        <li>A BUSINESS’ SOCIAL MEDIA PRESENCE CAN INDICATE POTENTIAL SUCCESS/REVENUE.</li>
                    </ol>
                    <p>Online lenders, and even a few banks, are starting to look at social media profiles when making a loan decision, which impacts both consumer AND business lending. What they see on your profile or page can influence their final decision.</p>
                    <p>According to Takepart, “there are no rules about how lending companies are allowed to use harvested social media information to make their decisions. If you are about to apply for a significant loan, such as a mortgage or small business loan, it might be a smart move to clean up your social media presence beforehand.</p>
                    <p>Here are a few things to consider.</p>
                    <h5>1. LENDERS MIGHT ASSUME “BIRDS OF A FEATHER FLOCK TOGETHER.”</h5>
                    <p>Last year, Facebook acquired a patent that allows lenders to assess creditworthiness based on the credit ratings of those in the borrower’s social network. If your close friends have poor credit, they posit, so might you.</p>
                    <p>The patent would give lenders access to your Facebook friend’s credit scores. If the average credit rating is at least a minimum credit score, your loan is more likely to continue through the application process.</p>
                    <p>Use of the patent went nowhere, but its very existence has interesting implications for the lending process. Clearly, social media is being considered as a screening element for the lending process when other data is unavailable or lacking in detail. </p>
                    <p>It’s also possible that some lenders might assume friend behaviours mirror your own, since people typically surround themselves with like-minded people. If you like or share posts from friends related to gambling, frequent financial troubles or spending rent money on designer purses, for example, it might cause concern about your own reliability and spending habits.</p>
                    <p>On a more positive note, many say Social Media has the ability to identify potential new borrowers who qualify for credit but haven’t had easy access to it. </p>
                    <p>No matter how its data might be used, social media profiles are public and subject to scrutiny from any source, from casual friends to potential employers and lenders. Regardless of how tempting it is to share something, it’s always good to consider public perception and your personal brand before posting.</p>
                    <p>Glancing over your Social Media activity from the perspective of a lender can be a valuable exercise, and inspire removal of an occasional questionable post that might impact perception negatively.</p>
                    <h5>2. YOUR LINKEDIN PROFILE REFLECTS JOB STABILITY (OR LACK THEREOF) AND HOW SERIOUSLY YOU TAKE YOUR CAREER PATH.</h5>
                    <p>When we look at someone else’s LinkedIn profile, it instantly gives us an impression of job stability (or instability). This is true for lenders, too, who can use LinkedIn to gain a clearer understanding of job history, stability and income potential.</p>
                    <p>If the borrower is being considered for a business loan, does the LinkedIn profiles of the management team or business owner(s) support expertise noted in the business plan? Do they have experience in the specific industry related to the new business? Do they demonstrate community connections and influential relationships that might help their business grow? Does their LinkedIn profile show commitment?</p>
                    <p>LinkedIn might not be reviewed for the average consumer loan, but anyone seeking a business loan might consider adding as much detail as possible to their profile, and even asking past colleagues for LinkedIn recommendations. It can’t hurt, right?</p>
                    <p>If obtaining the loan is important to you, it’s probably worth investing time improving factors that can impact its approval.</p>
                    <h5>3. A BUSINESS’ SOCIAL MEDIA PRESENCE CAN INDICATE POTENTIAL SUCCESS/REVENUE.</h5>
                    <p>Personal profiles on social media aren’t the only things to consider if seeking any kind of business loan; activity on behalf of the brand is also important. After all, marketing plans are a key factor in evaluating a loan application, and online strategies are a critical part of today’s marketing mix.</p>
                    <p>The importance of social media can’t be denied.</p>
                    <p>Any business that is more than a year old and seeking funding should consider that the lender might evaluate its online marketing activities as a predictor of future success. A business that is doing well on social media, successfully using it to grow reach and build audience, can be perceived as having a strong marketing foundation.</p>
                    <p>It signifies a business that has been able to leverage its available funds for growth and has its marketing “ducks in a row”–building lender confidence in its continued success once the business has been funded.</p>
                    <p>It can also demonstrate higher than typical levels of customer service and engagement, which impact customer retention and revenue.</p>
                    <p>On the flip side, lack of a strong social media presence might not hurt your ability to get the loan, but having one certainly strengthens likelihood of loan approval. So by all means, beef it up.</p>
                </div>
            </div>
        </div>

    </section>
@endsection

