<?php
/**
 * Created by PhpStorm.
 * User: colino
 * Date: 28/03/18
 * Time: 16:01
 */


?>
@extends("templates.layout")

@section ('site_content')
    <section class="social-page">
 <div class="container" style="margin:30px 0;">
     <p>Request from controller:</p><pre>{!! print_r($result, true) !!}</pre>
     <div class="row">
         <div class="col-12" style="text-align:center">
             <h3>The payment has been cancelled!</h3>
             <h4>Please return to <a href="/">knowso</a> or attempt to <a href="/register">register</a> again.</h4>
         </div>
     </div>
 </div>
    </section>

    @endsection
