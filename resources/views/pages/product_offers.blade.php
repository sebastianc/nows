<?php
/**
 * Created by PhpStorm.
 * User: colino
 * Date: 06/04/18
 * Time: 12:46
 */

?>
@extends("templates.layout")

@section ('site_content')
    <style>
        /* Style the tab */
        .tab {
            overflow: hidden;
            border: 1px solid #ccc;
            background-color: #f1f1f1;
        }

        /* Style the buttons that are used to open the tab content */
        .tab button {
            background-color: inherit;
            float: left;
            border: none;
            outline: none;
            cursor: pointer;
            padding: 14px 16px;
            transition: 0.3s;
        }

        /* Change background color of buttons on hover */
        .tab button:hover {
            background-color: #ddd;
        }

        /* Create an active/current tablink class */
        .tab button.active {
            background-color: #ccc;
        }

        /* Style the tab content */
        .tabcontent {
            display: none;
            padding: 6px 12px;
            border: 1px solid #ccc;
            border-top: none;
        }
    </style>
    <script>
        function openOffer(offerName) {
            var i;
            var x = document.getElementsByClassName("offer");
            for (i = 0; i < x.length; i++) {
                x[i].style.display = "none";
            }
            document.getElementById(offerName).style.display = "block";
        }

    </script>
    <section class="social-page">
<div class="ck-user-welcome" style="">
                <div class="item-list-header"><h1 style="text-align: center">Hello {{ Auth::user()->first_name }}. Premium Offers Available!</h1></div>

</div>
<div class="row" style="margin:0">
    <div class="col-md-12">
        <div class="container" style="padding-top:10px; text-align:center"><h3>{!! $intro_text !!}</h3></div>
    </div>
</div>
@php


    $row_count = 0; // add a row if modulus '3' = 0
    $end_div = "</div > <!-- row prod-list-row - END -->"
@endphp


<div class="tab">
    <button class="" onclick="openOffer('All')">All</button>
    <button class="" onclick="openOffer('Eating')">Eating</button>
    <button class="" onclick="openOffer('Shopping')">Shopping</button>
    <button class="" onclick="openOffer('Holidays')">Holidays</button>
    <button class="" onclick="openOffer('Health')">Health</button>
</div>
<div id="All" class="offer">
    @php $product_array = $offers_all; @endphp
    @include('includes.offer_rows')
</div>

<div id="Eating" class="offer" style="display:none">
    @php $product_array = $offers_eating; @endphp
    @include('includes.offer_rows')
</div>

<div id="Shopping" class="offer" style="display:none">
    @php $product_array = $offers_shopping; @endphp
    @include('includes.offer_rows')
</div>

<div id="Holidays" class="offer" style="display:none">
    @php $product_array = $offers_holidays; @endphp
    @include('includes.offer_rows')
</div>

<div id="Health" class="offer" style="display:none">
    @php $product_array = $offers_health; @endphp
    @include('includes.offer_rows')
</div>

</section>
@endsection