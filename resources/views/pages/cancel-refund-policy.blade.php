@extends("templates.layout")

@section ('site_content')
    <section class="pages-info">

        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1>Cancel My Account</h1>
                    <br>
                    <p>If you want to cancel your trial period or membership to Knowso.co.uk then you can do so at any time. Simply follow the three steps here.</p>
                    <p>If you have any other questions about cancelling your account or need any help you can contact us on 0161 711 0415.</p>
                    <h5>Step One - Sign In</h5>
                    <p>Sign in to your Knowso account via the Sign In button at the top of this page. Enter your email and password. If you have forgotten your password select the ‘Forgotten Password’ link and we’ll send you a reminder email</p>
                    <h5>Step Two - Open Profile Page</h5>
                    <p>Navigate to the My Profile page by clicking on your name. There you will see a button to ‘Cancel My Account’. This is located in the ‘Cancel My Account’ panel.</p>
                    <h5>Step Three - Cancel Account</h5>
                    <p>When you select the ‘Cancel My Account’ button, you will be presented with the following pop up. Click, ‘Yes, I Want to Cancel’ to confirm your account cancellation. After that, you will no longer be charged, nor have access to your account.</p>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1>Refund Policy</h1>
                    <br>
                    <p>A member may request a refund, cancel or terminate this agreement at any time by calling us at 0161 711 0420 or by notifying us in writing at: Terminations, Mediablanket Ltd. 1 Spinningfields. Manchester. M3 3JE. England.</p>
                    <p>However refunds will be given only in the cases of fraud, and/or in the event of our partial or total non-performance or inadequate performance of failure to deliver the agreed upon services to the member (i.e site failure or service failure). If the member uses the services (i.e. signs up using a credit and or debit card and validates themselves) then services have been determined and agreed to have been rendered and therefore except in the circumstances referenced above, you forfeit the ability to demand a full refund for services rendered. For avoidance of doubt as soon as a sign up occurs for the membership.</p>
                </div>
            </div>
        </div>

    </section>
@endsection

