<style type="text/css">
    #mainNav {
        display:none;
    }

    footer {
        display:none!important;
    }

    button.btn-user-home {
        display:none!important;
    }

    #cookie-message {
        display:none;
    }
</style>
<?php

$analysis_view = true; // for now, use this to display extra 'js' file in layout footer if we are on this view...
?>
@extends("templates.layout")

@section ('site_content')
<section class="user-analysis-section" style="padding: 0">
 <div class="container user-analysis">
     @if(isset($prev_results))

         <div class="row previous-results">
             <div class="col-12">
                 <div class="user-analysis-jumbotron standard-color" style="">
                     <h3>Hello again, {!! Auth::User()->first_name !!}</h3>
                     <p>You have the opportunity to view your previous identity scores here or take the test again</p>

                 </div>

             </div>

             <div class="col-12">
                 <a class="btn btn-view-prev" href="/user_analysis/results">View your last results</a>
             </div>
             <div class="col-12">
                 <a class="btn btn-success" href="/user_analysis/bypass">Run analysis again</a>
             </div>
         </div>




     @endif  {{-- END - if(isset($prev_results) --}}
     @if(isset($analysis))
         <div class="user-analysis-jumbotron standard-color" style="width:100%;">
             <h3>Hello {!! Auth::User()->first_name !!}!</h3><h4>Here you can analyse your personal identity traits</h4>
             {{--<p>Click or touch the button beside each option if you agree with the assessment.</p>--}}
             <p>For each phrase listed below, click the button that in your opinion currently describes you.</p>

         </div>
         @php
         $analysis_count = count((array)$analysis); // count the analysis array
         $section_count = 1; // start the count of sections to show.
         @endphp
         <form id="analysis_form" action="" method="post">
             @csrf

         @foreach($analysis as $section_title => $section)

     <div id="section-start-{{ $section_count }}" class=""></div>
     <div id="section-{{ $section_title }}" class="section">
         <div class="section-heading">
             <h4>{!! ucwords(str_replace("_", " ", $section_title)) !!} of {!! $analysis_count !!} <i class="fa fa-list-alt"></i></h4>
         </div>

         <div id="section-body-{{ $section_count }}" class="section-body" style="display:none">
         @foreach($section as $btn_value => $btn_description)

             <div class="input-group-text input-group-analysis" style="">
                 @if($btn_description == 'Detail oriented')
                     Detail orientated&#160;
                 @else
                     {!! $btn_description !!}&#160;
                 @endif
                 <div id="{{ $btn_value }}_btn" class="btn btn-warning btn-analysis-input" style="">
                     No
                 </div>
                 <input id="{{ $btn_value }}" type="checkbox" class="checkbox-hidden" name="{{ $btn_value }}" value="1"
                        aria-label="Checkbox for following text input" style="">

             </div>
         @endforeach
             @if($section_count > 1)
                 <div id="prev-{{ $section_count }}" class="btn btn-prev" style=""><i class="fa fa-chevron-circle-left"></i> Review</div>
             @endif
             @if($section_count < $analysis_count)

             <div id="next-{{ $section_count }}" class="btn btn-next" style="">Next <i class="fa fa-chevron-circle-right"></i></div>
             @endif
             @php
                 $section_count++;
             @endphp
         </div>
     </div>

        @endforeach
             {{--<input class="form-control" value="Hello World from test_input!" name="test_input">--}}

             <p id="submit-form-button" style="display:none"><button class="btn btn-analysis-submit" type="submit"><i class="fa fa-refresh"></i> See Results!</button></p>

         </form>
         @if(isset($bypass) && $bypass === "bypass")
         <a href="/user_analysis">
             <button class="btn btn-info btn-user-home">Back to Your Identity - home</button>
         </a>
         @endif
         @if(filter_input(INPUT_SERVER, 'HTTP_HOST') == "knowso.local")
         <div>Result:<pre><?= print_r($analysis, true) ?></pre> </div>
         @endif

     @endif {{-- END - if(isset($analysis) --}}
     @if(isset($show_results))
         {{--<h4>Your Results</h4>--}}
         @if(isset($personality_analysis_data))
         <div id="user-analysis-graph" class="user-analysis-jumbotron standard-color" style="width:100%; display:none">
             <h3>THANK YOU {!! strtoupper(Auth::User()->first_name )!!}</h3>
             <p>Your identity results are summarised in the chart below</p>
             <p>Click on each chart bar to see more information on your unique traits.</p>
             <div id="results-barchart"><span style="border-radius:4px;display:inline-block;padding:10px;background:#fff;color:#2d2d2d">Loading!</span></div>

         </div>
             {{-- http://fourtemperaments.com/4-primary-temperaments/ --}}
          <div id="sanguine_more" class="results-more" style="display:none">
              <div class="card card-analysis">
                  <div class="card-title">
                      Sanguine <span class="analysis-score animated bounce" style="">Score: </span>
                  </div>
                  <div class="card-body">
                      <p class="score-info-analysis animated slideInLeft"><span id="sanguine_score_info" style="font-style: italic"></span></p><hr>
                      <p>The Sanguine temperament has three combinations: Sanguine-Choleric, Sanguine-Phlegmatic, and Sanguine-Melancholy.</p>

                      <p>The traits of the primary temperament, Sanguine, may be altered or modified in some significant way due to the influence of the secondary temperament. Remember, there are at least three levels of intensity of a temperament: classic, moderate, and mild. Some Sanguines will be very strong, others somewhat strong, and still others more mild. Some are “Super Sanguines” because they are so talkative and active that they can be overwhelming.</p>

                      <p>Sanguines are naturally people-orientated. They have an active, positive movement in a favorable environment. They influence their environment by encouraging others to work together.</p>

                      <p>The Sanguine is by far the most versatile of the four temperaments. The Choleric, Phlegmatic, and Melancholy are all limited in what they can do, the Sanguine is not. The Choleric temperament is the fewest in number, and the Phlegmatic and Melancholy temperaments are not change agents, preferring to adapt to their environment.</p>

                      <p>The Sanguine has the potential for the widest range of behavior due to possessing the widest range of emotions. This allows them to participate (based on their second temperament) in any kind of human activity. They like to participate in, or change, their environment. The areas of business, politics, sports, and entertainment, to name a few, are dominated by the Sanguine temperament.</p>

                      <p>The Sanguine is extroverted, fun-loving, playful, activity-prone, impulsive, entertaining, persuasive, easily amused, and optimistic. They are enthusiastic, expressive, and tend to be very affectionate. Sanguines are personable, receptive, open to others, and build relationships quickly. They are animated, excitable, approachable, accepting, and trusting of others. They will smile, and talk easily and often. Sanguines are word smiths.</p>

                      <p>It is not unusual to feel as if you have known one who is Sanguine for years after the first meeting. They make and keep friends easily. They get so involved in conversations that they easily forget about time, and are often late arriving at their destination. Sanguines are easily bored if not involved in social activity. Sanguines dislike solitude. Their attention span is based on whether or not they are interested in the person or event. They can change their focus or interest in an instant if they become bored.</p>

                      <p>When telling a story, Sanguines often exaggerate what happened or leave out important details. They make the story exciting, and they may not let the facts get in the way!</p>

                      <p>Sanguines are very competitive. They usually like sports of any kind because of their natural desire to be active and involved with people.  They tend to be disorganized and easily forget where they left something. They sometimes have difficulty controlling their thoughts and emotions. Actually, they tend not to store their thoughts and feelings—if they think it or feel it, they share it!</p>

                      <p>Their voice will show excitement and friendliness. Sanguines enjoy dressing according to current fashion. They fear rejection or not making a favorable impression. Sanguines also fear others viewing them as unsuccessful. Sanguines are very effective working with others. Sanguines are easily distracted and can change quickly.</p>

                  </div>
                  <a href="#" target="_top">Back</a>
              </div>
          </div>
                 <div id="melancholic_more" class="results-more" style="display:none">
                     <div class="card card-analysis">
                         <div class="card-title">
                             Melancholic <span class="analysis-score animated bounceInDown" style="">Score: </span>
                         </div>
                         <div class="card-body">
                             <p class="score-info-analysis animated slideInLeft"><span id="melancholic_score_info" style="font-style: italic"></span></p><hr>
                             <p>The Melancholy temperament has three combinations: Melancholy-Choleric, Melancholy-Sanguine, and Melancholy-Phlegmatic.

                             <p>The traits of the primary temperament, Melancholy, may be altered or modified in some significant way due to the influence of the secondary temperament. Remember, there are at least three levels of intensity of a temperament: classic, moderate, and mild. Some Melancholies will be very strong, others somewhat strong, and still others more mild.</p>

                             <p>The Melancholy naturally wants to do things right, and is quality-orientated. Melancholies are not trying to be right, they are driven to figure out what is right. They have a cautious, tentative response designed to reduce tension in an unfavorable environment. The Melancholy’s second response is often to become aggressive to restore peace in an unfavorable situation. They influence their environment by adhering to the existing rules, and by doing things right according to predetermined (and accepted) standards.</p>

                             <p>Melancholies are detailed-orientated, operate from a plan, and they are very private. Melancholies are introverted, logical, analytical, and factual in communication. They need information, time alone to think, and a detailed plan in order to function effectively without anxiety.</p>

                             <p>Melancholies respond to others in a slow, cautious, and indirect manner. They are reserved and suspicious until they are sure of your intentions. Melancholies probe for the hidden meaning behind your words. They are timid, may appear unsure, and have a serious expression. Melancholies are self-sacrificing, gifted, and they can be a perfectionist.</p>

                             <p>Melancholies are conscientious, picky, and can be sensitive to what others think of their work. They have anxiety about the present and future. They tend to have guilt feelings but fail to realize that guilt will not change the past nor will worry change the future. They allow guilt and worry to rob them of enjoying the present.</p>

                             <p>Melancholies are well organized. However, on occasion, they may keep things cluttered, but they will know what is in the piles. They are determined to make the right and best decision so they will collect lots of information, and ask very specific questions, and sometimes they will ask the same question several times. They may take excessive time to think about their options before making a decision. Even then, they may not be sure it is the right, and best decision.</p>

                             <p>Melancholies need reassurance, feedback, and reasons why they should do something. They can be moody, which is usually related to their negative evaluation of people or events.</p>

                             <p>Melancholies fear taking risks, making wrong decisions, and being viewed as incompetent. They tend to have a negative attitude toward something new until they have had time to think it through. Melancholies are skeptical about almost everything, but they are creative and capable people. They tend to get bored with something once they get it figured out.</p>

                         </div>
                         <a href="#" target="_top">Back</a>
                     </div>
                 </div>
                 <div id="phlegmatic_more" class="results-more" style="display:none">
                     <div class="card card-analysis">
                         <div class="card-title">
                             Phlegmatic <span class="analysis-score animated bounceInUp" style="">Score: </span>
                         </div>
                         <div class="card-body">
                             <p class="score-info-analysis animated slideInLeft"><span id="phlegmatic_score_info" style="font-style: italic"></span></p><hr>
                             <p>The Phlegmatic temperament has three combinations: Phlegmatic-Choleric, Phlegmatic-Sanguine, and Phlegmatic-Melancholy.</p>

                             <p>The traits of the primary temperament, Phlegmatic, may be altered or modified in some significant way determined by the influence of the secondary temperament. Remember, there are at least three levels of intensity of a temperament: classic, moderate, and mild. Some Phlegmatics will be very strong, others somewhat strong, and still others more mild.</p>

                             <p>Phlegmatics are naturally service-orientated. They are passive in both favorable and unfavorable environments. They influence their environment by cooperating with others to carry out the task. They tend to not be highly ambitious and they tend to lack a sense of urgency (both due to their passive nature). Phlegmatics are introverted, calm, unemotional, easygoing, indecisive, patient, and agreeable. They are both slow and indirect when responding to others. Phlegmatics are slow to warm-up, but will be accommodating in the process. They are by far the easiest people with whom to get along—as long as you do not try to alter their routine or ask them to change.</p>

                             <p>Phlegmatics live a quiet, routine life free of the normal anxieties of the other temperaments. They avoid getting too involved with people, and life in general, preferring a private, low-key life-style, centered around home and family. A mother who has the Phlegmatic temperament will often refer to her children as, “My children,” leaving a bewildered look on her husband’s face.</p>

                             <p>Phlegmatics seldom exert themselves with others or push their way along in their career. They just let it happen. They make good team players. They communicate a warm, sincere interest in others, preferring to have just a few close friends. They are possessive of their friendships and material things. Phlegmatics will be very loyal to their friends. They find it difficult to break long-standing relationships regardless of what the other person does or doesn’t do. However, once a relationship is broken, they seldom return.</p>

                             <p>Phlegmatics strongly resist change. They need time to adjust when change does occur, especially sudden change. They avoid conflict (which is why they are so accommodating). They resist making quick decisions. Phlegmatics are practical, concrete, and traditional thinkers. Their stoic expression often hides their true feelings. They can be grudge holders. Phlegmatics can also be patient to the point of paralysis. They are persistent, and consistent, at whatever they undertake. Because of their passive nature, they tend to procrastinate easily and often.</p>

                         </div>

                         <a href="#" target="_top">Back</a>
                     </div>
                 </div>
                 <div id="choleric_more" class="results-more" style="display:none">
                     <div class="card card-analysis">
                         <div class="card-title">
                            Choleric <span class="analysis-score animated bounceIn" style="">Score: </span>
                         </div>
                         <div class="card-body">
                             <p class="score-info-analysis animated slideInLeft"><span id="choleric_score_info" style="font-style: italic"></span></p><hr>

                             <p>The Choleric temperament has three combinations: Choleric-Sanguine, Choleric-Phlegmatic, and Choleric-Melancholy. The Choleric is the least occurring of the four temperaments, and a female Choleric is extremely rare.</p>

                             <p>The traits of the primary temperament, Choleric, may be altered or modified in some significant way because of the influence of the secondary temperament. Remember, there are at least three levels of intensity of a temperament: classic, moderate, and mild. Some Cholerics will be very strong, others somewhat strong, and still others more mild.</p>

                             <p>Cholerics are naturally result-orientated. They have active, positive, and forward movement, in an antagonistic environment. They influence their environment by overcoming opposition to get results.</p>

                             <p>Cholerics are extroverted, quick-thinking, active, practical, strong-willed, and easily annoyed. They are self-confident, self-sufficient, and very independent minded. They are brief, direct, to the point, and firm when communicating with others. Cholerics like pressure and are easily bored when things are not happening fast enough. They are bold and like to take risks.</p>

                             <p>Cholerics are domineering, decisive, opinionated, and they find it easy to make decisions for themselves as well as for others. They wake up wanting to control, change or overcome something…anything! They leave little room for negotiating—it’s usually their way or no way.</p>

                             <p>Cholerics are visionaries and seem to never run out of ideas, plans, and goals, which are all usually practical. They do not require as much sleep as the other temperaments, so their activity seems endless. Their activity, however, always has a purpose because of their goal-orientated nature.</p>

                             <p>Cholerics usually do not give in to the pressure of what others think unless they see that they cannot get their desired results. They can be crusaders against social injustice, and they love to fight for a cause. They are slow to build relationships, and tend to have only a few close friends, because results are more important than people. Cholerics do not easily empathize with the feelings of others or show compassion. They think big and seek positions of authority.</p>

                             <p>Cholerics tend to not be angry, although their assertive push to get results may be interpreted as anger. They are quickly aroused, but quickly calmed..</p>

                         </div>
                         <a href="#" target="_top">Back</a>
                     </div>
                 </div>


         @else
             <p style="color:red; font-weight:700">Could not retrieve data...</p>
        @endif
         <a href="/user_analysis">
             <button class="btn btn-info btn-user-home" style="">Back to Your Identity - Home</button>
         </a>
     @endif  {{-- END - if(isset($prev_results) --}}
     @if(isset($show_results) && filter_input(INPUT_SERVER, 'HTTP_HOST') == "knowso.local")
             <pre>{!! print_r($personality_analysis_data, true) !!}</pre>
     @endif
 </div>
</section>

    <!-- The Modal -->
    <div class="modal fade" id="saveResultsModal">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div id="saveResultsModal-header" class="modal-header">

                    <img src="/img/logos/logo_icon_128.png" style="max-height:48px">&#160;
                    <h4 class="modal-title">Modal Heading</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div id="saveResultsModal-body" class="modal-body">
                    Modal body..
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>

    <!-- The Modal -->
    <div class="modal" id="temperament_modal">
        <div class="modal-dialog">
            <div class="temp-modal-content modal-content">

                <!-- Modal Header -->
                <div class="modal-header" style="border-bottom: 1px solid #e6e6e6">
                    {{-- NOTE: 'modal-title' gets overwritten by Javascript/Jquery objects in 'knowso_analysis.js' file --}}

                        <h4 class="modal-title">Temperament</h4>
                    <button type="button" class="close" data-dismiss="modal" style="color:#e6e6e6">&times;</button>
                </div>

                <!-- Modal body -->
                {{-- NOTE: 'modal-body' gets overwritten by Javascript/Jquery objects in 'knowso_analysis.js' file --}}
                <div class="modal-body">
                    {{--<p class="modal-analysis-score" style="display:none">Score: </p>--}}
                    <p class="animated bounceIn modal-analysis-score" style="">Score: </p>
                    <p class="modal-para"></p>
                    <a id="modal-more" href='#sanguine_more'>More...</a>
                </div>


            </div>
        </div>
    </div>

@endsection
