<?php
/**
 * Created by PhpStorm.
 * User: colino
 * Date: 28/03/18
 * Time: 16:01
 */

?>
@extends("templates.layout")

@section ('site_content')
<section class="social-page">
<div class="container" style="margin:30px 0">
    <div class="row">
        <div class="col-12" style="text-align:center">
            <h3>The Registration has been confirmed.</h3>
            <p>Auth result: {{ $auth_result }}</p>
            <p>Post info: <pre>{!! print_r($request, true) !!}</pre></p>
            <h4>Please log in <a href="/">here</a></h4>
        </div>
    </div>
</div>
</section>

@endsection
