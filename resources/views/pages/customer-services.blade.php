@extends("templates.layout")

@section ('site_content')
    <section class="pages-info">

        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1>SPEAK TO US:</h1>
                    <br>
                    <h4>Address:</h4>
                    <p>1 Spinningfields. Manchester. M3 3JE</p>
                    <h4>Email Address:</h4>
                    <p>hello@knowso.co.uk</p>
                    <h4>Phone:</h4>
                    <p>0161 711 0420</p>
                    <h4>Opening Hours:</h4>
                    <p>Monday - Friday 9am - 4pm</p>
                    <p>As a credit broker, our services are subject to the jurisdiction of the Financial Ombudsman Service. If you have a complaint about our services, you should contact us and we will endeavour to resolve it as soon as possible. If we do not respond within 8 weeks or you disagree with our response, you have the right to refer the complaint to: The Financial Ombudsman Service 020 7964 1000</p>
                </div>
            </div>
        </div>

    </section>
@endsection

