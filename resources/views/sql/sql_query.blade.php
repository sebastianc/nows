<?php
/**
 * Created by PhpStorm.
 * User: colino
 * Date: 06/03/18
 * Time: 11:38
 */
?>
@extends("templates.layout")

@section ('site_content')
    <header style="height:114px; background:#212529">

    </header>
    <section>
 <div class="container">
     <div class="row">
         <div class="sql-form col-10">
             <h4>Run SQL Query</h4>
             @if(!isset($auth_error) )

             <form action="/query_run" method="post">
                 @csrf
                 @if (session('status'))
                     <div class="alert alert-success">
                         {!! session('status')  !!}
                     </div>
                 @endif
                 @if (session('warning'))
                     <div class="alert alert-warning">
                         {!! session('warning') !!}
                     </div>
                 @endif
                 @if (session('error') )
                     <div class="alert alert-danger">
                         {!! session('error') !!}
                     </div>
                 @endif


                 <div class="col-10">
                     <textarea name="query_box" rows="5" style="width:100%; margin:15px 0"></textarea>
                 </div>
                 <div class="col-10">
                     <button class= "btn btn-warning" id="" style="width:100%">Run query</button>
                 </div>
             </form>
                 <div class="">
                     <p>Examples</p>
                     <pre>select id, name, email, last_login from kn_users;</pre>
                     <pre>select id, name, email, created_at from kn_user_register;</pre>
                     <pre>show columns from kn_users;</pre>
                     <pre>show columns from kn_user_register;</pre>
                     <pre>show tables;</pre>
                 </div>
                 @else
                     @if (isset($error) && $error )
                         <div class="alert alert-danger">
                             {{ $error }}
                         </div>
                     @endif
                 @endif



         </div>

     </div>

 </div>
    </section>

@endsection
