@php
$previous_url = strtolower(str_replace(url('/'), '', url()->previous()));

$display_header = "Welcome, " . ucfirst(Auth::user()->first_name);
$display_text = "Loading your profile. Please wait ..";
if($previous_url == "/dashboard"){

    $display_header = "Thank you, " . ucfirst(Auth::user()->first_name);
    $display_text = "Refreshing your profile. Please wait!";
}
@endphp
@extends("templates.layout")

@section ('site_content')
    <script>
     window.onload = function(){

         $('.modal').modal('show');

         window.location.href = '{{url("/dashboard")}}';

     }

    </script>
    <section class="social-page">
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12 col-md-10 social-welcome">


        </div>


    </div>

</div>
    </section>

    <div class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document" style="text-align:center">
            <div class="modal-content">
                <div class="modal-header">
                    <div style="text-align:center; width:100%"><img src="/img/logos/logo_icon_128.png" style="height:48px"></div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h4 class="modal-title site-color-text-1">{{ $display_header }}</h4>
                    <p class="site-color-text-2">{{ $display_text }}</p>
                    <i class="fa fa-gear fa-spin site-color-text-1" style="font-size:32px"></i>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

@endsection
