<?php
/**
 * Created by PhpStorm.
 * User: colino
 * Date: 01/06/18
 * Time: 09:58
 */
{{--@extends('errors::layout')--}}

{{--@section('title', 'Page Expired')--}}

@extends("templates.layout")

@section ('site_content')


    Notice: This page has expired due to inactivity.
    <br/><br/>
    Please refresh and try again.
@endsection
