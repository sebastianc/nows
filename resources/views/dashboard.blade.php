@php
$kn_dashboard = true;
@endphp
@extends("templates.layout")

@section ('site_content')
    <script>
        window.onload = function(){
            new WOW().init();

           // $('[data-toggle="tooltip"]').tooltip();
        };

        $.ajax({

            url: "/get_personality_results",
            //
            // data: analysis_data,
            dataType:'json',
            success: function (id_data) {

                var total_score = (id_data.sanguine + id_data.melancholic + id_data.phlegmatic + id_data.choleric);

                var sanguine_percent = Number((id_data.sanguine / total_score * 100).toFixed(2));
                var melancholic_percent = Number((id_data.melancholic / total_score * 100).toFixed(2));
                var phlegmatic_percent = Number((id_data.phlegmatic / total_score * 100).toFixed(2));
                var choleric_percent = Number((id_data.choleric / total_score * 100).toFixed(2));

                /*$('#sanguine').text('Sanguine: '+sanguine_percent+'%');
                $('#melancholic').text('Melancholic: '+melancholic_percent+'%');
                $('#phlegmatic').text('Phlegmatic: '+phlegmatic_percent+'%');
                $('#choleric').text('Choleric: '+choleric_percent+'%');*/

                // Changed to make names more understandable

                $('#sanguine').text('People Person: '+sanguine_percent+'%');
                $('#melancholic').text('Analytical: '+melancholic_percent+'%');
                $('#phlegmatic').text('Calm/Peaceful: '+phlegmatic_percent+'%');
                $('#choleric').text('Leader/Competitive: '+choleric_percent+'%');

                $('#identity-results').show();

                var maximum = Math.max(sanguine_percent, melancholic_percent, phlegmatic_percent, choleric_percent);

                switch (maximum) {
                    case sanguine_percent:
                        $('#sanguine-jobs').show();
                        $('#sanguine-desc').show();
                        break;
                    case melancholic_percent:
                        $('#melancholic-jobs').show();
                        $('#melancholic-desc').show();
                        break;
                    case phlegmatic_percent:
                        $('#phlegmatic-jobs').show();
                        $('#phlegmatic-desc').show();
                        break;
                    case choleric_percent:
                        $('#choleric-jobs').show();
                        $('#choleric-desc').show();
                        break;
                    default:
                        $('#melancholic-jobs').show();
                }

            },
            error: function (data,status, response) {

                //console.log("Error - data:\n " + JSON.stringify(data));
                /*console.log("Get results Error - status:\n " + JSON.stringify(status));
                console.log("Get Results Error - response:\n " + JSON.stringify(response));

                return JSON.stringify(response);*/

            }
        });

    </script>
    <style type="text/css">
        body#page-top {
            {{--/*background: linear-gradient(transparent,rgba(96.1,89.8,67.5,.5)),url(../img/background/tree.svg) center no-repeat;*/
            1200 goes with 35% vertical alignment
            --}}
            background: #f5e5ac url(/img/background/tree.svg) center 28% no-repeat;
            background-size: 1100px auto;
        }
        footer {
            position: fixed;
            right: 0;
            bottom: 0;
            left: 0;
            z-index: 1030;
        }
    </style>
    <section class="social-page">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-sm-12 col-md-8 social-welcome mt-0 mt-sm-3">
            {{--@if(!empty($data['last_login'] ))
                <div class="last-login d-md-none animated bounce">
                    Last Log in<br>{!! $data['last_login'] !!}
                </div>
            @else
                <div class="last-login d-md-none">
                    First time<br>log in for<br>{!! Auth::user()->first_name !!}
                </div>
            @endif--}}
            <h5 class="site-color-text-1" style="text-shadow: 1px 0.5px #f5e5ac;">Welcome to your social report</h5>
            <h5 class="site-color-text-2 d-none d-sm-block" style="text-shadow: 1px 0.5px #f5e5ac;">Below you will find all your social networks connected in one place.</h5>

        </div>
        <div class="col-md-2">
    {{--@if(!empty($data['last_login'] ))
        <div class="last-login d-none d-md-block animated bounce">
            Last Log in<br>{!! $data['last_login'] !!}
        </div>
    @else
        <div class="last-login d-none d-md-block">
            First time<br>log in for<br>{!! Auth::user()->first_name !!}
        </div>
    @endif--}}
        </div>
    </div>
    @if(null !== session('connected'))
        @if(session('connected'))
            <div class="alert alert-success">
                {!! session('connected_msg') !!}
            </div>
        @else
            <div class="alert alert-warning">
                {!! session('connected_msg') !!}
            </div>
        @endif
    @endif
    @if(null !== session('disconnected'))
        @if(session('disconnected'))
            <div class="alert alert-success">
                {{ session('disconnected_msg') }}
            </div>
        @else
            <div class="alert alert-warning">
                {{ session('disconnected_msg') }}
            </div>
        @endif
    @endif


    @if(isset($disconnected))
        <pre>{!! print_r($disconnected, true) !!}</pre>
    @endif
    @if(isset($err))
        <!-- connection error -->
        <div class="alert alert-danger">
            {!! $err !!}
        </div>
    @endif
    <div class="social_connection_status">
        <p id="{{--fb_login_status--}}" class="d-none d-sm-block" style="visibility:hidden;">Attempting to determine facebook login status</p>
    </div>
    {{--<div class="mobile-jumps d-md-none">
        @if($data['tw_connected'])
         <a href="#twitter_section">Jump to Twitter</a><br>
        @else
            <a href="#twitter_section">Connect Twitter</a><br>
        @endif
        @if($data['li_connected'])
            <a href="#linkedin_section">Jump to LinkedIn</a><br>
        @else
            <a href="#linkedin_section">Connect LinkedIn</a><br>
        @endif
        @if($data['ins_connected'])
            <a href="#instagram_section">Jump to Instagram</a><br>
        @else
            <a href="#instagram_section">Connect Instagram</a><br>
        @endif
    </div>--}}

    <div class="row mt-0 pt-sm-0 mt-md-5 pt-md-5" style="z-index: 1000;margin-top: 7%;position: absolute;width: 100%;">
        <div class="col-md-2 col-lg-4">
        </div>
        <div class="col-md-8 col-lg-4">
            <div class="row">
                <div class="col-5 col-md-3">
                    <a class="mt-0 mt-md-5 ml-md-5 close-fb cloz branch social-icon-holder site-border-color-icon social-icon-fb animated pulse infinite" style="margin-top: -7%;" ><i class="fa fa-facebook fa-inverse"></i></a>
                </div>
                <div class="col-6 col-md-3">
                    <a class="mt-0 close-in cloz branch social-icon-holder site-border-color-icon social-icon-li animated pulse infinite" aria-label="close"><i class="fa fa-linkedin fa-inverse"></i></a>
                </div>
                <div class="col-5 mt-5 pt-3 col-md-3">
                    <a class="close-tw cloz branch social-icon-holder site-border-color-icon social-icon-tw animated pulse infinite" aria-label="close"><i class="fa fa-twitter fa-inverse"></i></a>
                </div>
                <div class="col-6 mt-2 pt-1 mt-md-5 col-md-3">
                    <a class="mt-0 close-id cloz branch social-icon-holder site-border-color-icon social-icon-tw animated pulse infinite" style="margin-top: 20%;" aria-label="close"><i class="fa fa-search fa-inverse"></i></a>
                </div>
            </div>
            <div class="row mt-3" style="margin-top: 10%;">
                <div class="mt-0 col-5 col-md-4">
                    <a class="mt-0 close-jobs cloz branch social-icon-holder site-border-color-icon social-icon-tw animated pulse infinite" aria-label="close"><i class="fa fa-address-book"></i></a>
                </div>
                <div class="col-1 col-md-4">
                </div>
                <div class="col-5 mt-md-5 col-md-4">
                    <a class="mt-0 mr-lg-5 close-mo cloz branch social-icon-holder site-border-color-icon social-icon-tw animated pulse infinite" style="margin-top: 7%;" aria-label="close"><i class="fa fa-credit-card"></i></a>
                </div>
            </div>
        </div>
        <div class="col-md-2 col-lg-4">
        </div>
    </div>

    <div class="row row2" style="z-index: 2000;position: absolute;width: 100%;display: none;">
        {{--<div class="col-md-4" id="bogus" style="height:1078px;visibility: hidden;">
        </div>--}}
        <div class="col-md-4">
        </div>
        <div class="col-md-4">
            <div class="social-section" id="fb-tab">

                <a class="close-fb cloz">&times;</a>

                <div class="social-icon-holder site-border-color-icon social-icon-fb animated pulse infinite">
                    <i class="fa fa-facebook fa-inverse"></i>
                </div>



            @if(!$data['fb_connected'])
            <div class="connect-button">
            <a href="/fb_connect"><button class="btn btn-success">Connect Facebook</button></a>
            </div>
            @else

                @php
                $updated = (isset($fb_profile['posts']['data']['0']))? $fb_profile['posts']['data']['0']['created_time']: "";
                $verified = (isset($fb_profile['verified']))? "Yes": "No";

                $posts = 0;
                if(isset($fb_profile['posts']['data'])){
                    $posts = count($fb_profile['posts']['data']);
                }
                //foreach($fb_profile['posts']['data'] as $count)
                @endphp
                <div class="avatar">
                <img data-toggle="tooltip" title="{{$fb_profile['name']}}" src="{{ $fb_profile['picture']['data']['url'] }}" />
                </div>
                    <div class="social-score-holder site-border-color-icon wow bounce" data-wow-iteration="infinite" data-wow-duration="2s" data-wow-delay="0.5s">
                        <p>Score<br>{{ $fb_profile->social_score  }}%</p>
                    </div>
                <div class="profile-data row">
                    <div class="col-4 ">
                        <div class="profile-data-inner site-background-2">Friends<br>
                            @if(isset($fb_profile['friends']['summary']['total_count']))
                            {{ $fb_profile['friends']['summary']['total_count']}}
                            @else
                            0
                            @endif
                        </div>
                    </div>
                    <div class="col-4"><div class="profile-data-inner site-background-2">Name<br>
                                {{ $fb_profile->user['name'] }}
                    </div></div>
                    <div class="col-4"><div class="profile-data-inner site-background-2">Verified<br>{{$verified}}</div></div>

                    <div class="col-4"><div class="profile-data-inner site-background-2">Updated<br>{{date("Y-m-d", strtotime($updated))}}</div></div>
                    <div class="col-4"><div class="profile-data-inner site-background-2">Posts<br>{{ $posts }}</div></div>
                    <div class="col-4"><div class="profile-data-inner site-background-2">Groups joined<br>?</div></div>
                    <div class="col-12"><div class="profile-data-col site-background-2">Most Vocabulary Used<br><span>{{$fb_profile->most_vocab }}</span></div></div>
                    <div class="col-12"><div class="profile-data-col site-background-2">Places or Trends<br><span>{{ $fb_profile->trends }}</span></div></div>
                    <div class="col-12"><div class="profile-data-col site-background-2">Your searches<br><span>{!! $fb_profile->your_searches !!}</span></div></div>
                    <div class="col-12"><div class="profile-data-col site-background-2">Knowso Recommendations<br><span title="{{ $fb_profile->knowso_recommendations_words }}">{!! $fb_profile->knowso_recommendations !!}</span></div></div>
                    <div class="col-12"><div class="profile-data-col-high site-background-2"><i class="fa fa-link"></i> Other Recommendations<br><span>{!! $fb_profile->other_recommendations !!}</span></div></div>
                </div>

            @endif

            </div><!-- end fb-section -->
        </div>
        <div class="col-md-4">
        </div>
        <div class="col-md-4">
        </div>
        <div class="col-md-4">
            <div id="linkedin_section" class="d-md-none" style="height:50px"></div>
            <div class="social-section" id="in-tab">

                <a class="close-in cloz" aria-label="close">&times;</a>

                <div class="social-icon-holder site-border-color-icon social-icon-li animated pulse infinite">
                    <i class="fa fa-linkedin fa-inverse"></i>
                </div>
                @if(!$data['li_connected'])
                    <div class="connect-button">
                        <a href="/li_connect"><button class="btn btn-success">Connect Linkedin</button></a>
                    </div>
                @else
                    @php
                        $avatar = str_ireplace("_normal", "",$li_profile->avatar);
                        $avatar = str_ireplace("http:", "",$avatar);
                        $job_title = (isset($li_profile->user['positions']['values']['0']['title'])?$li_profile->user['positions']['values']['0']['title']:"n/a")

                    @endphp
                    <div class="avatar">
                        <img data-toggle="tooltip" title="{{$li_profile->name}}" src="{{ $avatar }}" />
                    </div>
                    <div class="social-score-holder site-border-color-icon wow bounce" data-wow-iteration="infinite" data-wow-duration="2s" data-wow-delay="1.5s">
                        <p>Score<br>{!! $li_profile->score !!}</p>
                    </div>
                    <div class="profile-data row">
                        <div class="col-4"><div class="profile-data-inner site-background-2">Headline<br>{{ $li_profile->user['headline'] }}</div></div>
                        <div class="col-4"><div class="profile-data-inner site-background-2">Connections<br>{{ $li_profile->user['numConnections'] }}</div></div>
                        <div class="col-4"><div class="profile-data-inner site-background-2">Job Title<br>{{ $job_title }}</div></div>
                        <div class="col-4"><div class="profile-data-inner site-background-2">Recent Posts<br>1</div></div>
                        <div class="col-4"><div class="profile-data-inner site-background-2">Total Jobs in Cv:<br>{!! $li_profile->jobs_total !!}</div></div>
                        <div class="col-4"><div class="profile-data-inner site-background-2">Summary Score<br>{!! $li_profile->summary !!}</div></div>
                        <div class="col-12"><div class="profile-data-col site-background-2">Location<br><span>{!! $li_profile->location !!}</span></div></div>
                        <div class="col-12"><div class="profile-data-col site-background-2">People You May Know<br><span>{!! $li_profile->people_may_know !!}</span></div></div>
                        <div class="col-12"><div class="profile-data-col site-background-2">Your Searches<br><span>{!! $li_profile->your_searches !!}</span></div></div>
                        <div class="col-12"><div class="profile-data-col site-background-2">Knowso Recommendations<br><span>{!! $li_profile->knowso_recommendations !!}</span></div></div>
                        <div class="col-12"><div class="profile-data-col-high site-background-2"><i class="fa fa-link"></i> Other Recommendations<br><span>{!! $li_profile->other_recommendations !!}</span></div></div>

                    </div>

                @endif

            </div><!-- end li-section -->
        </div>
        <div class="col-md-4">
        </div>
        <div class="col-md-4">
        </div>
        <div class="col-md-4">
            <div id="twitter_section" class="d-md-none" style="height:50px"></div>
            <div class="social-section" id="tw-tab">

                <a class="close-tw cloz" aria-label="close">&times;</a>

                <div class="social-icon-holder site-border-color-icon social-icon-tw animated pulse infinite">
                    <i class="fa fa-twitter fa-inverse"></i>
                </div>
                @if(!$data['tw_connected'])
                    <div class="connect-button">
                        <a href="/tw_connect"><button class="btn btn-success">Connect Twitter</button></a>
                    </div>
                @else
                    @php
                        $avatar = str_ireplace("_normal", "",$tw_profile->avatar);
                        $avatar = str_ireplace("http:", "",$avatar);

                    @endphp
                    <div class="avatar">
                        <img data-toggle="tooltip" title="{{$tw_profile->name}}" src="{{ $avatar }}" />
                    </div>
                    <div class="social-score-holder site-border-color-icon wow bounce" data-wow-iteration="infinite" data-wow-duration="2s" data-wow-delay="1s">
                        <p>Score<br>{{ $tw_profile->social_score }}%</p>
                    </div>
                    <div class="profile-data row">
                        <div class="col-4"><div class="profile-data-inner site-background-2">Tweets<br>{{ $tw_profile->user['statuses_count'] }}</div></div>
                        <div class="col-4 "><div class="profile-data-inner site-background-2">Following<br>
                                {{ $tw_profile->user['friends_count']}}</div></div>
                        <div class="col-4"><div class="profile-data-inner site-background-2">Followers<br>{{$tw_profile->user['followers_count']}}</div></div>
                        <div class="col-4"><div class="profile-data-inner site-background-2">Gender<br>N/A</div></div>


                        <div class="col-4"><div class="profile-data-inner site-background-2">Favourites<br>{{$tw_profile->user['favourites_count']}}</div></div>

                        <div class="col-4"><div class="profile-data-inner site-background-2">Pictured{{-- Retweets --}}<br>?{{--$tw_profile->user['retweeted']--}}</div></div>
                        <div class="col-12"><div class="profile-data-col site-background-2">Hashtags Used<br><span>{!! $tw_profile->hashtag_words_list !!}</span></div></div>
                        <div class="col-12"><div class="profile-data-col site-background-2">Trends for you<br><span>{!! $tw_profile->trends !!}</span></div></div>
                        <div class="col-12"><div class="profile-data-col site-background-2">Your searches<br><span>{!! $tw_profile->your_searches !!}</span></div></div>
                        <div class="col-12"><div class="profile-data-col site-background-2">Knowso Recommendations<br><span title="{{ $tw_profile->knowso_recommendations_words }}">{!! $tw_profile->knowso_recommendations !!}</span></div></div>
                        <div class="col-12"><div class="profile-data-col-high site-background-2"><i class="fa fa-link"></i> Other Recommendations<br><span>{!! $tw_profile->other_recommendations !!}</span></div></div>
                    </div>

                @endif

            </div><!-- end tw-section -->
        </div>
        <div class="col-md-4">
        </div>
    </div>
    <div class="row row3" style="z-index: 2000;position: absolute;width: 100%;display: none;">
        <div class="col-md-4">
        </div>
        <div class="col-md-4">
            <div id="id_section" class="d-md-none" style="height:50px"></div>
            <div class="social-section" id="id-tab">

                <a class="close-id cloz" aria-label="close">&times;</a>

                <div class="social-icon-holder site-border-color-icon social-icon-tw animated pulse infinite">
                    <i class="fa fa-search fa-inverse"></i>
                </div>
                <div class="connect-button">

                    <div class="social-section social-sub" id="identity-results" style="display:none;">
                        <h4 class="site-color-text-2 text-center px-2 mb-2">Your identity results</h4>
                        <p id="sanguine"></p>
                        <p id="melancholic"></p>
                        <p id="phlegmatic"></p>
                        <p id="choleric"></p>
                        <p id="sanguine-desc" style="display: none;">
                            People Person personality type is described primarily as being highly talkative, enthusiastic, active, and social. Sanguines tend to be more extroverted and enjoy being part of a crowd; they find that being social, outgoing, and charismatic is easy to accomplish. Individuals with this personality have a hard time doing nothing and engage in more risk seeking behaviour.
                        </p>
                        <p id="melancholic-desc" style="display: none;">
                            Analytical individuals tend to be analytical and detail-orientated, and they are deep thinkers and feelers. They are introverted and try to avoid being singled out in a crowd. A melancholic personality leads to self-reliant individuals who are thoughtful, reserved, and often anxious. They often strive for perfection within themselves and their surroundings, which leads to tidy and detail orientated behavior.
                        </p>
                        <p id="phlegmatic-desc" style="display: none;">
                            Calm/Peaceful individuals tend to be relaxed, peaceful, quiet, and easy-going. They are sympathetic and care about others, yet they try to hide their emotions. Phlegmatic individuals also are good at generalizing ideas or problems to the world and making compromises.
                        </p>
                        <p id="choleric-desc" style="display: none;">
                            Leader/Competitive individuals tend to be more extroverted. They are described as independent, decisive, and goal-orientated, and ambitious. These combined with their dominant, result-orientated outlook make them natural leaders. In Greek, Medieval and Renaissance thought, they were also violent, vengeful, and short-tempered.
                        </p>
                    </div>

                    <h5 class="site-color-text-2 text-center px-2 mb-2 mt-3">
                    Please review and use Knowso recommendations on how you can improve your online image
                    </h5>

                    <h4><a href="/user_analysis"><button class="btn btn-success">Personal Identity Analyser</button></a></h4>
                    {{--@isset($save_result->personality_analysis_data_1)
                        <h4><a><button class="btn btn-success" id="scroll-result">Your Latest Result</button></a></h4>
                    @endisset--}}
                </div>
            </div>
        </div>
        <div class="col-md-4">
        </div>
        <div class="col-md-4">
        </div>
        <div class="col-md-4">
            <div id="jobs_section" class="d-md-none" style="height:50px"></div>
            <div class="social-section" id="jobs-tab">

                <a class="close-jobs cloz" aria-label="close">&times;</a>

                <div class="social-icon-holder site-border-color-icon social-icon-tw animated pulse infinite">
                    <i class="fa fa-address-book"></i>
                </div>
                <div class="connect-button">

                    <div class="social-section social-sub" id="sanguine-jobs" style="display:none;">
                        <h4 class="site-color-text-2 text-center px-2 mb-2">The best suited jobs for your personality:</h4>
                        @forelse ($allJobs['sanguine'] as $job)
                            {{--<div class="job">
                                <a target="_blank" href="https://www.cv-library.co.uk{{ $job['url'] }}"><h5>{{ $job['title'] }}</h5></a>
                                <p>{{ $job['description'] }}</p>
                            </div>--}}
                            <h4>
                                <a href="https://www.cv-library.co.uk{{ $job['url'] }}" target="_blank"><button class="btn btn-success" id="scroll-result">{{ $job['title'] }}</button></a>
                            </h4>
                        @empty
                        @endforelse
                    </div>

                    <div class="social-section social-sub" id="melancholic-jobs" style="display:none;">
                        <h4 class="site-color-text-2 text-center px-2 mb-2">The best suited jobs for your personality:</h4>
                        @forelse ($allJobs['melancholic'] as $job)
                            {{--<div class="job">
                                <a target="_blank" href="https://www.cv-library.co.uk{{ $job['url'] }}"><h5>{{ $job['title'] }}</h5></a>
                                <p>{{ $job['description'] }}</p>
                            </div>--}}
                            <h4>
                                <a href="https://www.cv-library.co.uk{{ $job['url'] }}" target="_blank"><button class="btn btn-success" id="scroll-result">{{ $job['title'] }}</button></a>
                            </h4>
                        @empty
                        @endforelse
                    </div>

                    <div class="social-section social-sub" id="phlegmatic-jobs" style="display:none;">
                        <h4 class="site-color-text-2 text-center px-2 mb-2">The best suited jobs for your personality:</h4>
                        @forelse ($allJobs['phlegmatic'] as $job)
                            {{--<div class="job">
                                <a target="_blank" href="https://www.cv-library.co.uk{{ $job['url'] }}"><h5>{{ $job['title'] }}</h5></a>
                                <p>{{ $job['description'] }}</p>
                            </div>--}}
                            <h4>
                                <a href="https://www.cv-library.co.uk{{ $job['url'] }}" target="_blank"><button class="btn btn-success" id="scroll-result">{{ $job['title'] }}</button></a>
                            </h4>
                        @empty
                        @endforelse
                    </div>

                    <div class="social-section social-sub" id="choleric-jobs" style="display:none;">
                        <h4 class="site-color-text-2 text-center px-2 mb-2">The best suited jobs for your personality:</h4>
                        @forelse ($allJobs['choleric'] as $job)
                            {{--<div class="job">
                                <a target="_blank" href="https://www.cv-library.co.uk{{ $job['url'] }}"><h5>{{ $job['title'] }}</h5></a>
                                <p>{{ $job['description'] }}</p>
                            </div>--}}
                            <h4>
                                <a href="https://www.cv-library.co.uk{{ $job['url'] }}" target="_blank"><button class="btn btn-success" id="scroll-result">{{ $job['title'] }}</button></a>
                            </h4>
                        @empty
                        @endforelse
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-4">
        </div>
        <div class="col-md-4">
        </div>
        <div class="col-md-4">
            <div id="money_section" class="d-md-none" style="height:50px"></div>
            <div class="social-section" id="mo-tab">

                <a class="close-mo cloz" aria-label="close">&times;</a>

                <div class="social-icon-holder site-border-color-icon social-icon-tw animated pulse infinite">
                    <i class="fa fa-credit-card"></i>
                </div>
                <div class="connect-button">

                    <div class="social-section social-sub">
                        @if(Auth::user()->residential_status=='home_owner')
                            <img class="knowso-logo" src="/img/logos/supa.png"><h3>Money</h3>
                            <h4><a href="https://secured.supacompare.co.uk/" target="_blank"><button class="btn btn-success" id="scroll-result">Loan Offers for you</button></a></h4>
                            <h4><a href="https://www.supacompare.co.uk/compare/Credit-Cards" target="_blank"><button class="btn btn-success" id="scroll-result">Credit Offers for you</button></a></h4>
                            <h4><a href="https://www.supacompare.co.uk/deals" target="_blank"><button class="btn btn-success" id="scroll-result">Offers Page</button></a></h4>
                        @else
                            <img class="knowso-logo" src="/img/logos/supa.png"><h3>Money</h3>
                            <h4><a href="https://www.supacompare.co.uk/compare/index/?slug=compare-short-term-loans" target="_blank"><button class="btn btn-success" id="scroll-result">Loan Offers for you</button></a></h4>
                            <h4><a href="https://www.supacompare.co.uk/compare/Credit-Cards" target="_blank"><button class="btn btn-success" id="scroll-result">Credit Offers for you</button></a></h4>
                            <h4><a href="https://www.supacompare.co.uk/deals" target="_blank"><button class="btn btn-success" id="scroll-result">Offers Page</button></a></h4>
                        @endif
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-4">
        </div>
    </div>
    <div class="row">

    </div>
    @isset ($test_api)
        @if($test_api)
    <pre>{!! print_r($data, true) !!}</pre>
    <p>Instagram:</p><pre>{!! print_r($ins_profile, true) !!}</pre>
    <p>Linkedin:</p><pre>{!! print_r($li_profile, true) !!}</pre>
    <p>Twitter:</p><pre>{!! print_r($tw_profile, true) !!}</pre>
    <p>Facebook:</p><pre>{!! print_r($fb_profile, true) !!}</pre>
        @endif
    @endisset
</div>

    </section>
    <div id="modal-facebook" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Disconnect</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to disconnect from your Facebook profile?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <a href="/fb_disconnect">
                        <button type="button" class="btn btn-primary">Disconnect</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="modal-twitter" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Disconnect</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to disconnect from your Twitter profile?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <a href="/tw_disconnect">
                        <button type="button" class="btn btn-primary">Disconnect</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="modal-linkedin" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Disconnect</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to disconnect from your LinkedIn profile?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <a href="/li_disconnect">
                        <button type="button" class="btn btn-primary">Disconnect</button>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-instagram" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Disconnect</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to disconnect from your Instagram profile?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <a href="/ins_disconnect">
                        <button type="button" class="btn btn-primary">Disconnect</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
