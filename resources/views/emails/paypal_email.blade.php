<?php
/**
 * Created by PhpStorm.
 * User: colino
 * Date: 12/04/18
 * Time: 23:12
 */
?>
<html>
<head>
    <title>Knowso - Paypal payment information</title>
    <!-- Bootstrap core CSS -->
    <link href="https://www.knowso.co.uk/vendor/bootstrap/css/bootstrap.min.css?1523875877" rel="stylesheet">
</head>
<body>
<div class="container">
    <img class="knowso-logo" src="https://www.knowso.co.uk/img/logos/knowso_logo_blue.png" style="height:64px"  />
    <h4>Paypal Payment information</h4>
    {!! $email !!}
</div>

</body>
</html>


