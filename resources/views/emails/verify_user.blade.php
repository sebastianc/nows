<!DOCTYPE html>
<html>

<head>
    <title>Knowso - Welcome</title>
    <!-- Bootstrap core CSS -->
    <link href="https://www.knowso.co.uk/vendor/bootstrap/css/bootstrap.min.css?1523875877" rel="stylesheet">
</head>

<body>

<div class="container">
    <img class="knowso-logo" src="https://www.knowso.co.uk/img/logos/knowso_logo_blue.png" style="height:64px"  />
    <h2>Welcome to the site {{$user['name']}}</h2>
    <br/>
    Your registered email-id is {{$user['email']}} , Please click on the below link to verify your email account
    <br/>

    @if(stristr(url("/"), "knowso.co.uk"))
    <a href="{{url('user/verify', $verify_users['token'])}}">Verify Email</a>
    @else
    <a href="http://knowso.test/user/verify/{{ $verify_users['token']}}">Verify Email</a>
    @endif

</div>

</body>

</html>
