<!DOCTYPE html>
<html lang="en">

<head>
    <title>{{$data['page_title']}}</title>
</head>

<body>

    <div class="container">
        <img class="knowso-logo" src="https://www.knowso.co.uk/img/logos/knowso_logo_blue.png" style="height:64px"  />
        <h2>{{$data['title']}}</h2>
        {!! $data['text1'] !!}
        {!! $data['text2'] !!}
    </div>

</body>

</html>
