<!DOCTYPE html>
<html>

<head>
    <title>{{$data['page_title']}}</title>
    <!-- Bootstrap core CSS -->
    <link href="https://www.knowso.co.uk/vendor/bootstrap/css/bootstrap.min.css?1523875877" rel="stylesheet">
    <link href="https://www.knowso.co.uk/public/css/animate.min.css" rel="stylesheet">
</head>

<body>

<div class="container">
    <img class="knowso-logo" src="https://www.knowso.co.uk/img/logos/knowso_logo_blue.png" style="height:64px"  />
    <h2>{{$data['title']}}</h2>
    <br/>
    {!! $data['text1'] !!}
    {{--$user['email']--}}
    <br/>
    {!! $data['text2'] !!}
    <br/>
    @if(stristr(url("/"), "knowso.co.uk"))
    <a href="{{url('/')}}">Contact Us</a>
    @else
    <a href="http://knowso.test/">Contact Us</a>
    @endif

</div>

</body>

</html>
