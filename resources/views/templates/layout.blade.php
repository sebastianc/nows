<?php
$reload_css = time(); // temporarily at least, this should avoid caching css during development

$get = filter_input_array(INPUT_GET);
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Knowso - Social Reporting</title>
    <link rel="icon" type="image/png" href="/img/logos/logo_icon_128.png" sizes="12x16">
    <link rel="icon" type="image/png" href="/img/logos/logo_icon_128.png" sizes="24x32">

    <!-- Bootstrap core CSS -->
    <link href="/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Dosis|Lato|PT+Sans|Sarala" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/css/agency.min.css?{{ $reload_css }}" rel="stylesheet">
    <link href="/css/animate.min.css" rel="stylesheet">
    <link href="/css/knowso.min.css?{{ $reload_css }}" rel="stylesheet">

    <!-- Bootstrap core JavaScript -->
    <script src="/vendor/jquery/jquery.min.js"></script>
    <script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="https://js.stripe.com/v3/"></script>
    <script src="/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-74179321-17"></script>
    <script>

        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());  gtag('config', 'UA-74179321-17');

    </script>
</head>

<body id="page-top">

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark{{ (!isset($analysis_view)? " fixed-top":"")}}" id="mainNav">
    <div class="container">
        <a class="js-scroll-trigger" href="{{ (Request::path() == "/")? "#page-top": "/" }}"><img class="knowso-logo" src="/img/logos/knowso_logo_blue.png"  /></a>

        <div class="mobile-login-signup">
            @if(!Auth::check())
                <ul>
                    <li><a href="/login@php if(isset($get['aid'])){echo '/?aid='.$get['aid'];}@endphp
                    "><span>LOG IN</span></a></li>
                    <li class="color-inverse"><a href="/register@php if(isset($get['aid'])){echo '/'.$get['aid'];}@endphp
                    "><span>SIGN UP</span></a></li>
                </ul>
            @endif
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fa fa-bars"></i>
        </button>
        </div>


        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="/">Home</a>
                </li>

                @if(Auth::check())
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="/offers">Offers</a>
                    </li>
                    {{--@if(filter_input(INPUT_SERVER, 'HTTP_HOST') == "knowso.local" || filter_input(INPUT_SERVER, 'HTTP_HOST') == "192.168.1.61:591")--}}
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="/user_analysis">Your Identity</a>
                    </li>
                    {{--@endif--}}
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">Log out</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                    <li class="nav-item">
                        <a href="/profile"><span style="" class="nav-link" data-toggle="tooltip" title="{{Auth::user()->email}}{{ (!empty(session('last_login'))? " - Last login: ".session('last_login'): "") }}"><i class="fa fa-user"></i> &#160;{{Auth::user()->first_name}}</span></a>
                    </li>
                @else

                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="/#what-is-it">What is it?</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="/customer-services">Customer Services</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="/social-media-and-loans">Social Media & Loans</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="/#faq">FAQ</a>
                </li>
                <li class="nav-item d-lg-none">
                    <a class="nav-link" href="/login@php if(isset($get['aid'])){echo '/?aid='.$get['aid'];}@endphp">Log in</a>
                </li>
                <li class="nav-item d-lg-none">
                    <a class="nav-link" href="/register@php if(isset($get['aid'])){echo '/'.$get['aid'];}@endphp">Sign up</a>
                </li>
                <li class="nav-item d-none d-lg-block">
                    <div class="login-register site-color-text-1">
                    <a class="nav-link site-background-1" href="/login@php if(isset($get['aid'])){echo '/?aid='.$get['aid'];}@endphp">Log in</a>
                    </div>
                </li>
                <li class="nav-item d-none d-lg-block">
                    <div class="login-register site-color-text-1">
                    <a class="nav-link site-background-1" href="/register@php if(isset($get['aid'])){echo '/'.$get['aid'];}@endphp">Sign up</a>
                    </div>
                </li>
                @endif

            </ul>
        </div>
    </div>
</nav>

@yield('site_content')
<!-- Footer -->
<footer class="pb-0 pb-sm-4 pt-0 pt-sm-4">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <span class="copyright" style="text-shadow: 1px 0.5px #f5e5ac;">Copyright &copy; Knowso.co.uk {{ date("Y") }}</span>
            </div>
            <div class="col-md-4">
                <ul class="list-inline social-buttons">
                    <li class="list-inline-item">
                        <a href="#">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="#">
                            <i class="fa fa-facebook"></i>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="#">
                            <i class="fa fa-linkedin"></i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-md-4">
                <ul class="list-inline quicklinks mx-md-0 px-md-0">
                    <li class="list-inline-item">
                        <a href="/privacy-policy">Privacy Policy</a>
                    </li>
                    <li class="list-inline-item">
                        <a href="/terms-of-use">Terms of Use</a>
                    </li>
                    <li class="list-inline-item">
                        <a href="/cancellation-refund-policy">Cancellation & Refunds</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div id="cookie-message" style="display: block;">
        <p>
            This website uses cookies to ensure that you get the best possible experience. &#160; <a href="/privacy-policy"><span>Learn more:</span></a> &#160; <a id="got-it"><button class="btn btn-warning">Got that!</button></a>
        </p>
    </div>
</footer>

<!-- Contact form JavaScript -->
<script src="/js/jqBootstrapValidation.min.js"></script>
<script src="/js/contact_me.min.js"></script>

<!-- Custom scripts for this template -->
<script src="/js/agency.min.js"></script>
<script src="/js/wow.min.js"></script>
@if(isset($kn_dashboard) && ($kn_dashboard))
<script src="/js/knowso_dashboard.js"></script>
@endif
@if(isset($analysis_view) && ($analysis_view))
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="/js/knowso_analysis.min.js"></script>

@endif

</body>

</html>

