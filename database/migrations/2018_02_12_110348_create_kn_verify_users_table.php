<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKnVerifyUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::create('kn_verify_users', function (Blueprint $table) {
//            $table->increments('id');
//            $table->timestamps();
//        });
        if (!Schema::hasTable('kn_verify_users')) {
            Schema::create('kn_verify_users', function (Blueprint $table) {
                $table->integer('user_id');
                $table->string('token');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kn_verify_users');
    }
}
