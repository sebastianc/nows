<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameUsersPaymentColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kn_users', function(Blueprint $table) {
            $table->renameColumn('paypal_app_id', 'stripe_id');
            $table->dropColumn('paypal_subscr_id');
            $table->renameColumn('paypal_subscr_active', 'stripe_paid');
            $table->renameColumn('paypal_subscr_date', 'sub_date');
            $table->renameColumn('paypal_subscr_pay_date', 'payment_date');
            $table->dropColumn('paypal_payer_id');
            $table->dropColumn('paypal_payer_email');
            $table->dropColumn('paypal_subscr_suspended');
            $table->renameColumn('paypal_subscr_cancelled', 'cancelled');
            $table->renameColumn('paypal_subscr_cancelled_date', 'cancelled_date');
            $table->renameColumn('paypal_subscr_expiry_date', 'expiry_date');
        });
    }


    public function down()
    {
        Schema::table('kn_users', function(Blueprint $table) {
            $table->renameColumn('stripe_id', 'paypal_app_id');
            $table->integer('paypal_subscr_id');
            $table->renameColumn('stripe_paid', 'paypal_subscr_active');
            $table->renameColumn('sub_date', 'paypal_subscr_date');
            $table->renameColumn('payment_date', 'paypal_subscr_pay_date');
            $table->integer('paypal_payer_id');
            $table->string('paypal_payer_email');
            $table->tinyInteger('paypal_subscr_suspended');
            $table->renameColumn('cancelled', 'paypal_subscr_cancelled');
            $table->renameColumn('cancelled_date', 'paypal_subscr_cancelled_date');
            $table->renameColumn('expiry_date', 'paypal_subscr_expiry_date');
        });
    }
}
