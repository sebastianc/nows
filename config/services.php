<?php
//$call_back_host = $_SERVER['HTTP_HOST'];
return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],



    'facebook' => [
        //'app_id' => env('1289232081174305'),         // Your GitHub Client ID
        'client_id' => env('FB_KEY','1289232081174305'),
        'client_secret' => env('FB_SECRET','d57dc5dc1fe4925b0e371bb8d34714b4'), // Your GitHub Client Secret
        //'redirect' => 'http://'.$_SERVER['HTTP_HOST']."/auth_redirect/facebook", // default : change with ->redirectUrl() in class code, if necessary (Login with FB, etc)
        'redirect' => "https://www.knowso.co.uk/auth_redirect/facebook", // default : change with ->redirectUrl() in class code, if necessary (Login with FB, etc)
    ],

    'instagram' => [
        'client_id' => env('INSTAGRAM_KEY','2bbc838203e044a3a03a730d3c73cdbb'),
        'client_secret' => env('INSTAGRAM_SECRET','97f41a6f1ccf465b92665b93c88f4103'),
        'redirect' => env('INSTAGRAM_REDIRECT_URI', env('APP_URL').'/auth_redirect/instagram')
    ],


    'twitter' => [

        'client_id' => env('TW_KEY','batrTKryAHxoFOOfA2PI0rt4Z'), // devknowso(admin@knowso.co.uk) credentials
        'client_secret' => env('TW_SECRET','6ghvZife24JKg6X4M0gFrcARMuMX8UyNKBp5rJZ0cbfzwzEoAg'), // devknowso(admin@knowso.co.uk) credentials

        'oauth_access_token' => "879977561710878720-VZtbYQP9oZNN0W7nVppik1soDO2T3TE",
        'oauth_access_token_secret' => "5B7VDQQR647R9l3xPx3rsJLaDKgOH6ilxXFWOHccdI0Wf",

        'redirect' => "https://www.knowso.co.uk/auth_redirect/twitter",
    ],

    // 2018-04-19 - New login credentials and APP for LinkedIn:
    //Knowso Personal Social Media Profile Analyser (hello@mediablanket.co.uk)
    'linkedin' => [

        'client_id' => env('LI_KEY','771jwblbqheeli'), // hello@mediablanket.co.uk credentials

        'client_secret' => env('LI_SECRET','g2CUUTe2nVT7TAix'), // hello@mediablanket.co.uk credentials

        'redirect' => env('APP_URL')."/auth_redirect/linkedin",
    ],

    'cv-library' => [
        'get-jobs' => 'https://www.cv-library.co.uk/search-jobs-json?key=5A4C5Lf6ga-m7rGc',
        'view-job'  => 'https://partners.cv-library.co.uk/api/v1/jobs/',
    ],

    'payment-service' => [
        'development' => 'http://ec2-18-202-176-40.eu-west-1.compute.amazonaws.com/payments',
        'production'  => 'http://ec2-18-202-176-40.eu-west-1.compute.amazonaws.com/payments',
    ],

    'register-stripe-service' => [
        'development' => 'http://ec2-18-202-176-40.eu-west-1.compute.amazonaws.com/members',
        'production'  => 'http://ec2-18-202-176-40.eu-west-1.compute.amazonaws.com/members',
    ],

//    'linkedin' => [
//
//        'client_id' => env('LI_KEY','86yi7wsero1ew3'), // devknowso(admin@knowso.co.uk) credentials
//
//        'client_secret' => env('LI_SECRET','vMDENKtRgFzhbY7k'), // devknowso(admin@knowso.co.uk) credentials
//
//        'redirect' => env('APP_URL')."/auth_redirect/linkedin",
//    ],

];
