<?php
/**
 * Created by PhpStorm.
 * User: colino
 * Date: 21/05/18
 * Time: 18:05
 */

Artisan::command('read_words_test', function () {

    $user = new App\User;
    $user_data = $user->getSocialConnections( "1" ); // check connections against logged in user.

    $fb_profile  =($fb = new App\Http\Controllers\Social_API\Facebook_API())->get_facebook_profile($user_data['fb_access_token']);
    //$fb_profile = ;
    //echo "fb profile: \n".print_r($fb_profile, true);
    $facebook_posts_list = $fb->get_posts_info($fb_profile->user['posts']['data'], 'message');
    //echo "fb posts list: \n".print_r($facebook_posts_list, true);
    $words_count = $fb->get_other_recommendations($facebook_posts_list);
    echo "Words count: \n".print_r($words_count, true);

})->describe('Reading and sorting words');

Artisan::command('save_analysis_test', function () {

//    $user = new App\User;
//    $user_data = $user->getSocialConnections( "1" ); // check connections against logged in user.

    $test_string = "open,understanding,quiet,gregarious,option_1";
    $fb_profile  =($fb = new App\Http\Controllers\Pages\User_analysis())->save_result_test($test_string);
//    //$fb_profile = ;
//    //echo "fb profile: \n".print_r($fb_profile, true);
//    $facebook_posts_list = $fb->get_posts_info($fb_profile->user['posts']['data'], 'message');
//    //echo "fb posts list: \n".print_r($facebook_posts_list, true);
//    $words_count = $fb->get_other_recommendations($facebook_posts_list);
//    echo "Words count: \n".print_r($words_count, true);

})->describe('Save Analysis output tests');

Artisan::command('analysis_results', function () {

//    $user = new App\User;
//    $user_data = $user->getSocialConnections( "1" ); // check connections against logged in user.

    $test_string = "open,understanding,quiet,gregarious,option_1";
    $res  =($fb = new App\Http\Controllers\Pages\User_analysis())->analysis_scores_array();

    echo "Analysis Results:\n\n".print_r($res, true)."\n***";


})->describe('Test analysis results array');