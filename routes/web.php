<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// extra 'register' route on top of default one for test data: March 2018
#$this->get('register/{test_data}', 'Auth\RegisterController@showRegistrationForm_old')->name('register-test');
$this->get('register/{aid}', 'Auth\RegisterController@showRegistrationForm')->name('register');
$this->get('login', 'Auth\LoginController@showLoginForm')->name('login');

$this->get('register_mb', 'Auth\RegisterController@showRegistrationForm_old');
$this->get('register_mb/{test_data}', 'Auth\RegisterController@showRegistrationForm_old');

Route::post('/register_send_paypal', 'Auth\RegisterController@register_paypal');

Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser');

Auth::routes();

Route::get('/', function () {
    if(Auth::check()){
        return redirect("/dashboard");
    }
    return view('knowso_welcome'); // Check | This should ultimately bump non-authenticated user back to login screen.
});

Route::get('/privacy-policy', function () {
    return view('pages/privacy-policy');
});

// Login with Facebook removed in order to link knowso with creditknowledge users, due to facebook policy which doesn't reveal emails

//Route::get('/redirect_to_fb', 'Auth\LoginWithFacebook@RedirectToFB');
//Route::get('/callback_from_fb', 'Auth\LoginWithFacebook@CallbackFromFB');

Route::get('/terms-of-use', function () {
    return view('pages/terms-of-use');
});

Route::get('/customer-services', function () {
    return view('pages/customer-services');
});

Route::get('/cancellation-refund-policy', function () {
    return view('pages/cancel-refund-policy');
});

Route::get('/social-media-and-loans', function () {
    return view('pages/social-media-and-loans');
});

Route::get('/dashboard_load', function () {

    return view('dashboard_load');

});

Route::get('/dashboard', 'HomeController@index');

// Store Social media profile access connections against Auth user in Database.
//Route::get('/store/facebook/{access_token}', 'Social_API\Facebook_API@store_facebook_profile')->middleware('auth');
Route::get('/store/facebook/', 'Social_API\Facebook_API@store_facebook_profile')->middleware('auth');
Route::get('/store/twitter/', 'Social_API\Twitter_API@store_twitter_profile')->middleware('auth');
Route::get('/store/linkedin/', 'Social_API\Linkedin_API@store_linkedin_profile')->middleware('auth');
Route::get('/store/instagram/', 'Social_API\Instagram_API@store_instagram_profile')->middleware('auth');

// Connect social media
Route::get('/fb_connect', 'Social_API\Facebook_API@redirect_to_fb')->middleware('auth');
Route::get('/tw_connect', 'Social_API\Twitter_API@redirect_to_tw')->middleware('auth');
Route::get('/li_connect', 'Social_API\Linkedin_API@redirect_to_li')->middleware('auth');
Route::get('/ins_connect', 'Social_API\Instagram_API@redirect_to_ins')->middleware('auth');

Route::get('/auth_redirect', 'Social_API\Social_Connect_API@handleProviderCallback')->middleware('auth'); // default provider : facebook
Route::get('/auth_redirect/{provider}', 'Social_API\Social_Connect_API@handleProviderCallback')->middleware('auth');

// Disconnect social media
Route::get('/fb_disconnect', 'Social_API\Facebook_API@disconnect_facebook')->middleware('auth');
Route::get('/tw_disconnect', 'Social_API\Twitter_API@disconnect_twitter')->middleware('auth');
Route::get('/li_disconnect', 'Social_API\Linkedin_API@disconnect_linkedin')->middleware('auth');
Route::get('/ins_disconnect', 'Social_API\Instagram_API@disconnect_instagram')->middleware('auth');
//Route::get('/fb_disconnect', 'HomeController@disconnect_facebook');

Route::get('/sql_mb', 'SqlDB\Run_query@index')->middleware('auth');
Route::post('/query_run', 'SqlDB\Run_query@run_query')->middleware('auth');

//Pages
Route::get('/offers', 'Pages\Offers@index')->middleware('auth');
Route::get('/user_analysis', 'Pages\User_analysis@index')->middleware('auth');
Route::get('/user_analysis/results', 'Pages\User_analysis@view_results')->middleware('auth');
Route::get('/user_analysis/results_no_header', 'Pages\User_analysis@viewResultsRaw')->middleware('auth')->name('analysisRaw');

Route::get('/get_personality_results', 'Pages\User_analysis@get_results')->middleware('auth');

Route::get('/user_analysis/{bypass}', 'Pages\User_analysis@index')->middleware('auth');

Route::post('save_user_analysis', 'Pages\User_analysis@save_data')->middleware('auth');

Route::get('/profile', 'Pages\ProfileController@view')->middleware('auth');
Route::post('/profile/update', 'Pages\ProfileController@update')->middleware('auth');
Route::post('/profile/cancel_confirm', 'Pages\ProfileController@cancel')->middleware('auth');

// New Stripe Registration Routes
Route::post('register', 'PaymentController@register');
Route::post('charge', 'PaymentController@processPayment');

Route::get('/address_lookup/{postcode}', 'Auth_forms\AddressLookup@search');

//Route::get('cancel/{app}', 'Paypal\Paypal_process@complete_registration');
Route::get('cancel/{app}', 'Paypal\Paypal_process@cancel');
Route::post('confirmed_registration/{app}', 'Paypal\Paypal_process@complete_registration');
Route::post('confirmed_registration', 'Paypal\Paypal_process@complete_registration');

// Paypal IPN listener / processing
Route::post('paypal_response', 'Paypal\Paypal_listener@index');
//Route::get('paypal_response/{unique_id}', 'Paypal\Paypal_listener@index');