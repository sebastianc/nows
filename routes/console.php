<?php

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/
include "console_colin.php"; // include Colin's developer test file to potentially avoid clashes with amended console.php file in future.

Artisan::command('copy_user', function () {
    //$paypal_unique_id = "kn_5ad4714fa534e";
    $paypal_unique_id = "kn_5ad1ca1ceae01";
    $copy = (new App\User_register())->copy_temp_user($paypal_unique_id);
    echo "Copy result for unique id, '$paypal_unique_id': $copy\n";
})->describe('Run a copy test of user');

Artisan::command('query_paypal', function () {
    //$paypal_unique_id = "kn_5ad4714fa534e";
    //$paypal_unique_id = "kn_5ad1ca1ceae01";
    $copy = (new App\User())->get_user_paypal_check("colcmorris@gmail.com");
    echo "Paypal retrieve query: \n".print_r($copy, true);
    if($copy['cancelled'] && $copy['expiry_date'] < date("Y-m-d H:i:s")){
        echo "\nPaypal subscription cancelled (".$copy['cancelled'].") and paypal subs expiry date ( ".$copy['expiry_date']." ) smaller than current time";
    }
})->describe('Retrieve selected paypal parameters');

//App\Http\Controllers\Paypal;
Artisan::command('paypal_date', function () {
    //$paypal_unique_id = "kn_5ad4714fa534e";
    //$paypal_unique_id = "kn_5ad1ca1ceae01";
    (new App\Http\Controllers\Paypal\Paypal_listener())->calc_dates(date("Y-m-d H:i:s"));
    //echo "Copy result for unique id, '$paypal_unique_id': $copy\n";
})->describe('Run date calcs');

Artisan::command('email_alert', function () {

    (new App\Http\Controllers\Email\Email_alerts())->send_basic();

})->describe('Send knowso email test');
